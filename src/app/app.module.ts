import {NgModule, NO_ERRORS_SCHEMA} from "@angular/core"
import {registerElement} from "nativescript-angular"
import {NativeScriptModule} from "nativescript-angular/nativescript.module"
import {NativeScriptUIListViewModule} from "nativescript-ui-listview/angular"
import {NativeScriptFormsModule} from "nativescript-angular/forms"
import {NativeScriptHttpClientModule} from "nativescript-angular/http-client"
import {NativeScriptSvgModule} from "nativescript-svg/angular"
import {NativeScriptUIChartModule} from "nativescript-ui-chart/angular"
import {CardView} from "@nstudio/nativescript-cardview"
import {DropDownModule} from "nativescript-drop-down/angular"
import {AppRoutingModule} from "./app-routing.module"
import {AppComponent} from "./app.component"
import {LoginComponent} from './login/login.component'
import {HomeComponent} from './home/home.component'
import {UserService} from "~/app/user.service"
import {ItemService} from "~/app/item/item.service"
import {OnboardComponent} from './onboard/onboard.component'
import {TabsModule} from "~/app/tabs/tabs.module"
import {SharedModule} from "~/app/shared/shared.module"
import {ApiService} from "~/app/api.service"
import {TNSCheckBoxModule} from "@nstudio/nativescript-checkbox/angular"
import {HTTP_INTERCEPTORS} from "@angular/common/http"
import {HttpInterceptorService} from "~/app/http-interceptor.service"
import * as purchase from '@proplugins/nativescript-purchase'
purchase.init([
    'Market_Insights',
    'Morning_Insights',
    'Market_Survival'
])
registerElement("CardView", () => CardView)
// registerElement("PreviousNextView", () => require("nativescript-iqkeyboardmanager").PreviousNextView)

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptModule,
        AppRoutingModule,
        TabsModule,
        SharedModule,
        NativeScriptUIListViewModule,
        NativeScriptUIChartModule,
        NativeScriptHttpClientModule,
        NativeScriptFormsModule,
        NativeScriptSvgModule,
        DropDownModule,
        TNSCheckBoxModule
    ],
    declarations: [
        AppComponent,
        HomeComponent,
        LoginComponent,
        OnboardComponent
    ],
    providers: [
        ItemService,
        UserService,
        ApiService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpInterceptorService,
            multi: true
        }
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
