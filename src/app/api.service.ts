import {Injectable} from '@angular/core'
import {HttpClient, HttpErrorResponse} from "@angular/common/http"
import {Observable, throwError} from "rxjs"
import {catchError} from "rxjs/operators";
import * as appSettings from "tns-core-modules/application-settings";
import * as dialogs from 'tns-core-modules/ui/dialogs'

@Injectable({
  providedIn: 'root'
})
export class ApiService {

    constructor(private http: HttpClient) {}

    getJSON(url, options) {
        return this.http.get<any>(url, options).pipe(
            // @ts-ignore
            catchError((err: HttpErrorResponse) => {
                console.log(' --- CATCH GET ERROR !!! ---- > ')
                this.handleError(err, url, options)
            })
        )
    }

    postJSON(url, body, options) {
        return this.http.post(url, body, options).pipe(
            // @ts-ignore
            catchError((err: HttpErrorResponse) => {
                console.log(' --- CATCH POST ERROR !!! ---- > ')
                this.handleError(err, url, options)
            })
        )
    }

    handleError(err: any, url: string, options: any) {
        if (err) {
            console.log('HANDLING ERROR ------- > ', err)
            this.reconnectUser().subscribe(response => {
                console.log('did reconnect respond? ', response)

                // delete app settings

                // store when broken, clear when re established

                // on login, if broken, force login

                let userPermStatus = response.headers.get('userPermissionStatus') //if (response.body['loggedInStatus'] == 'true' && userPermStatus == 'allow') {
                let resHeader = response.headers.get('Set-Cookie')
                let cookie = resHeader.toString()
                appSettings.setString('cookie', cookie)

                dialogs.alert({
                    title: 'Session timeout',
                    message: 'Please close any tabs and return to Home to continue',
                    okButtonText: 'OK'
                }).then(res => {
                    console.log('After the session dialog')


                    // doing nothing after the user hits OK ....
                })
                //}
            })

        }
    }

    getMarketMinute(cookie: string): Observable<any> {
        const url = 'https://app.chaikinanalytics.com/CPTRestSecure/app/pfd/getMajorUSMarketData'
        const options = {
            withCredentials: true,
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'en-US,en;q=0.9',
                'Connection': 'keep-alive',
                'Cookie': cookie,
                'Host': 'app.chaikinanalytics.com',
                'Referer': 'https://app.chaikinanalytics.com/'
            }
        }
        return this.getJSON(url, options)
    }

    reconnectUser(): Observable<any> {
        const e = appSettings.getString('email')
        const p = appSettings.getString('pW')
        const route = `https://app.chaikinanalytics.com/CPTRestSecure/app/authenticate/getAuthorization`;

        return this.http.get(route, {
            observe: 'response',
            withCredentials: true,
            params: {
                acquireSessionForcibly: 'yes',
                email: e,
                password: p
            }
        })


    }

    getSectorTracker(cookie: string): Observable<any> {
        const url = 'https://app.chaikinanalytics.com/CPTRestSecure/app/pfd/getSectorTrackers'
        const options = {
            withCredentials: true,
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'en-US,en;q=0.9',
                'Connection': 'keep-alive',
                'Cookie': cookie,
                'Host': 'app.chaikinanalytics.com',
                'Referer': 'https://app.chaikinanalytics.com/'
            }
        }
        return this.getJSON(url, options)
    }

    // get initial sector tracker info
    getSectorsInfo(cookie: string): Observable<any> {
        const url = 'https://app.chaikinanalytics.com/CPTRestSecure/app/etfSectors/getEtfSectorsPerformanceData'
        const options = {
            params: {
                period: 'year',
                etf_group_type: 'All Types',
                include_active_management: false,
                include_etns: false,
                include_leveraged: false,
                issuer: 'All Issuers',
                long_short: 'Long',
                order_by: 'rating',
                sectorViewAction: false,
                sort_direction: 'desc',
                sectorsETFUniverse: 'Sectors'
            },
            withCredentials: true,
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'en-US,en;q=0.9',
                'Connection': 'keep-alive',
                'Cookie': cookie,
            }
        }
        return this.getJSON(url, options)
    }

    generalEtfInfo(cookie: string): Observable<any> {
        const url = 'https://app.chaikinanalytics.com/CPTRestSecure/app/etfSectors/getEtfSectorsPerformanceData'
        const options = {
            params: {
                period: '6month',
                etf_group_type: 'All Types',
                include_active_management: true,
                include_etns: true,
                include_leveraged: false,
                issuer: 'All Issuers',
                long_short: 'Long',
                sectorsETFUniverse: 'ETFs'
            },
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'en-US,en;q=0.9',
                'Connection': 'keep-alive',
                'Cookie': cookie,
            }
        }
        return this.getJSON(url, options)
    }

    spdrEtfInfo(cookie: string): Observable<any> {
        const url = 'https://app.chaikinanalytics.com/CPTRestSecure/app/etfSectors/getEtfSectorsPerformanceData'
        const options = {
            params: {
                period: '6month',
                etf_group_type: 'All Types',
                include_active_management: true,
                include_etns: true,
                include_leveraged: false,
                issuer: 'All Issuers',
                long_short: 'Long',
                sectorsETFUniverse: 'Sectors'
            },
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'en-US,en;q=0.9',
                'Connection': 'keep-alive',
                'Cookie': cookie,
            }
        }
        return this.getJSON(url, options)
    }

    generalEtfInfoGroupType(cookie: string, group: string): Observable<any> {
        const url = 'https://app.chaikinanalytics.com/CPTRestSecure/app/etfSectors/getEtfSectorsPerformanceData'
        const options = {
            params: {
                period: '6month',
                etf_group_type: group,
                include_active_management: true,
                include_etns: true,
                include_leveraged: false,
                issuer: 'All Issuers',
                long_short: 'Long',
                order_by: 'rating',
                sectorViewAction: false,
                sort_direction: 'desc',
                sectorsETFUniverse: 'ETFs'
            },
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'en-US,en;q=0.9',
                'Connection': 'keep-alive',
                'Cookie': cookie,
            }
        }
        return this.getJSON(url, options)
    }

    singleEtfInfo(cookie: string, symbol: string): Observable<any> {
        const url = 'https://app.chaikinanalytics.com/CPTRestSecure/app/etfSectors/getEtfSectorsSummaryAndBrackdownData'
        const options = {
            params: {
                compositeTicker: symbol
            },
            withCredentials: true,
            headers: {
                'Accept'            : 'application/json, text/plain, */*',
                'Accept-Encoding'   : 'gzip, deflate, br',
                'Accept-Language'   : 'en-US,en;q=0.9',
                'Connection'        : 'keep-alive',
                'Cookie'            : cookie,
                'Host'              : 'app.chaikinanalytics.com',
                'Referer'           : 'https://app.chaikinanalytics.com/'
            }
        }
        return this.getJSON(url, options)
    }

    goodie(cookie: string, ticker: string): Observable<any> {
        const url = 'https://app.chaikinanalytics.com/CPTRestSecure/app/etfSectors/getEtfSectorsSummaryAndBrackdownData'
        const options = {
            params: {
                compositeTicker: ticker
            },
            withCredentials: true,
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'en-US,en;q=0.9',
                'Connection': 'keep-alive',
                'Cookie': cookie,
            }
        }
        return this.getJSON(url, options)
    }

    // get more detailed sector tracker info
    sector2(cookie: string): Observable<any> {
        const url = 'https://app.chaikinanalytics.com/CPTRestSecure/app/etfSectors/getSectorsPerformanceChart'
        const options = {
            withCredentials: true,
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'en-US,en;q=0.9',
                'Connection': 'keep-alive',
                'Cookie': cookie,
            }
        }
        return this.getJSON(url, options)
    }

    isMarketOpen(cookie: string): Observable<any> {
        const url = 'https://app.chaikinanalytics.com/CPTRestSecure/app/price/isMarketOpen'
        const options = {
            withCredentials: true,
            headers: {
                'Connection': 'keep-alive',
                'Cookie': cookie,
            }
        }
        return this.getJSON(url, options)
    }

    // show like stocks
    discover(cookie: string, stock: string): Observable<any> {
        const url = 'https://app.chaikinanalytics.com/CPTRestSecure/app/discoveryEngine/getResultLists?uid=1055035'
        const options = {
            params: {
                stock: stock
            },
            withCredentials: true,
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'en-US,en;q=0.9',
                'Connection': 'keep-alive',
                'Cookie': cookie,
            }
        }
        return this.getJSON(url, options)
    }

    getRecentPosts(): Observable<any> {
        const url = 'https://insights.chaikinanalytics.com/?json=get_recent_posts'
        const options = {
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'en-US,en;q=0.9',
            }
        }
        return this.getJSON(url, options)
    }

    getInsights(): Observable<any> {
        const url = 'https://insights.chaikinanalytics.com/?json=secursive.get_product_updates&app=1&id=4&count=4'
        const options = {
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'en-US,en;q=0.9',
                'Connection': 'keep-alive',
            }
        }
        return this.getJSON(url, options)
    }

    getMarketInsights() {
        const url = 'https://insights.chaikinanalytics.com/wp-json/wp/v2/posts'
        const params = {
            categories: [5]
        }
        return this.getJSON(url, { params } )
    }

    getMorningInsights() {
        const url = 'https://insights.chaikinanalytics.com/wp-json/wp/v2/posts'
        const params = {
            categories: [6]
        }
        return this.getJSON(url, {params} )
    }

    getMarketSurvivalGuide() {
        const url = 'https://insights.chaikinanalytics.com/wp-json/wp/v2/posts'
        const params = {
            categories: [30]
        }
        return this.getJSON(url, { params } )
    }

    getWPAds() {
        const url = 'https://insights.chaikinanalytics.com/wp-json/wp/v2/posts'
        const params = {
            categories: [35]
        }
        return this.getJSON(url, { params } )
    }

    getWPAdsBanner() {
        const url = 'https://insights.chaikinanalytics.com/wp-json/wp/v2/posts'
        const params = {
            categories: [36]
        }
        return this.getJSON(url, { params } )
    }

    getIndustryFocus(cookie: string): Observable<any> {
        const url = 'https://app.chaikinanalytics.com/CPTRestSecure/app/pfd/getSubSectorFocus'
        const options = {
            withCredentials: true,
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'en-US,en;q=0.9',
                'Connection': 'keep-alive',
                'Cookie': cookie,
            }
        }
        return this.getJSON(url, options)
    }

    getTrendingStocks(cookie: string): Observable<any> {
        const url = 'https://app.chaikinanalytics.com/CPTRestSecure/app/pfd/getGainerAndLoserStocks'
        const options = {
            withCredentials: true,
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'en-US,en;q=0.9',
                'Connection': 'keep-alive',
                'Cookie': cookie,
            }
        }
        return this.getJSON(url, options)
    }

    stockInNews(cookie: string): Observable<any> {
        const url = 'https://app.chaikinanalytics.com/CPTRestSecure/app/pfd/getStockInNews'
        const options = {
            withCredentials: true,
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'en-US,en;q=0.9',
                'Connection': 'keep-alive',
                'Cookie': cookie,
            }
        }
        return this.getJSON(url, options)
    }

    getHeadlines(symbol: string, cookie: string): Observable<any> {
        const url = 'https://app.chaikinanalytics.com/CPTRestSecure/app/xigniteNews/getHeadlines'
        const options = {
            params: {
                'symbol' : symbol
            },
            withCredentials: true,
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'en-US,en;q=0.9',
                'Connection': 'keep-alive',
                'Cookie': cookie,
            }
        }
        return this.getJSON(url, options)
    }

    getEarningsUpdateUpcoming(cookie: string): Observable<any> {
        const url = 'https://app.chaikinanalytics.com/CPTRestSecure/app/pfd/getUpcomingEarningReports'
        const options = {
            withCredentials: true,
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'en-US,en;q=0.9',
                'Connection': 'keep-alive',
                'Cookie': cookie,
            }
        }
        return this.getJSON(url, options)
    }

    getEarningsUpdateRecent(cookie: string): Observable<any> {
        const url = 'https://app.chaikinanalytics.com/CPTRestSecure/app/pfd/getRecentEarningReports'
        const options = {
            withCredentials: true,
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'en-US,en;q=0.9',
                'Connection': 'keep-alive',
                'Cookie': cookie,
            }
        }
        return this.getJSON(url, options)
    }

    getStockOfDay(cookie: string): Observable<any> {
        const url = 'https://app.chaikinanalytics.com/CPTRestSecure/app/pfd/getStockOfTheDay'
        const options = {
            withCredentials: true,
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'en-US,en;q=0.9',
                'Connection': 'keep-alive',
                'Cookie': cookie,
            }
        }
        return this.getJSON(url, options)
    }

    getInitialData(cookie: string): Observable<any> {
        const url = 'https://app.chaikinanalytics.com/CPTRestSecure/app/screener/getInitialData'
        const options = {
            withCredentials: true,
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'en-US,en;q=0.9',
                'Connection': 'keep-alive',
                'Cookie': cookie,
            }
        }
        return this.getJSON(url, options)
    }

    createStarredList(cookie: string, uid: string): Observable<any> {
        const url = 'https://app.chaikinanalytics.com/CPTRestSecure/app/portfolio/addListToPortfolio'
        const options = {
            withCredentials: true,
            params: {
                uid: uid,
                responseType: 'custom',
                listName: 'Starred'
            },
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Accept-Encoding':'gzip, deflate, br',
                'uid': uid,
                'Connection': 'keep-alive',
                'Cookie': cookie,
            }
        }
        return this.getJSON(url, options)
    }

    getMyLists(uid: string, cookie: string): Observable<any> {
        const url = 'https://app.chaikinanalytics.com/CPTRestSecure/app/portfolio/getAuthorizedLists'
        const options = {
            params: {
                uid: uid,
                rank: 'false',
                responseType: 'custom',
                environment: 'desktop',
                version: '1.3.4'
            },
            withCredentials: true,
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Accept-Encoding':'gzip, deflate, br',
                'Referer': 'https://app.chaikinanalytics.com/',
                'uid': uid,
                'Accept-Language': 'en-US,en;q=0.9,es;q=0.8',
                'Connection': 'keep-alive',
                'Cookie': cookie,
            }
        }
        return this.getJSON(url, options)
    }
}
