import {Component, OnInit, ViewContainerRef} from '@angular/core'
import {ActivatedRoute} from "@angular/router"
import {ItemService} from "~/app/item/item.service"
import * as appSettings from "tns-core-modules/application-settings"
import {ModalDialogOptions, ModalDialogService, RouterExtensions} from "nativescript-angular";
import {EtfDetailComponent} from "~/app/etf-detail/etf-detail.component";
import {ApiService} from "~/app/api.service";
import {ListEtfHoldingsComponent} from "~/app/list-etf-holdings/list-etf-holdings.component";

@Component({
    selector: 'ns-list-powerbar',
    templateUrl: './list-powerbar.component.html',
    moduleId: module.id,
})
export class ListPowerbarComponent implements OnInit {

    list: any = {}
    user: any = {}
    config: any = {}
    ready: boolean = false

    constructor(private route: ActivatedRoute,
                private router: RouterExtensions,
                private item: ItemService,
                private modalService: ModalDialogService,
                private viewContainerRef: ViewContainerRef,
                private api: ApiService) {}

    ngOnInit() {
        console.log(' --- INIT: LIST POWBAR COMP --- ')
        this.user = {
            cookie: appSettings.getString('cookie'),
            uid: appSettings.getString('uid')
        }
        this.list.name = appSettings.getString('ListTitle')
        if (this.list.name == 'Major Indices') {
            this.item.getAuthorizedLists(this.user.uid, this.user.cookie)
                .subscribe(lists => {
                    let indexOfIndices;
                    let numberOfLists = Object.keys(lists).length
                    for (let i=0;i<numberOfLists;i++) if (Object.keys(lists[i]).toString() == 'Indices') indexOfIndices = i
                    let list = lists[indexOfIndices]['Indices']
                    let answer = []
                    let cleanedIndex = list.filter(x => x['name'].length>8)
                    cleanedIndex.forEach(item => {
                        let symbol = item['name'].substring(0,3)
                        this.item.getSymbolData(symbol, this.user.uid, this.user.cookie)
                            .subscribe(s => {
                                let powerBarSplit = item['powerBar'].split(',')
                                answer.push({
                                    name: item['name'],
                                    bear: parseInt(powerBarSplit[0]),
                                    neut: parseInt(powerBarSplit[1]),
                                    bull: parseInt(powerBarSplit[2]),
                                    list: item['list_id'],
                                    ticker: symbol,
                                    pgr: s['metaInfo'][0]['PGR'],
                                    raw: s['metaInfo'][0]['raw_PGR']
                                })
                                this.list.items = answer
                                this.list.items.sort((a,b) => {
                                    let sorted = b['pgr'] < a['pgr'] ? -1 : b['pgr'] > a['pgr'] ? 1 : 0
                                    return sorted
                                })
                                this.ready = true
                            })
                    })
                })
        }

        if (this.list.name == 'SPDR Sectors') {
            this.list.id = appSettings.getNumber('ListId')
            this.api.getSectorsInfo(this.user.cookie)
                .subscribe(info => {
                    let answer = []
                    let sectors = info['sectors_performance_table_data']
                    sectors.forEach(sect => {
                        console.log('!!!',sect['PGR'], sect['raw_PGR'])
                        answer.push({
                            name: sect['etf_name'],
                            bear: sect['last_week_power_bar']['red'],
                            neut: sect['last_week_power_bar']['yellow'],
                            bull: sect['last_week_power_bar']['green'],
                            list: sect['list_id'],
                            ticker: sect['composite_ticker'],
                            pgr: sect['PGR'],
                            raw: sect['raw_PGR']
                        })
                    })
                    this.list.items = answer
                    this.ready = true
                })
        }
    }

    pgrRatingETF(pgr, raw) {
        return this.item.getPGRratingETF(pgr, raw)
    }

    setBullWidth(item) {
        let total = (item.bull+item.bear)
        let bullishness = (item['bull'] / total)
        let math = (bullishness * 90)
        return Math.max(math,40)
    }

    setBearWidth(item) {
        let total = (item.bull+item.bear)
        let bearishness = (item.bear / total)
        let math = (bearishness * 90)
        return Math.max(math,40)
    }

    setNeutWidth(item) {
        let total = (item.bull+item.bear+item.neut)
        let neutralness = (item.neut / total)
        let math = (neutralness * 40)
        return Math.min(Math.max(math,40), 110) // return at least 40, up to ratio but max 80
    }

    goToETFDetail(item) {
        console.log(' === GO TO ETF DETAIL : === ', item)
        appSettings.setString('etf-detail', item.ticker)
        // appSettings.setString('holdings-list-id', item.list.toString())
        const options: ModalDialogOptions = {
            viewContainerRef: this.viewContainerRef,
            fullscreen: true,
            context: {}
        }
        this.modalService.showModal(EtfDetailComponent, options)
    }

    goToListDetail(item) {
        console.log(' --- GO TO LIST DETAIL : --- ', item)
        appSettings.setNumber('ListId', item.list)
        appSettings.setString('ListName', item.name)
        const options: ModalDialogOptions = {
            viewContainerRef: this.viewContainerRef,
            fullscreen: true,
            context: {}
        }
        this.modalService.showModal(ListEtfHoldingsComponent, options)
    }
}
