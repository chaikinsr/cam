import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { ExploreComponent } from "~/app/explore/explore.component";
import { DiscoverComponent } from "~/app/discover/discover.component";
import { DiscoverMenuComponent } from "~/app/discover-menu/discover-menu.component";
import { ScreenerMenuComponent } from "~/app/screener-menu/screener-menu.component";

const routes: Routes = [
    {   path: '',
        component: ExploreComponent,
        children: [
            {
                path: '',
                children: [
                    { path: '', redirectTo: 'discover' },
                    // shows all idea elements, not just discover
                    { path: 'discover', component: DiscoverComponent },
                    { path: 'discover-menu', component: DiscoverMenuComponent },
                    { path: 'screener-menu', component: ScreenerMenuComponent }
                ]
            }
        ]
    },
];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class ExploreRoutingModule { }
