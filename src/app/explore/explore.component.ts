import { Component, OnInit } from '@angular/core';
import {RouterExtensions} from "nativescript-angular";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'ns-explore',
  templateUrl: './explore.component.html',
  styleUrls: ['./explore.component.scss'],
  moduleId: module.id,
})
export class ExploreComponent implements OnInit {

  constructor(private router: RouterExtensions,
              private route: ActivatedRoute) { }

  ngOnInit() {
      console.log(' --- EXPLORE COMPONENT INIT --- ')
  }



}
