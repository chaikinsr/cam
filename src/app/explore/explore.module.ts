import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { DropDownModule } from "nativescript-drop-down/angular";
import { ExploreRoutingModule } from './explore-routing.module';
import { ExploreComponent } from './explore.component';
import { DiscoverComponent } from "~/app/discover/discover.component";
import { DiscoverMenuComponent } from "~/app/discover-menu/discover-menu.component";
import { ScreenerMenuComponent } from "~/app/screener-menu/screener-menu.component";
import {NativeScriptUIListViewModule} from "nativescript-ui-listview/angular";

@NgModule({
  declarations: [
        ExploreComponent,
        DiscoverComponent,
        DiscoverMenuComponent,
        ScreenerMenuComponent
  ],
  imports: [
        ExploreRoutingModule,
        NativeScriptCommonModule,
        DropDownModule,
        NativeScriptUIListViewModule,

  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class ExploreModule { }
