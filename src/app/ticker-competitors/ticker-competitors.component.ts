import { Component, OnInit } from '@angular/core';
import * as appSettings from "tns-core-modules/application-settings";
import {ItemService} from "~/app/item/item.service";

@Component({
  selector: 'ns-ticker-competitors',
  templateUrl: './ticker-competitors.component.html',
  styleUrls: ['./ticker-competitors.component.css'],
  moduleId: module.id,
})
export class TickerCompetitorsComponent implements OnInit {

    public stock: any = {};
    public user: any = {};
    public competitors: any = {};

    constructor(private itemService: ItemService) { }

    ngOnInit() {
        this.stock.symbol = appSettings.getString('symbol');
        this.stock.name = appSettings.getString('name');
        this.user.cookie = appSettings.getString('cookie')
        this.user.uid = appSettings.getString('uid');

        this.itemService.getTickerCompetitors(this.stock.symbol, this.user.cookie).subscribe(res => {
            console.log('get ticker competitors res');
            console.log(res['compititors']);

            this.competitors = res['compititors'];


        }, err => {
            console.log('err', err)
        })
    }

    get getCompetitors(): any {
        console.log('get these jawns')
        return this.stock.competitors;
    }

}
