import {Component, OnInit} from '@angular/core'
import {ApiService} from "~/app/api.service"
import * as utils from "tns-core-modules/utils/utils"
import * as moment from 'moment'
import * as appSettings from 'tns-core-modules/application-settings'
import {UserService} from "~/app/user.service"
import * as dialogs from "tns-core-modules/ui/dialogs"
import * as purchase from '@proplugins/nativescript-purchase'
import {Transaction, TransactionState} from "@proplugins/nativescript-purchase/transaction"
import {Product} from "@proplugins/nativescript-purchase/product"

@Component({
    selector: 'ns-insights',
    templateUrl: './insights.component.html',
    styleUrls: ['./insights.component.css'],
    moduleId: module.id,
})

export class InsightsComponent implements OnInit {
    insights: any = {}
    ready: any = {}
    products: any = []
    ads: any = {}
    user: any = {}
    marketInsightLocked: boolean = true
    morningInsightLocked: boolean = true
    marketSurvivalLocked: boolean = true
    purchased: any = {}
    menu: any = {}

    constructor(public api: ApiService,
                private userService: UserService) {
        this.ready = {
            survival: false,
            morning: false,
            insight: false,
            perm: false
        }
    }

    get getMorningInsight() {
        return this.insights.morning
    }

    get getMarketInsight() {
        return this.insights.market
    }

    get getMarketSurvival() {
        return this.insights.survival
    }

    ngOnInit() {
        console.log(' --- INSIGHTS COMPONENT INIT --- ')
        this.user = {
            email: appSettings.getString('email'),
            cookie: appSettings.getString('cookie'),
            uid: appSettings.getString('uid'),
            starred: appSettings.getNumber('starred')
        }
        this.loadMyPerms()
        this.menu = 'buy'

        purchase.on(purchase.transactionUpdatedEvent, (transaction: Transaction) => {
            console.log(' ----- transactionUpdatedEvent ----- > ', transaction)
            if (transaction.transactionState == TransactionState.Purchased) { // regular purchase
                console.log('buying ', transaction.productIdentifier)
                appSettings.setBoolean(transaction.productIdentifier, true)
                dialogs.alert({
                    title: 'Thanks for your purchase!',
                    message: 'You may view it by clicking READ above',
                    okButtonText: 'OK'
                })
                console.log('sending data to unlock ------> ')
                this.unlock(transaction.productIdentifier)
            }
            if (transaction.transactionState == TransactionState.Failed) {
                console.log(' ==== transaction failed: ', transaction.productIdentifier)
                appSettings.setBoolean(transaction.productIdentifier, false)
            }
            if (transaction.transactionState == TransactionState.Restored) {
                console.log(transaction.originalTransaction.transactionDate);
                // console.log(`Purchase of ${transaction.originalTransaction.productIdentifier} restored.`)
                // console.log(transaction.originalTransaction);
                // console.log(transaction.originalTransaction.transactionDate);
                // appSettings.setBoolean(transaction.originalTransaction.productIdentifier, true)
            }
        })
        this.prepareInsights() // IF HAS INFUSIONSOFT TAG 67581 SHOW MARKET SURVIVAL
    }

    loadMyPerms() {
        this.userService.getUserPerms(this.user.uid).subscribe(perms => { // get back end perms
            let permsArr = perms.split(',')
            let keys = [],
                vals = []
            permsArr.forEach(item => {
                let key = item.split(':')
                keys.push(key[0].trim())
                vals.push(key[1].trim())
            })
            let mkt = vals[0] == 'true',
                mor = vals[1] == 'true',
                mar = vals[2] == 'true',
                pre = vals[3] == 'true',
                userPerms = {
                    marketSurvivalGuide: mkt,
                    morningInsights: mor,
                    marketInsights: mar,
                    premium: pre
                }

            appSettings.setBoolean('Market_Insights', mkt)
            appSettings.setBoolean('Morning_Insights', mor)
            appSettings.setBoolean('Market_Survival', mar)

            this.user.perms = userPerms
            this.marketSurvivalLocked = !mkt
            this.morningInsightLocked = !mor
            this.marketInsightLocked = !mar
            this.ready.perm = true

            if (!userPerms.premium) { // free users
                this.api.getWPAds().subscribe(ads => {
                    let regex = /(https?:\/\/.*\.(?:png|jpg))/;
                    let img = ads[0]['content']['rendered']
                    let imgUrl = regex.exec(img)
                    let URL = imgUrl[0].replace('&#215;', 'x') // imgUrl replace &#215; with 'x' as in 1024x720
                    let linkItem = ads[0]['excerpt']['rendered']
                    let link = linkItem.slice(3, linkItem.length - 5)
                    this.ads = {
                        jpg: URL,
                        url: link
                    }
                })
            }
        })
    }

    doSomething() {
        if (purchase.canMakePayments()) { // should this be here or on the perms setup ?
            purchase.restorePurchases()
                .then((res) => {
                    console.log(' ---- restore purchases ---- > ', res)
                }).catch((err) => {
                    console.log('err', err)
                }
            )

            purchase.getProducts()
                .then((products: Array<Product>) => {
                    this.products = products
                })
                .catch((err) => {
                        console.log('err', err)
                        dialogs.alert({
                            title: 'can not make purch Disconnection Error',
                            message: 'Lost contact to your apple account and profile',
                            okButtonText: 'OK'
                        })
                    }
                )
        } else {
            dialogs.alert({
                title: 'Error',
                message: 'Can not make purchases',
                okButtonText: 'OK'
            })
        }
    }

    shouldIShowAds() {
        return !this.user.perms.premium
    }

    routeToBuy(){
        this.menu = 'buy'
    }

    routeToRead(){
        this.menu = 'read'
    }

    getAdSrc() {
        return this.ads.jpg
    }

    goToAds() {
        this.openBrowser(this.ads.url)
    }

    prepareInsights() {
        this.api.getMorningInsights().subscribe(res => {
            this.insights.morning = res
            this.ready.morning = true
        })
        this.api.getMarketInsights().subscribe(res => {
            this.insights.market = res
            this.ready.insight = true
        })
        this.api.getMarketSurvivalGuide().subscribe(res => {
            this.insights.survival = res
            this.ready.survival = true
        })
    }

    dateParse(date) {
        return moment(date).format('MMMM DD YYYY')
    }

    titleFormatter(title){
        title = title.replace('&#8230;', '...')
        return title.replace('#038;', '')
    }

    openBrowser(url) {
        utils.openUrl(url);
    }

    public getMarketInsightPic(item) {
        return item['uagb_featured_image_src']['medium'][0]
    }

    showMarket() {
        return appSettings.getBoolean('Market_Insights')
    }

    showMorning() {
        return appSettings.getBoolean('Morning_Insights')
    }

    showSurvival() {
        return appSettings.getBoolean('Market_Survival')
    }

    addToSubscription(item) {
        if (purchase.canMakePayments()) {
            purchase.buyProduct(item)
        } else {
            dialogs.alert({
                title: 'Sorry',
                message: 'Your account is not able to make purchases',
                okButtonText: 'OK'
            })
        }
    }

    unlock(insight) {
        switch (true) {
            case insight == 'Market_Insights':
                this.marketInsightLocked = false
                this.user.perms.marketInsights = true
                break
            case insight == 'Morning_Insights':
                this.morningInsightLocked = false
                this.user.perms.morningInsights = true
            case insight == 'Market_Survival':
                this.marketSurvivalLocked = false
                this.user.perms.marketSurvivalGuide = true
                break
        }
        this.menu = 'read'
        let u = this.user.perms
        let userPermsObj = 'marketSurvivalGuide: ' + u['marketSurvivalGuide'] + ', morningInsights: ' + u['morningInsights'] + ', marketInsights: ' + u['marketInsights'] + ', premium: ' + u['premium']
        this.userService.setUserPerms(this.user.uid, userPermsObj).subscribe(res => console.log('after purchase response: ', res))
    }
}
