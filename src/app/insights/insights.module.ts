import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';

import { InsightsRoutingModule } from './insights-routing.module';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { InsightsComponent } from './insights.component';
import {NativeScriptUIListViewModule} from "nativescript-ui-listview/angular";

@NgModule({
  declarations: [InsightsComponent],
  imports: [
    InsightsRoutingModule,
    NativeScriptCommonModule,
      NativeScriptUIListViewModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class InsightsModule { }
