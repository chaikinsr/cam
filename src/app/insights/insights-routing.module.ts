import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import {InsightsComponent} from "~/app/insights/insights.component";

const routes: Routes = [
    { path: '', component: InsightsComponent }
];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class InsightsRoutingModule { }
