import {Component, NgZone, OnInit} from '@angular/core';
import * as appSettings from "tns-core-modules/application-settings";
import {RouterExtensions} from "nativescript-angular";
import {ActivatedRoute} from "@angular/router";
import {ApiService} from "~/app/api.service";
import {TextField} from "tns-core-modules/ui/text-field";

@Component({
  selector: 'ns-discover-menu',
  templateUrl: './discover-menu.component.html',
  // styleUrls: ['../explore/explore.component.css'],
  moduleId: module.id,
})
export class DiscoverMenuComponent implements OnInit {

    constructor(private router: RouterExtensions,
                private route: ActivatedRoute,
                public api: ApiService,
                private zone: NgZone) { }

    private user: any = {};
    public general = {};
    public alikeStocks = {};
    public swapCandidates = {};
    public similarBulls = {};
    public etfsHolding = {};
    show: any = {};

    ngOnInit() {
        this.user.cookie = appSettings.getString('cookie');

        this.api.discover(this.user.cookie, 'MSFT').subscribe(res => {

            // for (let keys in res['results'][0]) console.log('!', keys)

            let general = res['metainfo'];
            this.general = general;

            let alikeStocks = res['results'][0]['stocks'];
            this.alikeStocks = alikeStocks;

            let swapCandidates = res['results'][1]['stocks'];
            this.swapCandidates = swapCandidates;

            let similarBulls = res['results'][2]['stocks'];
            this.similarBulls = similarBulls;
            console.log('$$', similarBulls)

            let etfsHolding = res['results'][4]['stocks'];
            this.etfsHolding = etfsHolding;

        })
        this.show.alikeStocks = true;
    }

    public pgr(pgr) {
        switch(pgr) {
            case -1:
                return 'res://ico__PGR-E--LG__None--light'
                break;
            case 0:
                return 'res://ico__PGR-E--LG__None--light'
                break;
            case 1:
                return 'res://ico__PGR-E--LG__VeryBearish--light'
                break;
            case 2:
                return 'res://ico__PGR-E--LG__Bearish--light'
                break;
            case 3:
                return 'res://ico__PGR-E--LG__Neutral--light'
                break;
            case 4:
                return 'res://ico__PGR-E--LG__Bullish--light'
                break;
            case 5:
                return 'res://ico__PGR-E--LG__VeryBullish--light'
                break;
        }
    }


    public onTextChange(args) {
        let textField = <TextField>args.object;
        console.log('onTextChange');
        console.log('text!', textField.text);
        // console.log('woo!', this.search.query);
        // this.getSymbolLookup();
    }

    public onReturn(args) {
        let textField = <TextField>args.object;
        console.log('onReturn', textField);
        // this.search.query = textField.text;
        console.log(typeof textField)
        // console.log('woo!', this.search.query);
        // this.getSymbolLookup();

    }

    toggleAlikeStocks() {
        this.show.alikeStocks = !this.show.alikeStocks;
    }
    toggleSwapCandidates() {
        this.show.swapCandidates = !this.show.swapCandidates;
    }
    toggleSimilarBulls() {
        this.show.similarBulls = !this.show.similarBulls;
    }
    toggleEtfsHolding() {
        this.show.etfsHolding = !this.show.etfsHolding;
    }

}
