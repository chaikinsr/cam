import {Injectable} from '@angular/core'
import {Observable} from "rxjs"
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http"
import {ApiService} from "~/app/api.service"
import {catchError} from "rxjs/operators"
import * as appSettings from 'tns-core-modules/application-settings'

@Injectable({
    providedIn: 'root'
})
export class UserService {
    NSString: any;
    NSUTF8StringEncoding: any;

    constructor(private http: HttpClient,
                private api: ApiService) {
    }

    baseAuth(): Observable<any> {
        const route = `https://app.chaikinanalytics.com/CPTRestSecure/app/authenticate/getAuthorization`;
        return this.http.get(route, {
            observe: 'response',
            withCredentials: true,
            params: {
                acquireSessionForcibly: 'yes',
                email: 'free',
                password: 'free'
            }
        })
    }

    getUserPerms(uid) {
        const route = `https://app.chaikinanalytics.com/CPTRestSecure/app/mobile/getMobileData?uid=` + uid
        const options = {
            responseType: 'text' as 'text'
        }
        return this.http.get(route, options)
    }

    setUserPerms(uid, body) {
        const url = `https://app.chaikinanalytics.com/CPTRestSecure/app/mobile/storeMobileData?uid=` + uid
        const options = {
            params: {
                uid: uid
            },
            responseType: 'text',
            headers: new HttpHeaders({
                'Content-Type': 'text/plain'
            })
        }
        return this.api.postJSON(url,body,options)
    }

    authUser(email, pw): Observable<any> {
        const route = `https://app.chaikinanalytics.com/CPTRestSecure/app/authenticate/getAuthorization`;

        appSettings.setString('email', email)
        appSettings.setString('pW', pw)

        return this.http.get(route, {
            observe: 'response',
            withCredentials: true,
            params: {
                acquireSessionForcibly: 'yes',
                email: email,
                password: pw
            }
        })
    }

    checkSession() {
        console.log('checking session')

        let uid = appSettings.getString('uid')

        // first, trying to make a simple call like get price

        const route = `https://app.chaikinanalytics.com/CPTRestSecure/app/price/getSymbolPrice?uid=` + uid + `9582&ticker=SPY`

        return this.api.getJSON(route, {})

        // if this doesnt work, try calling reconnectUser() right away...see if this.authUser can be called repeatedly

    }

    refreshLogin(): Observable<any> { // like reconnect user but without email / pw
        let uid = 1057771;
        const route = `https://app.chaikinanalytics.com/CPTRestSecure/app/price/getSymbolPrice?uid=` + uid + `9582&ticker=SPY`
        return this.http.get(route).pipe(
            // @ts-ignore
            catchError((err: HttpErrorResponse) => {
                console.log(' --- LOGIN ERROR WIPE ---- > ')



                appSettings.clear()
            })
        )
    }

    reconnectUser() {
        console.log(' -- reconnect user --- ')
        let e = appSettings.getString('email')
        let p = appSettings.getString('pW')
        this.authUser(e,p).subscribe(response => {
                console.log(' --- re auth user --- ', response)
                let userPermStatus = response.headers.get('userPermissionStatus')
                if (response.body.loggedInStatus == 'true' && userPermStatus == 'allow') {
                    let resHeader = response.headers.get('Set-Cookie')
                    let cookie = resHeader.toString()
                    appSettings.setString('cookie', cookie)
                }
        })
    }

    loginWithEmail(email, cookie): Observable<any> {
        const route = `https://app.chaikinanalytics.com/CPTRestSecure/app/user/login`;
        const options = {
            params: {deviceId: email},
            withCredentials: true,
            headers: {'Cookie': cookie}
        }
        return this.api.getJSON(route, options)
    }

    logout(cookie): Observable<any> {
        const route = `https://app.chaikinanalytics.com/CPTRestSecure/app/session/killsessions`
        const options = {headers: {'Cookie': cookie}};
        return this.api.getJSON(route, options)
    }

    resetPassword(cookie: string, email: string): Observable<any> {
        const route = `https://app.chaikinanalytics.com/chaikin-is/password_reset`
        const options = {params: {'email': email}, headers: {'Cookie': cookie}};
        return this.api.getJSON(route, options)
    }

    ISFTcreateContact(email: string): Observable<any> {
        const route = 'https://app.chaikinanalytics.com/chaikin-is/register/mobile';
        let d = new FormData();
        d.append('email', email);
        d.append('secret', 'wMqYVbahgTodWNPCSWsZVnNV7NUkVSp3');
        return this.http.post(route, d)
    }

    ISFTaddMobileTag(email: string): Observable<any> {
        const route = 'https://app.chaikinanalytics.com/chakin-is/register/existing';
        let d = new FormData();
        d.append('email', email);
        d.append('secret', 'wMqYVbahgTodWNPCSWsZVnNV7NUkVSp3');
        return this.http.post(route, d)
    }

    registerNewUser(email: string, pW: string) {
        const route = `https://app.chaikinanalytics.com/CPTRestSecure/app/user/register`;
        let d = new FormData();
        d.append('token', '0ff55166ce04d373f092f47bad78a5b8');
        d.append('email', email);
        d.append('password', pW);
        d.append('version', '1.0');
        d.append('environment', 'mobile');
        return this.http.post(route, d);
    }

    setInitialNewUserRole(email: string) {
        const route = 'https://app.chaikinanalytics.com/CPTRestSecure/app/user/setRole';
        let d = new FormData();
        d.append('uuid', email);
        d.append('role', 'PFDFree');
        d.append('token', '0FF55166CE04D373F092F47BAD78A5B8')
        return this.http.post(route, d)
    }
}
