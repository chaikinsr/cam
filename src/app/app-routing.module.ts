import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { Routes } from "@angular/router";
import { LoginComponent } from "~/app/login/login.component";
import { OnboardComponent } from "~/app/onboard/onboard.component";
import { SettingsComponent } from "~/app/settings/settings.component";
const TabsMod = '~/app/tabs/tabs.module#TabsModule';

const routes: Routes = [
    { path: '', redirectTo: '/login', pathMatch: 'full' },
    { path: 'login', component: LoginComponent },
    { path: 'onboard', component: OnboardComponent },
    { path: 'settings', component: SettingsComponent },
    { path: 'nav', loadChildren: TabsMod }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes, { enableTracing: false })],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
