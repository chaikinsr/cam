import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {NativeScriptCommonModule} from 'nativescript-angular/common';
import {ItemDetailComponent} from "~/app/item/item-detail.component";
import {EtfDetailComponent} from "~/app/etf-detail/etf-detail.component";
import {NativeScriptUIChartModule} from "nativescript-ui-chart/angular";
import {NativeScriptUIListViewModule} from "nativescript-ui-listview/angular";
import {SettingsComponent} from "~/app/settings/settings.component";
import {ListEtfHoldingsComponent} from "~/app/list-etf-holdings/list-etf-holdings.component";

@NgModule({
    declarations: [
        ItemDetailComponent,
        EtfDetailComponent,
        SettingsComponent,
        ListEtfHoldingsComponent
    ],
    imports: [
        NativeScriptCommonModule,
        NativeScriptUIChartModule,
        NativeScriptUIListViewModule
    ],
    schemas: [NO_ERRORS_SCHEMA],
    entryComponents: [
        ListEtfHoldingsComponent
    ],
    exports: [
        ItemDetailComponent,
        EtfDetailComponent,
        SettingsComponent,
        ListEtfHoldingsComponent,
        NativeScriptCommonModule,
        NativeScriptUIChartModule,
        NativeScriptUIListViewModule
    ]
})
export class SharedModule {
}
