import {Component, ElementRef, NgZone, OnInit, ViewChild} from '@angular/core';
import {RouterExtensions} from "nativescript-angular";
import {ActivatedRoute} from "@angular/router";
import {ApiService} from "~/app/api.service";
import * as appSettings from "tns-core-modules/application-settings";
import { MultiSelect, AShowType } from "nativescript-multi-select";
import { MSOption } from "nativescript-multi-select";
import { DropDown, SelectedIndexChangedEventData, ValueList} from "nativescript-drop-down";

@Component({
  selector: 'ns-discover',
  templateUrl: './discover.component.html',
  styleUrls: ['./discover.component.scss'],
  moduleId: module.id,
})
export class DiscoverComponent implements OnInit {

    public user: any = {};
    private multiSelect: MultiSelect;
    private predefinedItems: Array<any>;
    public selectedItems: Array<any>;
    public startingUniverse: Array<any>;
    public powerGaugeRating: Array<any>;
    show: any = {};
    public selectedIndex = 1;
    public items: Array<string>;

    @ViewChild('dd', { static: true })
    dropDown: ElementRef;


    constructor(private router: RouterExtensions,
                private route: ActivatedRoute,
                public api: ApiService,
                private zone: NgZone) {
        this.multiSelect = new MultiSelect();
    }

    ngOnInit() {
        this.show.exploreScreener = true;
        this.show.exploreDiscover = true;
        this.show.exploreIdeas = true;
        this.show.exploreEarnings = true;
        this.user.cookie = appSettings.getString('cookie');

        // this.api.getInitialData(this.user.cookie).subscribe(res => {
        //
        //     let startingUniverse = res[0]['Starting Universe'][0]['factor'];
        //     this.startingUniverse = startingUniverse;
        //
        //     let powerGaugeRating = res[1]['Power Gauge Rating'][0]['factor'];
        //     this.powerGaugeRating = powerGaugeRating;
        //
        //     let priceTrend = res[8]['Price Levels'][0]['factor'];
        //     let chaikinMoneyFlow = res[9]['Chaikin Money Flow'][0]['factor'];
        //     let marketCap = res[12]['Market Cap'][0]['factor'];
        //     let divYield = res[12]['Div Yield'][0]['factor'];
        //     let earningsDate = res[14]['Earnings Report Date'][0]['factor'];
        //     let beta = res[18]['Beta'][0]['factor'];
        // })
    }

    goTo(route) {
        console.log('go to:', route)
        this.router.navigate([route], {
            relativeTo: this.route,
            transition: {
                name: 'fade',
                duration: 200
            }
        }).then(
            res => console.log('SUCCESS!', res),
            err => console.error('ERROR!', err)
        )
    }

    public onSelectTapped(): void {
        console.log('ready', this.startingUniverse)

        const options: MSOption = {
            title: 'Select Starting Universe',
            selectedItems: this.predefinedItems,
            items: this.startingUniverse,
            bindValue: 'id',
            displayLabel: 'name',
            onConfirm: selectedItems => {
                this.zone.run(() => {
                    this.selectedItems = selectedItems;
                    this.predefinedItems = selectedItems;
                    console.log("SELECTED ITEMS => ", selectedItems);
                })
            },
            onItemSelected: selectedItem => {
                console.log("SELECTED ITEM => ", selectedItem);
            },
            onCancel: () => {
                console.log('CANCEL');
            },
            // android: {
            //     titleSize: 25,
            //     cancelButtonTextColor: "#252323",
            //     confirmButtonTextColor: "#70798C",
            // },
            ios: {
                cancelButtonBgColor: "#252323",
                confirmButtonBgColor: "#70798C",
                cancelButtonTextColor: "#ffffff",
                confirmButtonTextColor: "#ffffff",
                showType: AShowType.TypeBounceIn
            }
        };

        this.multiSelect.show(options);
    }

    public onchange(args: SelectedIndexChangedEventData) {
        console.log(`Drop Down selected index changed from ${args.oldIndex} to ${args.newIndex}`);
    }

    public onopen() {
        console.log("Drop Down opened.");
    }

    public onclose() {
        console.log("Drop Down closed.");
    }

    get getStarting() {
        return this.startingUniverse;
    }

    toggleExploreScreener() {
        this.show.exploreScreener = !this.show.exploreScreener;
    }
    toggleExploreDiscover() {
        this.show.exploreDiscover = !this.show.exploreDiscover;
    }
    toggleExploreIdeas() {
        this.show.exploreIdeas = !this.show.exploreIdeas;
    }
    toggleExploreEarnings() {
        this.show.exploreEarnings = !this.show.exploreEarnings;
    }

}
