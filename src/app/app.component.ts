import { Component, OnInit, AfterViewInit, ChangeDetectorRef } from "@angular/core";
import { RouterExtensions } from "nativescript-angular";
import { HttpLoaderService } from "~/app/http-loader.service";

@Component({
    selector: "ns-app",
    moduleId: module.id,
    templateUrl: "./app.component.html",
})
export class AppComponent implements AfterViewInit, OnInit {

    public UIImage: any;
    public tabSelectedIndex: number;
    public tabSelectedIndexResult: string;
    private _mainContentText: string;

    constructor(private router: RouterExtensions,
                private _changeDetectionRef: ChangeDetectorRef,
                public loaderService: HttpLoaderService) {
        this.tabSelectedIndex = 0;
        this.tabSelectedIndexResult = 'Profile Tab (tabSelectedIndex = 0 )';
    }

    ngAfterViewInit() {
        this._changeDetectionRef.detectChanges();
    }

    ngOnInit(): void {
        console.log(' < --- APP COMP INIT --- > ?')

        // get JSON Perms

        let userPerms = {
            marketSurvivalGuide: false,
            morningInsights: false,
            marketInsights: false,
            premium: false
        }

        // IF USER BUYS ANY, BECOMES TRUE

        // IF CA USER, MORNING, MARKET INSIGHTS AND PREMIUM BECOME 'CA'

        // IF ANY USER LOGS ON THAT IS NOT CA BUY HAS THESE ENTITLEMENTS (CANCELED MEMBER) THAT SETTING BECOMES NULL

        // IF PULSE USER, MARKET INSIGHTS IS SET TO 'PP', ALL ELSE IS FALSE
    }

    get mainContentText() {
        return this._mainContentText;
    }

    set mainContentText(value: string) {
        this._mainContentText = value;
    }
}
