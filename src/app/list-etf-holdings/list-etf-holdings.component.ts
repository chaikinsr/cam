import {Component, OnInit, ViewContainerRef} from '@angular/core'
import {ActivatedRoute} from "@angular/router"
import {ItemService} from "~/app/item/item.service"
import {ApiService} from "~/app/api.service"
import {ModalDialogOptions, ModalDialogParams, ModalDialogService, RouterExtensions} from "nativescript-angular"
import * as appSettings from "tns-core-modules/application-settings"
import {ItemDetailComponent} from "~/app/item/item-detail.component"

@Component({
    selector: 'ns-list-etf-holdings',
    templateUrl: './list-etf-holdings.component.html',
    styleUrls: ['./list-etf-holdings.component.css']
})
export class ListEtfHoldingsComponent implements OnInit {

    user: any = {};
    items: any = {};
    list: any = {};

    constructor(private route: ActivatedRoute,
                private item: ItemService,
                private api: ApiService,
                private router: RouterExtensions,
                private modalService: ModalDialogService,
                private viewContainerRef: ViewContainerRef,
                private params: ModalDialogParams ) {
        this.items.ready = false;
    }

    ngOnInit() {
        console.log(' INIT --- LIST ETF HOLDINGS COMP ---')
        this.user = {
            listId: appSettings.getNumber('ListId'),
            listName: appSettings.getString('ListName'),
            cookie: appSettings.getString('cookie'),
            uid: appSettings.getString('uid'),
            starred: appSettings.getNumber('starred')
        }
        this.prepareList()
    }

    prepareList() {
        this.item.getListInfo(this.user.uid, this.user.listId, this.user.cookie)
            .subscribe(info => {
                let rawArr = info.symbols
                let listArr = []
                rawArr.forEach(i => {
                    let sortPGR = 0
                    if (i.PGR == 3 && i.PGR == i.raw_PGR) sortPGR = 3
                    if (3 == i.PGR && i.PGR < i.raw_PGR) sortPGR = 3.5
                    if (3 == i.PGR && i.PGR > i.raw_PGR) sortPGR = 2.5
                    if (sortPGR == 0) sortPGR = i.PGR
                    listArr.push({
                        symbol: i.symbol,
                        name: i.name,
                        last: i.Last,
                        change: i.Change,
                        percent: i['Percentage '],
                        pgr: i.PGR,
                        raw: i.raw_PGR,
                        sortPGR: sortPGR,
                        etf: i.is_etf_symbol
                    })
                })
                this.items.powerbar = info.PowerBar.split(',')
                this.items.symbols = listArr
                this.sort('sortPGR')
                this.items.ready = true
            })
    }

    getListName() {
        return this.user.listName
    }

    getColumns() {
        let bull: number = parseInt(this.items.powerbar[2])
        let bear: number = parseInt(this.items.powerbar[0])
        if (bull > bear) {
            let r = bull/bear
            let floor = Math.floor(r)
            let cols = floor + '*, 40, *'
            return cols
        } else {
            let r = bear/bull
            let floor = Math.floor(r)
            let cols = '*, 40, ' + floor + '*'
            return cols
        }
    }

    close() {
        this.params.closeCallback()
    }

    bulls() {
        return this.items.powerbar[2]
    }

    neutrals() {
        return this.items.powerbar[1]
    }

    bears() {
        return this.items.powerbar[0]
    }

    get getList() {
        return this.items.symbols
    }

    pgrRating(pgr, rawPGR) {
        return this.item.getPGRiconAllRAWconfig(pgr, rawPGR, false)
    }

    navToDetails(stock) {
        appSettings.setString('symbol', stock.symbol);
        const options: ModalDialogOptions = {
            viewContainerRef: this.viewContainerRef,
            fullscreen: true,
            context: {}
        }
        this.modalService.showModal(ItemDetailComponent, options)
    }

    sort(by) {
        this.list.toggle = !this.list.toggle;
        this.list.carat = by;
        this.list.toggle
            ? this.items.symbols.sort((a,b) => {
                let sorted = a[by] < b[by] ? 1 : a[by] > b[by] ? -1 : 0;
                return sorted;
            })
            : this.items.symbols.sort((a,b) => {
                let sorted = b[by] < a[by] ? 1 : b[by] > a[by] ? -1 : 0;
                return sorted;
            })
    }
}
