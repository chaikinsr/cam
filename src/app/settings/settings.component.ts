import {Component, OnInit} from '@angular/core'
import {RouterExtensions} from 'nativescript-angular'
import * as appSettings from 'tns-core-modules/application-settings'
import {UserService} from '~/app/user.service'
import * as utils from 'tns-core-modules/utils/utils'
import * as dialogs from 'tns-core-modules/ui/dialogs'
@Component({
    selector: 'ns-settings',
    templateUrl: './settings.component.html',
    moduleId: module.id,
})
export class SettingsComponent implements OnInit {

    user: any = {};
    products: any = [];
    manageSubs: boolean = false;
    unsubscribe: any = {}
    ready: boolean = false

    constructor(private router: RouterExtensions,
                public service: UserService,
                private userService: UserService) {
    }

    ngOnInit() {
        this.user = {
            email: appSettings.getString('email'),
            cookie: appSettings.getString('cookie'),
            uid: appSettings.getString('uid'),
            starred: appSettings.getNumber('starred')
        }
        this.products = [
            {
                id: 'com.chaikin.cam.Morning_Insights',
                name: 'Morning Insights',
                show: appSettings.getBoolean('Morning Insights')
            },
            {
                id: 'com.chaikin.cam.Market_Insights',
                name: 'Market Insights',
                show: appSettings.getBoolean('Market Insights')
            },
            {
                id: 'com.chaikin.cam.Market_Survival_Guide',
                name: 'Market Survival Guide',
                show: appSettings.getBoolean('Market Survival')
            }
        ]
        this.loadMyPerms()
    }

    goBack() {
        this.router.back()
    }

    goToLogin() {
        this.router.navigate(['/login'], {
            transition: {
                name: 'fade',
                duration: 300,
                curve: 'ease'
            }
        })
    }

    loadMyPerms() {
        this.userService.getUserPerms(this.user.uid).subscribe(perms => {
            let permsArr = perms.split(',')
            let keys = [],
                vals = []
            permsArr.forEach(item => {
                let key = item.split(':')
                keys.push(key[0].trim())
                vals.push(key[1].trim())
            })
            let mkt = vals[0] == 'true',
                mor = vals[1] == 'true',
                mar = vals[2] == 'true',
                pre = vals[3] == 'true',
                userPerms = {
                    marketSurvivalGuide: mkt,
                    morningInsights: mor,
                    marketInsights: mar,
                    premium: pre
                }
            this.user.perms = userPerms
            this.ready = true
        })
    }

    remove(insight) {
        dialogs.confirm({
            title: 'Removing Subscription ' + insight.name,
            message: 'Are you sure that you wish to unsubscribe from ' + insight.name + '?',
            okButtonText: 'Unsubscribe',
            cancelButtonText: "Cancel",
        }).then(function (result) {
            console.log('UNSUBSCRIBE FROM ' + insight.name + ' ----> ' + result)
            console.log('index of result', this.products.indexOf(result))
        });
    }

    public signOut() {
        console.log(' --- Good bye --- ')
        this.service.logout(this.user.cookie)
            .subscribe(res => {
                // appSettings.clear()
                appSettings.setBoolean('LoggedInBefore', true)
                this.router.navigate(['/login'], {
                    transition: {
                        name: 'fade',
                        duration: 500,
                        curve: 'ease'
                    },
                    clearHistory: true
                })
        })
    }

    linkOutWebinars() {
        utils.openUrl('https://www.chaikinanalytics.com/webinars/')
    }

    linkOutBlog() {
        utils.openUrl('https://www.chaikinanalytics.com/blog/')
    }

    linkOutAboutCA() {
        utils.openUrl('https://www.chaikinanalytics.com/analytics/')
    }

    linkOutAboutPG() {
        utils.openUrl('https://www.chaikinanalytics.com/power-gauge-rating/')
    }

    linkOutFAQ() {
        utils.openUrl('https://www.chaikinanalytics.com/mobile-guide/')
    }

    linkOutDisclaimer() {
        utils.openUrl('https://www.chaikinanalytics.com/disclaimer')
    }

    linkOutPrivacy() {
        utils.openUrl('https://www.chaikinanalytics.com/privacy')
    }

    linkOutTOS() {
        utils.openUrl('https://www.chaikinanalytics.com/terms-and-conditions/')
    }

    linkOutAttributions() {
        utils.openUrl('https://www.chaikinanalytics.com/attributions/')
    }
}
