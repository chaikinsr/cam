import {Component, ElementRef, NgZone, OnInit, ViewChild} from '@angular/core'
import {SelectedIndexChangedEventData, ValueList} from "nativescript-drop-down"
import {MultiSelect, MSOption, AShowType} from "nativescript-multi-select"
import {RouterExtensions} from "nativescript-angular"
import {ActivatedRoute} from "@angular/router"
import {ApiService} from "~/app/api.service"
import * as appSettings from "tns-core-modules/application-settings"

@Component({
    selector: 'ns-screener-menu',
    templateUrl: './screener-menu.component.html',
    moduleId: module.id,
})
export class ScreenerMenuComponent implements OnInit {

    user: any = {};
    show: any = {};
    multiSelect: MultiSelect; // actual multi select menu
    selectedItems: Array<any>;
    startingUniverses: ValueList<any>;
    screeningCriteria: ValueList<any>;
    // pgrRatingCriteria: ValueList<any>;
    priceTrendCriteria: ValueList<any>;
    priceRangeMinCriteria: ValueList<any>;
    priceRangeMaxCriteria: ValueList<any>;
    cmfCriteria: ValueList<any>;
    mktCriteria: ValueList<any>;
    divCriteria: ValueList<any>;
    earningsCriteria: ValueList<any>;
    betaCriteria: ValueList<any>;
    selectedIndex = 1;
    selectedScreeningIndex = 1;
    selectedPriceTrendIndex = 1;
    selectedPriceMinIndex = 1;
    selectedPriceMaxIndex = 1;
    selectedCMFIndex = 1;
    selectedMktCapIndex = 1;
    selectedDivIndex = 1;
    selectedEarningsIndex = 1;
    selectedBetaIndex = 1;
    selectedScreeningCriteria: any = {};
    pgrMultiSelectOptions: any = {};
    pgrSelectedValues: any = {};
    @ViewChild('ddStartingUniv', {static: true}) dropDown: ElementRef<any>; // starting universe
    @ViewChild('ddScreeningCriteria', {static: true}) screenDropDown: ElementRef<any>; // screening criteria
    @ViewChild('ddPriceTrend', {static: true}) priceTrendDropDown: ElementRef<any>; // price trend
    @ViewChild('ddPriceMin', {static: true}) priceMinDropDown: ElementRef<any>;
    @ViewChild('ddPriceMax', {static: true}) priceMaxDropDown: ElementRef<any>;
    @ViewChild('ddCMF', {static: true}) cmfDropDown: ElementRef<any>;
    @ViewChild('ddMktCap', {static: true}) mktCapDropDown: ElementRef<any>;
    @ViewChild('ddDivYield', {static: true}) divYieldDropDown: ElementRef<any>;
    @ViewChild('ddEarnings', {static: true}) earningsDropDown: ElementRef<any>;
    @ViewChild('ddBeta', {static: true}) betaDropDown: ElementRef<any>;

    constructor(private router: RouterExtensions,
                private route: ActivatedRoute,
                public api: ApiService,
                private zone: NgZone) {
        this.multiSelect = new MultiSelect();
    }

    get showSelectedPGRCriteria() {
        if (this.selectedItems) {
            return this.pgrSelectedValues
        }
    }

    // public prepareScreen() {
    //     this.show.screen = false;
    //     this.show.priceTrend = false;
    //     this.show.priceRange = false;
    //     this.show.cmf = false;
    //     this.show.marketCap = false;
    //     this.show.divYield = false;
    //     this.show.earnings = false;
    //     this.show.beta = false;
    //
    //     this.api.getInitialData(this.user.cookie).subscribe(res => {
    //
    //         // for (let keys in res) console.log('major', res[keys])
    //         // STARTING UNIV SINGLE SELECT - VALUE LIST
    //
    //         let startingUniverse = res[0]['Starting Universe'][0]['factor'];
    //         let betterBox = new ValueList<any>();
    //
    //         startingUniverse.map(uni => {
    //             betterBox.push({
    //                 value: uni.id,
    //                 display: uni.name
    //             })
    //         });
    //         this.startingUniverses = betterBox; // starting universes
    //
    //
    //         // PGR RATING MULTI SELECT - OBJ NAME/VALUE
    //
    //         let powerGaugeRating = res[1]['Power Gauge Rating'][0]['factor'];
    //         let pgrBox = [];
    //
    //         powerGaugeRating.map(rating => {
    //             pgrBox.push({
    //                 name: rating.name,
    //                 value: rating.id
    //             })
    //         });
    //         this.pgrMultiSelectOptions = pgrBox;
    //
    //
    //         // PRICE TREND SINGLE SELECT
    //
    //         let priceTrend = res[8]['Price Levels'][0]['factor'];
    //         let priceTrendArr = new ValueList();
    //
    //         priceTrend.map(trend => {
    //             priceTrendArr.push({
    //                 value: trend.id,
    //                 display: trend.name
    //             })
    //         })
    //
    //         this.priceTrendCriteria = priceTrendArr;
    //
    //
    //         // PRICE RANGE SINGLE SELECT OF LOW AND HIGH !!
    //
    //         let priceRangeMin = res[19]['Min Price'][0]['factor'];
    //         let priceRangeMax = res[20]['Max Price'][0]['factor'];
    //
    //         let priceMinArr = new ValueList();
    //         let priceMaxArr = new ValueList();
    //
    //         priceRangeMin.map(price => {
    //             priceMinArr.push({
    //                 value: price.id,
    //                 display: price.name
    //             })
    //         })
    //
    //         this.priceRangeMinCriteria = priceMinArr;
    //
    //         priceRangeMax.map(price => {
    //             priceMaxArr.push({
    //                 value: price.id,
    //                 display: price.name
    //             })
    //         })
    //
    //         this.priceRangeMaxCriteria = priceMaxArr;
    //
    //
    //         // CHAIKIN MONEY FLOW SINGLE SELECT
    //
    //         let chaikinMoneyFlow = res[9]['Chaikin Money Flow'][0]['factor'];
    //         let cmfArr = new ValueList();
    //
    //         chaikinMoneyFlow.map(cmf => {
    //             cmfArr.push({
    //                 value: cmf.id,
    //                 display: cmf.name
    //             })
    //         })
    //
    //         this.cmfCriteria = cmfArr;
    //
    //
    //         // MARKET CAP SINGLE SELECT
    //
    //         let marketCap = res[12]['Market Cap'][0]['factor'];
    //         let marketArr = new ValueList();
    //
    //         marketCap.map(mkt => {
    //             marketArr.push({
    //                 value: mkt.id,
    //                 display: mkt.name
    //             })
    //         })
    //
    //         this.mktCriteria = marketArr;
    //
    //
    //         // DIV YIELD SINGLE SELECT
    //
    //         let divYield = res[13]['Div Yield'][0]['factor'];
    //         let divArr = new ValueList();
    //
    //         divYield.map(div => {
    //             divArr.push({
    //                 value: div.id,
    //                 display: div.name
    //             })
    //         })
    //
    //         this.divCriteria = divArr;
    //
    //
    //         // EARNINGS DATE SINGLE SELECT
    //
    //         let earningsDate = res[14]['Earnings Report Date'][0]['factor'];
    //         let earningsArr = new ValueList();
    //
    //         earningsDate.map(date => {
    //             earningsArr.push({
    //                 value: date.id,
    //                 display: date.name
    //             })
    //         })
    //
    //         this.earningsCriteria = earningsArr;
    //
    //
    //         // BETA SINGLE SELECT
    //
    //         let beta = res[18]['Beta'][0]['factor'];
    //         let betaArr = new ValueList();
    //
    //         beta.map(beta => {
    //             betaArr.push({
    //                 value: beta.id,
    //                 display: beta.name
    //             })
    //         })
    //
    //         this.betaCriteria = betaArr;
    //     })
    //
    //     let criteria = new ValueList();
    //
    //     let test = [
    //         { value: '0', display: 'Power Gauge Rating' },
    //         { value: '1', display: 'Price Trend' },
    //         { value: '2', display: 'Price Range' },
    //         { value: '3', display: 'Chaikin Money Flow' },
    //         { value: '4', display: 'Market Cap' },
    //         { value: '5', display: 'Dividend Yield' },
    //         { value: '6', display: 'Earnings Date' },
    //         { value: '7', display: 'Beta' }
    //     ];
    //
    //     test.forEach(function (idk) {
    //         criteria.push({
    //             value: idk.value,
    //             display: idk.display
    //         })
    //     })
    //     this.screeningCriteria = criteria;
    // }

    ngOnInit() {
        this.user.cookie = appSettings.getString('cookie');
        // this.prepareScreen();
    }

    universeChange(args: SelectedIndexChangedEventData) {
        console.log('uni went from ', args.oldIndex)
        console.log('uni to this ', args.newIndex)
    }

    universeOpen() {
        console.log('choosing screening universe')
    }

    universeClose() {
        console.log('closed universe screener')
    }

    priceTrendChange(args: SelectedIndexChangedEventData) {
        console.log('price trend went from ', args.oldIndex)
        console.log('to this ', args.newIndex)
    }

    priceTrendOpen() {
        console.log('choosing price trend')
    }

    priceTrendClose() {
        console.log('closed price trend')
    }

    priceMinChange(args: SelectedIndexChangedEventData) {
        console.log('price trend went from ', args.oldIndex)
        console.log('to this ', args.newIndex)
    }

    priceMinOpen() {
        console.log('choosing price trend')
    }

    priceMinClose() {
        console.log('closed price trend')
    }

    priceMaxChange(args: SelectedIndexChangedEventData) {
        console.log('price trend went from ', args.oldIndex)
        console.log('to this ', args.newIndex)
    }

    priceMaxOpen() {
        console.log('choosing price trend')
    }

    priceMaxClose() {
        console.log('closed price trend')
    }

    cmfChange(args: SelectedIndexChangedEventData) {
        console.log('cmf went from ', args.oldIndex)
        console.log('to this ', args.newIndex)
    }

    cmfOpen() {
        console.log('choosing cmf')
    }

    cmfClose() {
        console.log('closed cmf')
    }

    mktCapChange(args: SelectedIndexChangedEventData) {
        console.log('cmf went from ', args.oldIndex)
        console.log('to this ', args.newIndex)
    }

    mktCapOpen() {
        console.log('choosing cmf')
    }

    mktCapClose() {
        console.log('closed cmf')
    }

    divYieldChange(args: SelectedIndexChangedEventData) {
        console.log('cmf went from ', args.oldIndex)
        console.log('to this ', args.newIndex)
    }

    divYieldOpen() {
        console.log('choosing cmf')
    }

    divYieldClose() {
        console.log('closed cmf')
    }

    earningsChange(args: SelectedIndexChangedEventData) {
        console.log('cmf went from ', args.oldIndex)
        console.log('to this ', args.newIndex)
    }

    earningsOpen() {
        console.log('choosing cmf')
    }

    earningsClose() {
        console.log('closed cmf')
    }

    betaChange(args: SelectedIndexChangedEventData) {
        console.log('cmf went from ', args.oldIndex)
        console.log('to this ', args.newIndex)
    }

    betaOpen() {
        console.log('choosing cmf')
    }

    betaClose() {
        console.log('closed cmf')
    }

    onScreeningCriteriaChange(args: SelectedIndexChangedEventData) {
        this.selectedScreeningCriteria = this.screeningCriteria['_array'][args.newIndex];
        this.show.screen = true;
        switch (this.selectedScreeningCriteria['value']) {
            case '0':
                console.log('POWER GAUGE OPEN SCROLLY')
                const options: MSOption = {
                    title: "Please Select Which PGR Ratings You'd Like to See",
                    items: this.pgrMultiSelectOptions,
                    bindValue: 'value',
                    displayLabel: 'name',
                    onConfirm: selectedItems => {
                        this.zone.run(() => {
                            this.selectedItems = selectedItems;
                            // this.predefinedItems = selectedItems;
                            console.log("SELECTED ITEMS => ", selectedItems);
                            let PGRoptions = this.pgrMultiSelectOptions; // all the options
                            this.pgrSelectedValues = this.selectedItems.map(item => { // go through selected options
                                let pgrSelection = PGRoptions.find(function (rating) { // find within all pgr options
                                    return rating.value == item // the names matching these ids
                                })
                                return pgrSelection;
                            })
                        })
                    },
                    onItemSelected: selectedItem => {
                        console.log("SELECTED ITEM => ", selectedItem);
                    },
                    onCancel: () => {
                        console.log('CANCEL');
                    },
                    android: {
                        titleSize: 25,
                        cancelButtonTextColor: "#252323",
                        confirmButtonTextColor: "#70798C",
                    },
                    ios: {
                        cancelButtonBgColor: "#252323",
                        confirmButtonBgColor: "#70798C",
                        cancelButtonTextColor: "#ffffff",
                        confirmButtonTextColor: "#ffffff",
                        showType: AShowType.TypeBounceIn
                    }
                };
                this.screenDropDown.nativeElement.close();
                this.multiSelect.show(options);
                break;
            case '1':
                // close this menu
                this.screenDropDown.nativeElement.close();
                // open single select
                this.show.priceTrend = true;
                break;
            case '2':
                console.log('PRICE RANGE OPEN SCROLL')
                this.screenDropDown.nativeElement.close();
                this.show.priceRange = true;
                break;
            case '3':
                console.log('CMF OPEN SCROLL')
                this.screenDropDown.nativeElement.close();
                this.show.cmf = true;
                break;
            case '4':
                console.log('MARKET CAP OPEN SCROLL');
                this.screenDropDown.nativeElement.close();
                this.show.marketCap = true;
                break;
            case '5':
                console.log('DIV YIELD OPEN SCROLLY')
                this.screenDropDown.nativeElement.close();
                this.show.divYield = true;
                break;
            case '6':
                console.log('EARNINGS DATE OPEN SCROLLY')
                this.screenDropDown.nativeElement.close();
                this.show.earnings = true;
                break;
            case '7':
                console.log('BETA OPEN SCROLLY')
                this.screenDropDown.nativeElement.close();
                this.show.beta = true;
                break;
            default:
                console.log('ERROR ...?')
        }
    }

    screenCritOpen() {
        console.log("screen crit opened.");
    }

    screenCritClose() {
        console.log("screen crit closed.");
    }

    openScreeningCrit() {
        this.show.screen = true;
        // this.screenDropDown.nativeElement.open();
    }

    openAnotherScreenCrit() {
        this.screenDropDown.nativeElement.open();
    }
}
