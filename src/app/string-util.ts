import { isIOS } from "tns-core-modules/platform";

declare var NSString: any;
declare var NSUTF8StringEncoding: any;
declare var java: any;
declare var android: any;

export function base64Encode(value) {
    console.log('STRING UTILS!')
    if (isIOS) {
        let text = NSString.stringWithString(value);
        let data = text.dataUsingEncoding(NSUTF8StringEncoding);
        return data.base64EncodedStringWithOptions(0);
    } else {
        let text = new java.lang.String(value);
        let data = text.getBytes("UTF-8");
        return android.util.Base64.encodeToString(data, android.util.Base64.DEFAULT);
    }
}
