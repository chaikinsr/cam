import {Component, OnInit} from '@angular/core'
import {ActivatedRoute} from "@angular/router"
import {RouterExtensions} from "nativescript-angular"
import * as appSettings from "tns-core-modules/application-settings"
import {ItemService} from "~/app/item/item.service"
import {ApiService} from "~/app/api.service"

@Component({
    selector: 'ns-list-list',
    templateUrl: './list-list.component.html',
    styleUrls: ['./list-list.component.scss'],
    moduleId: module.id,
})
export class ListListComponent implements OnInit {

    user: any = {}
    lists: any = {}
    config: any = {}

    constructor(private route: ActivatedRoute,
                private router: RouterExtensions,
                private item: ItemService) {
    }

    ngOnInit() {
        console.log(' --- INIT: LIST OF LISTS COMP --- ')
        this.user = {
            cookie: appSettings.getString('cookie'),
            uid: appSettings.getString('uid'),
            starred: appSettings.getNumber('starred'),
            type: appSettings.getString('usertype')
        }
        this.loadMyPerms()
        this.item.getAuthorizedLists(this.user.uid, this.user.cookie)
            .subscribe(lists => {
                this.lists = lists[0]['User Lists']
            })
    }

    loadMyPerms() {
        let userPerms = this.user.perms
        let userType = appSettings.getString('usertype')
        if (userType == 'CA') {
            userPerms = {
                marketSurvivalGuide: false,
                morningInsights: true,
                marketInsights: true,
                prem: true
            }
        }
        if (userType == 'PP') {
            userPerms = {
                marketSurvivalGuide: false,
                morningInsights: true,
                marketInsights: false,
                prem: true
            }
        }
        this.config.perms = userPerms
    }

    navigateTo(route) {
        appSettings.setNumber('ListId', this.user.starred)
        appSettings.setString('ListName', route)
        this.router.navigate(['../list-detail'], {
            relativeTo: this.route,
            transition: {
                name: 'fade',
                duration: 100
            }
        })
    }

    navigateToMyList(route) {
        let myList = appSettings.getNumber('UserListId');
        appSettings.setNumber('ListId', myList)
        appSettings.setString('ListName', 'My Stocks')
        this.router.navigate(['../list-detail'], {
            relativeTo: this.route,
            transition: {
                name: 'fade',
                duration: 100
            }
        })
    }

    navigateToSPDR(route) {
        appSettings.setNumber('ListId', 1291384);
        appSettings.setString('ListName', route);
        this.router.navigate(['../list-detail'], {
            relativeTo: this.route,
            transition: {
                name: 'fade',
                duration: 100
            }
        })
    }

    navigateToSubmenu(route) {
        appSettings.setString('ListTitle', route);
        this.router.navigate(['../submenu'], {
            relativeTo: this.route,
            transition: {
                name: 'fade',
                duration: 100
            }
        })
    }

    navSPDRtoPowBar(route) {
        appSettings.setString('ListTitle', route)
        appSettings.setNumber('ListId', 1291384);
        this.router.navigate(['../powbar'], {
            relativeTo: this.route,
            transition: {
                name: 'fade',
                duration: 100
            }
        })
    }

    navToPowBar(route) {
        appSettings.setString('ListTitle', route)
        this.router.navigate(['../powbar'], {
            relativeTo: this.route,
            transition: {
                name: 'fade',
                duration: 100
            }
        })
    }

    openSettings() {
        this.router.navigate(['../settings'], {
            relativeTo: this.route,
            transition: {
                name: 'slideRight',
                duration: 300
            }
        })
    }

    goToLL() {
        this.router.navigate(['../group'], {
            relativeTo: this.route,
            transition: {
                name: 'fade',
                duration: 100
            }
        })
    }
}
