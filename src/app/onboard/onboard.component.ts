import {Component, OnInit} from '@angular/core';
import {RouterExtensions} from "nativescript-angular";
import {setInterval} from "tns-core-modules/timer";

@Component({
  selector: 'ns-onboard',
  templateUrl: './onboard.component.html',
  moduleId: module.id,
})
export class OnboardComponent implements OnInit {

    constructor(private router: RouterExtensions) { }

    ngOnInit() {
        // this.startTimer()
    }

    goToMarket() {
        this.router.navigate(['nav/tabs'], {
            transition: {
                name: 'fade',
                duration: 300,
                curve: 'ease'
            },
            clearHistory: true
        })
    }

    startTimer() {
        setTimeout(() => {
            this.goToMarket()
        }, 5000)
    }
}
