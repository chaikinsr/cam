// import { Component, OnInit } from "@angular/core";
// import { ItemService } from "./item.service";
// import { UserService } from "~/app/user.service";
// import * as appSettings from "tns-core-modules/application-settings";
// import { ObservableArray } from "tns-core-modules/data/observable-array";
// import { RouterExtensions } from "nativescript-angular";
// import { TextField } from "tns-core-modules/ui/text-field";
//
// @Component({
//     selector: "ns-items",
//     moduleId: module.id,
//     templateUrl: "./items.component.html",
// })
// export class ItemsComponent implements OnInit {
//     items: any = {};
//     user: any = {};
//     search: any = {};
//     ticker: any = {};
//     UIImage: any;
//
//     constructor(private itemService: ItemService,
//                 private userService: UserService,
//                 private router: RouterExtensions) {
//         this.search.isOpen = false;
//         this.search.alertsOpen = false;
//     }
//
//     ngOnInit(): void {
//         console.log(' -- ITEMS COMPONENT INIT -- ')
//         this.user.email = appSettings.getString('email');
//         this.user.cookie = appSettings.getString('cookie');
//         this.userService.login(this.user.email, this.user.cookie)
//             .subscribe(
//                 res => {
//                     this.user.uid = res['UID'];
//                     this.user.userType = res['userType'];
//                     appSettings.setString('uid', this.user.uid)
//                     this.getListInfo(this.user.uid, this.user.cookie);
//                     this.getMarketData();
//                 },
//                 err => {
//                     this.user.error = err.name;
//                 }
//             )
//     }
//
//     get email(): string {
//         return this.user.email;
//     }
//
//     get stocklist(): ObservableArray<any> {
//         return this.items.stocklist
//     }
//
//     get alertlist(): ObservableArray<any> {
//         return this.items.alertedComps;
//     }
//
//     get marketTickerSymbol(): string {
//         return this.ticker.symbol;
//     }
//
//     get marketTickerLast(): string {
//         return this.ticker.last;
//     }
//
//     // get SPY, DJI and QQQ for today, resets 10s
//     getMarketData() {
//         this.itemService.initialMarketSectorData(this.user.cookie)
//             .subscribe(res => {
//                 const ind = res['market_indices']
//                 this.ticker.SPY = ind[0]
//                 this.ticker.DJI = ind[1]
//                 this.ticker.QQQ = ind[2]
//                 this.ticker.symbol = this.ticker.SPY['symbol']
//                 this.ticker.last = this.ticker.SPY['last']
//                 setInterval(() => {
//                     this.switchMarketTicker();
//                 }, 10000);
//             })
//     }
//
//     getListInfo(uid, cookie): void {
//         this.itemService.getAuthorizedLists(uid, cookie)
//             .subscribe(res => {
//                 this.user.list = res[0]['User Lists'];
//                 this.user.mainListId = res[0]['User Lists'][0]['list_id'];
//                 this.itemService.getListAlerts(uid, this.user.mainListId, cookie)
//                     .subscribe(res => {
//                         this.items.alerts = res;
//                         let alerted: any = [];
//                         for (let i=0; i< this.items.alerts['PGR'].length; i++) {
//                             alerted.push({
//                                 ticker: this.items.alerts['PGR'][i]['Symbol'],
//                                 text: this.items.alerts['PGR'][i]['Text'],
//                                 value: this.items.alerts['PGR'][i]['Value']
//                             })
//                         }
//                         for (let i=0; i<this.items.alerts['EarningSurprise'].length; i++) {
//                             alerted.push({
//                                 ticker: this.items.alerts['EarningSurprise'][i]['Symbol'],
//                                 text: this.items.alerts['EarningSurprise'][i]['Text'],
//                                 target: this.items.alerts['EarningSurprise'][i]['ConsensusEstimate'],
//                                 actual: this.items.alerts['EarningSurprise'][i]['ActualEPS']
//                             })
//                         }
//                         for (let i=0; i<this.items.alerts['EarningEstimate'].length; i++) {
//                             alerted.push({
//                                 ticker: this.items.alerts['EarningEstimate'][i]['Symbol'],
//                                 text: this.items.alerts['EarningEstimate'][i]['Text'],
//                                 target: this.items.alerts['EarningEstimate'][i]['MeanESTPreviousDay'],
//                                 actual: this.items.alerts['EarningEstimate'][i]['MeanESTCurrentDay']
//                             })
//                         }
//                         this.items.alertedComps = alerted;
//                         this.setupLanding();
//                     })
//                 }
//             );
//     }
//
//     setupLanding() {
//         this.itemService.getListInfo(this.user.uid, this.user.mainListId, this.user.cookie)
//             .subscribe(res => {
//                 this.items.stocklist = res['symbols'];
//                 this.items.powerbar = res.PowerBar;
//             })
//     };
//
//     switchMarketTicker() {
//         if (this.ticker.symbol == this.ticker.SPY['symbol']) {
//             this.ticker.symbol = this.ticker.DJI['symbol'];
//             this.ticker.last = this.ticker.DJI['last'];
//         } else if (this.ticker.symbol == this.ticker.DJI['symbol']) {
//             this.ticker.symbol = this.ticker.QQQ['symbol'];
//             this.ticker.last = this.ticker.QQQ['last'];
//         } else if (this.ticker.symbol = this.ticker.QQQ['symbol']) {
//             this.ticker.symbol = this.ticker.SPY['symbol'];
//             this.ticker.last = this.ticker.SPY['last'];
//         }
//     }
//
//     onTap(item){
//         appSettings.setString('symbol', item['symbol']);
//         appSettings.setString('name', item['name']);
//         let route = 'item/' + item['symbol'];
//         this.router.navigate([route], {
//             transition: {
//                 name: 'curl',
//                 duration: 200
//             }
//         });
//     }
//
//     sortByRating() {
//         this.items.sortRating = !this.items.sortRating;
//         this.items.sortRatingDirection = this.items.sortRating ? 1 : -1;
//         this.items.stocklist.sort((a,b) => {
//             if (a.PGR < b.PGR) return (-1 * this.items.sortRatingDirection);
//             else if (a.PGR > b.PGR) return this.items.sortRatingDirection;
//             else return 0;
//         })
//     }
//
//     sortByCompany() {
//         this.items.sortTicker = !this.items.sortTicker;
//         this.items.sortTickerDirection = this.items.sortTicker ? 1 : -1;
//         this.items.stocklist.sort((a,b) => {
//             if (a.symbol < b.symbol) return (-1 * this.items.sortTickerDirection);
//             else if (a.symbol > b.symbol) return this.items.sortTickerDirection;
//             else return 0;
//         })
//     }
//
//     sortByPrice() {
//         this.items.sortPrice = !this.items.sortPrice;
//         this.items.sortPriceDirection = this.items.sortPrice ? 1 : -1;
//         this.items.stocklist.sort((a,b) => {
//             if (a.Change < b.Change) return (-1 * this.items.sortPriceDirection);
//             else if (a.Change > b.Change) return this.items.sortPriceDirection;
//             else return 0;
//         })
//     }
//
//     showSearchInput() {
//         if (this.search.isOpen == false) {
//             this.search.isOpen = true
//         } else {
//             this.setupLanding();
//             this.search.isOpen = false;
//             delete this.search.query;
//             delete this.search.list;
//         }
//     }
//
//     onTextChange(args) {
//         let textField = <TextField>args.object;
//         this.search.query = textField.text;
//         this.getSymbolLookup();
//     }
//
//     onReturn(args) {
//         let textField = <TextField>args.object;
//         this.search.query = textField.text;
//         this.getSymbolLookup();
//     }
//
//     getSymbolLookup() {
//         this.itemService.symbolLookup(this.search.query)
//             .subscribe(res => {
//                 this.search.list = res;
//                 this.search.isOpen = true;
//             })
//     }
//
//     onSearchTap(args) {
//         this.onTap(args);
//     }
//
//     onAddTap(args) {
//         this.addStockIntoList(args, this.user.mainListId);
//         this.search.isOpen = false;
//         delete this.search.query;
//         delete this.search.list;
//     }
//
//     addStockIntoList(ticker, listId) {
//         this.itemService.addStockIntoList(ticker, this.user.uid, listId, this.user.cookie)
//             .subscribe(res => {
//                 console.log('added stock', res)
//                 this.setupLanding();
//             });
//     }
//
//     showAlerts() {
//         this.search.alertsOpen = !this.search.alertsOpen;
//     }
//
//     logout(): void {
//         this.router.navigate(['/home']);
//     }
//
//     tapAlert(arg): void {
//         appSettings.setString('ticker', arg.ticker)
//         appSettings.setString('text', arg.text)
//         appSettings.setNumber('value', arg.value)
//         this.router.navigate(['baseAlertDetail/' + arg.ticker]).then(res => { console.log('res', res)}, err => { console.log('err', err)})
//     }
// }
