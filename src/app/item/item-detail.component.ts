import {Component, OnInit, Input, ElementRef, ViewChild} from "@angular/core"
import {ActivatedRoute} from "@angular/router"
import {ItemService} from "~/app/item/item.service"
import {UserService} from "~/app/user.service"
import * as appSettings from "tns-core-modules/application-settings"
import {ObservableArray} from "tns-core-modules/data/observable-array"
import {ModalDialogParams, RouterExtensions} from "nativescript-angular"
import {ApiService} from "~/app/api.service"
import * as utils from "tns-core-modules/utils/utils"
import {TNSFancyAlert} from "nativescript-fancyalert"
import * as dialogs from 'tns-core-modules/ui/dialogs'

@Component({
    selector: "item-details",
    moduleId: module.id,
    templateUrl: "./item-detail.component.html",
})
export class ItemDetailComponent implements OnInit {
    @ViewChild('ScrollList', { static: true }) private scrollList: ElementRef;
    @Input() private scrollableHeight;

    stock: any = {}

    options: any = {}

    graphData: any = {}

    user: any = {}

    otherData: any = {
        pgrContext: Array
    }

    readyFlag: boolean = false

    priceMin: any

    priceMax: any

    show: any = {}

    chart: any = {}

    timespanPerChange: any

    timespanPriceChange: any

    loaded: any = {}

    menu = 'chart'

    subMenu = 'overall'

    error: any = {}

    ads: any = {}

    private _mainChart: ObservableArray<any>

    constructor(private route: ActivatedRoute,
                private userService: UserService,
                private router: RouterExtensions,
                private api: ApiService,
                private item: ItemService,
                private params: ModalDialogParams) {
                this.chart.current = '1M'
                this.user.perms = {
                    prem: false,
                    marketSurvivalGuide: false,
                    morningInsights: false,
                    marketInsights: false
                }
                this.loaded = {
                    data: false,
                    graph: false,
                    rating: false,
                    chart: false,
                    perms: false
                }
                this.options.starred = false
    }

    ngOnInit(): void {
        this.stock.symbol = appSettings.getString('symbol')
        console.log(' ------- ITEM DETAIL INIT -------', this.stock.symbol)
        this.user = {
            cookie: appSettings.getString('cookie'),
            uid: appSettings.getString('uid'),
            starred: appSettings.getNumber('starred'),
            userList: appSettings.getNumber('UserListId')
        }
        this.loadMyPerms()
        this.getSymbolData(this.stock.symbol)
        this.item.getListInfo(this.user.uid, this.user.starred, this.user.cookie)
            .subscribe(item => {
                item.symbols.forEach(ticker => {
                    if (ticker.symbol == this.stock.symbol) this.options.starred = true
                })
                this.api.getHeadlines(this.stock.symbol, this.user.cookie)
                    .subscribe(res => {
                        if (res.status == 'failed') {
                            console.log('no headlines')
                            this.show.detailHeadlines = false
                        } else {
                            this.stock.headlines = res.headlines
                            this.show.detailHeadlines = true
                        }
                        this.loaded.data = true
                    }
                )
            })
        this.item.getListInfo(this.user.uid, this.user.userList, this.user.cookie)
            .subscribe(item => {
                item.symbols.forEach(ticker => {
                    if (ticker.symbol == this.stock.symbol) this.options.myList = true
                })
            })
        this.getReportData(this.stock.symbol)
    }

    loadMyPerms() {
        this.userService.getUserPerms(this.user.uid).subscribe(perms => {
            let permsArr = perms.split(',')
            let keys = [],
                vals = []
            permsArr.forEach(item => {
                let key = item.split(':')
                keys.push(key[0].trim())
                vals.push(key[1].trim())
            })
            let mkt = vals[0] == 'true',
                mor = vals[1] == 'true',
                mar = vals[2] == 'true',
                pre = vals[3] == 'true',
                userPerms = {
                    marketSurvivalGuide: mkt,
                    morningInsights: mor,
                    marketInsights: mar,
                    premium: pre
                }
            this.user.perms = userPerms
            this.loaded.perms = true

            if (!userPerms.premium) {
                this.api.getWPAdsBanner().subscribe(ads => {
                    let regex = /(https?:\/\/.*\.(?:png|jpg))/;
                    let img = ads[0]['content']['rendered']
                    let imgUrl = regex.exec(img)
                    let URL = imgUrl[0].replace('&#215;', 'x') // imgUrl replace &#215; with 'x' as in 1024x720
                    console.log('URL', URL)
                    let linkItem = ads[0]['excerpt']['rendered']
                    let link = linkItem.slice(3, linkItem.length - 5)
                    console.log('LINK!!!', link)
                    this.ads = {
                        jpg: URL,
                        url: link
                    }
                    console.log('ADS??', this.ads)
                })
            }

        })
    }

    tapToRatings(menu, subMenu) {
        if (this.user.perms.premium) {
            this.menu = menu
            this.subMenu = subMenu
        } else {
            this.menu = 'rating'
        }
    }

    tapToSubMenu(submenu) {
        if (this.user.perms.premium) {
            this.subMenu = submenu
        }
    }

    get contextSummaries(): any {
        return this.stock.contextSums
    }

    get getHeadlines(): any {
        return this.stock.headlines
    }

    openBrowser(url) {
        utils.openUrl(url)
    }

    Star() {
        return this.options.starred
            ? this.item.deleteStockFromList(this.stock.symbol, this.user.uid, this.user.starred, this.user.cookie)
                .subscribe(del => {
                    this.options.starred = false
                    let msg = 'Removed ' + this.stock.symbol + ' from my Starred List'
                    dialogs.alert({
                        title: 'Removed',
                        message: msg,
                        okButtonText: 'OK'
                    })

                })
            : this.item.addStockIntoList(this.stock.symbol, this.user.uid, this.user.starred, this.user.cookie)
                .subscribe(add => {
                    this.options.starred = true
                    let msg = 'Added ' + this.stock.symbol + ' to my Starred List'
                    dialogs.alert({
                        title: 'Added',
                        message: msg,
                        okButtonText: 'OK'
                    })
                })
    }

    addToUser() {
        return this.options.myList
            ? this.item.deleteStockFromList(this.stock.symbol, this.user.uid, this.user.userList, this.user.cookie)
                .subscribe(del => {
                    this.options.myList = false
                    let msg = 'Removed ' + this.stock.symbol + ' from my Stocks List'
                    dialogs.alert({
                        title: 'Removed',
                        message: msg,
                        okButtonText: 'OK'
                    })
                })
            : this.item.addStockIntoList(this.stock.symbol, this.user.uid, this.user.userList, this.user.cookie)
                .subscribe(add => {
                    this.options.myList = true
                    let msg = 'Added ' + this.stock.symbol + ' to my Stocks List'
                    dialogs.alert({
                        title: 'Added',
                        message: msg,
                        okButtonText: 'OK'
                    })
                })
    }

    getSymbolData(stock: string) {
        this.item.getSymbolData(stock, this.user.uid, this.user.cookie)
            .subscribe(res => {
                let symbolData = {
                    symbol: this.stock.symbol,
                    fundamentals: res['fundamentalData'],
                    details: res['metaInfo'][0],
                    earnings: res['EPSData'],
                    PGR: res['metaInfo'][0].PGR,
                    pgr: {
                        val: res['pgr'][0]['PGR Value'],
                        fin: res['pgr'][1]['Financials'][0]['Value'],
                        earn: res['pgr'][2]['Earnings'][0]['Value'],
                        tech: res['pgr'][3]['Technicals'][0]['Value'],
                        exp: res['pgr'][4]['Experts'][0]['Value']
                    }
                }
                this.stock = symbolData
                this.stock.PGR > 0
                    ? this.prepareRatings()
                    : this.error.noRating = true
                console.log('symboldata done', this.stock.details)
            }
        )
    }

    prepareRatings() {
        this.item.getPgrDataAndContextSummary(this.stock.symbol, this.user.cookie)
            .subscribe(res => {

                console.log('res! ------ > ',res)

                let regexp = /\<.*?\>/g;

                let pgrContextSum = res['pgrContextSummary'][0]
                let context = {
                    earningsContextSum: res['earningsContextSummary'][0],
                    experts: res['expertOpnionsContextSummary'][0], //.explanatorySentence.replace(regexp, '') + res['expertOpnionsContextSummary'][0].generalSentence.replace(/\<.*?\>/g, ''),
                    financialContextSum: res['financialContextSummary'][0],
                    technicals: res['priceVolumeContextSummary'][0],
                    pgrData: res['pgrData'][0],
                    pgrContextSum: [
                        pgrContextSum['mainSentence'].replace(regexp, ''),
                        pgrContextSum['additonalSentence'].replace(regexp, '')
                    ]
                }
                this.stock.context = context
                this.loaded.rating = true
            }
        )
    }

    detailFormatter(words) {
        let regex = /\<.*?\>/g;
        let newWords = words.replace(regex, '')
        return newWords
    }

    getReportData(stock: string): void {
        this.item.getStockSummaryData(stock) // todo fix this shit
            .subscribe(data => {
                this.graphData = data;
                this.toggleChartTime('1M')
            })
    }

    pgrRating(pgr, raw) {
        return this.item.getPGRiconStockMaxRAWconfig(pgr,raw)
    }

    getGradient(pgr) {
        switch(pgr) {
            case 0:
                return ''
                break;
            case 1:
                return 'gradient-verybearish'
                break;
            case 2:
                return 'gradient-bearish'
                break;
            case 3:
                return 'gradient-neutral'
                break;
            case 4:
                return 'gradient-bullish'
                break;
            case 5:
                return 'gradient-verybullish'
                break;
        }
    }

    starred() {
        return this.options.starred ? 'starredActive' : null
    }

    onMyList() {
        return this.options.myList ? 'starredActive' : null
    }

    getDotColor(pgr) {
        switch(pgr) {
            case 0:
                return ''
                break;
            case 1:
                return 'dot-verybearish'
                break;
            case 2:
                return 'dot-bearish'
                break;
            case 3:
                return 'dot-neutral'
                break;
            case 4:
                return 'dot-bullish'
                break;
            case 5:
                return 'dot-verybullish'
                break;
        }
    }

    getChartBGColor(pgr) {
        switch (pgr) {
            case 'Very Bearish':
                return 'bearish--gradient'
                break;
            case 'Bearish':
                return 'bearish--gradient'
                break;
            case 'Neutral':
                return 'neutral--gradient'
                break;
            case 'Bullish':
                return 'bullish--gradient'
                break;
            case 'Very Bullish':
                return 'bullish--gradient'
                break;
        }
    }

    getRatingBGColor(pgr) {
        switch(pgr) {
            case 'Very Bearish':
                return 'bearish--gradient__rating-tab'
                break;
            case 'Bearish':
                return 'bearish--gradient__rating-tab'
                break;
            case 'Neutral':
                return 'neutral--gradient__rating-tab'
                break;
            case 'Bullish':
                return 'bullish--gradient__rating-tab'
                break;
            case 'Very Bullish':
                return 'bullish--gradient__rating-tab'
                break;
        }
    }

    detail52WkFormatter(lo, hi) {
        return parseFloat(lo).toFixed(2) + ' - ' + parseFloat(hi).toFixed(2)
    }

    detail52WkPerfFormatter(perf) {
        return parseFloat(perf).toFixed(2) + '%'
    }

    fixedTwo(item) {
         return item == 'N/A' ? 'N/A' : parseFloat(item).toFixed(2)
    }

    getBGcolor(pgr) {
        switch(pgr) {
            case -1:
                return ''
                break;
            case 'Very Bearish':
                return 'redbg'
                break;
            case 'Bearish':
                return 'redbg'
                break;
            case 'Neutral':
                return 'yellowbg'
                break;
            case 'Bullish':
                return 'greenbg'
                break;
            case 'Very Bullish':
                return 'greenbg'
                break;
        }
    }

    statusColors(chg) {
        return chg == 0
            ? 'black'
            : chg > 0
            ? 'green'
            : 'red'
    }

    statusColorPGR(pgr) {
        return pgr == 3
            ? 'yellow'
            : pgr > 3
            ? 'green'
            : 'red'
    }

    pgrRatingWord(pgr, raw) {
        return this.item.getPGRword(pgr, raw)
    }

    pgrNoTxt(pgr, raw) {
        return this.item.getPGRiconNoTxtRAWconfig(pgr, raw)
    }

    get stockSymbol(): string {
        return this.stock.symbol
    }

    get stockName(): string {
        return this.stock.details.name
    }

    get lastChange(): string {
        return this.stock.details['Change'].toFixed(2)
    }

    get lastPercentage(): string {
        return this.stock.details['Percentage '] > 0
            ? ('(+' + this.stock.details['Percentage '].toFixed(2) + '%)')
            : ('(' + this.stock.details['Percentage '].toFixed(2) + '%)')
    }

    get lastPrice(): string {
        return '$' + parseFloat(this.stock.details['Last']).toFixed(2)
    }

    get movementStatus1(): string {
        return this.stock.details['name'] + ' was'
    }

    get getIndustry(): string {
        return this.stock.details['industry_name']
    }

    get getNextReport() {
        return this.stock.earnings.next_report_date
    }

    get getEarningsQuarter() {
        return 'Q' + this.stock.earnings.current_quarter + ' Estimate:'
    }

    get getCurrentRevision() {
        return this.stock.earnings.current_quarter_consensus_estimate
    }

    get getMktCap() {
        let cap = this.stock.details['marketCap']
        switch (true) {
            case cap > 10000:
                return 'Large Cap - ' + (cap / 1000).toFixed(2) + 'B'
                break;
            case cap > 2000:
                return 'Mid Cap - ' + (cap/1000).toFixed(2) + 'B'
                break;
            case cap > 1000 && cap < 2000:
                return 'Small Cap - ' + (cap/1000).toFixed(2) + 'B'
                break;
            case cap < 1000:
                return 'Small Cap - ' + cap.toFixed(2) + 'M'
        }
    }

    revenueFormatter(rev) {
        rev = parseFloat(rev)
        return rev > 1000
            ? (rev/1000).toFixed(1) + 'B'
            : rev.toFixed(1) + 'M'
    }

    topStories(){
        console.log('refresh the newstand')
        this.api.getHeadlines(this.stock.symbol, this.user.cookie)
            .subscribe(res => {
                console.log('headlines ------ > ', res)

                if (res.status == 'failed') {
                    console.log('no headlines')
                    this.show.detailHeadlines = false
                } else {
                    this.stock.headlines = res.headlines
                    this.show.detailHeadlines = true
                }
            })
    }

    get getLabelFormatLinear() {
        return this.chart.labelFormatLinear
    }

    get graphStep(): number {
        return this.priceMax/3
    }

    close() {
        this.params.closeCallback()
    }

    showMoreInformation() {
        TNSFancyAlert.showInfo(
            this.stock.details.name + ' Company Details',
            this.stock.fundamentals['Company Text Blurb'],
            'OK',
            10,
            200
        )
    }

    calculatePricePerChange(firstClose: number, lastClose: number): number {
        return  ( (lastClose - firstClose) / firstClose) * 100
    }

    calculatePriceChange(firstClose: number, lastClose: number): number {
        return lastClose - firstClose
    }

    toggleChartTime(span: string) {
        this.chart.current = span
        this.loaded.chart = false
        let cut = 0;
        switch (span) {
            case '1M':
                cut = 20;
                this.chart.labelFormatLinear = '%.2f'
                break;
            case '3M':
                cut = 60
                this.chart.labelFormatLinear = '%.0f'
                break;
            case '6M':
                cut = 120;
                this.chart.labelFormatLinear = '%.0f'
                break;
            case '1Y':
                cut = 250;
                this.chart.labelFormatLinear = '%.0f'
                break;
        }

        let exp_dates = this.graphData['one_year_chart_data']['calculations_dates'].slice(0, cut).reverse()
        this.chart.MAXDATE = new Date(exp_dates[cut-1]) // today
        this.chart.MINDATE = new Date(exp_dates[0]) // CUT
        let JSdate = exp_dates.map(d => {return new Date(d)})
        let closePrices = this.graphData['one_year_chart_data']['close_price'].map(x => +x).reverse().slice(this.graphData['one_year_chart_data']['close_price'].length - cut)
        let dema = this.graphData['one_year_chart_data']['chaikin_trend_20001'].map(x => parseFloat(x)).reverse().slice(this.graphData['one_year_chart_data']['chaikin_trend_20001'].length - cut)
        let firstPriceIndex = 0;
        for (let i=0; i<closePrices.length; i++) {
            if (closePrices[i] == 0) {
                firstPriceIndex = i+1
            }
        }
        this.timespanPerChange = this.calculatePricePerChange(closePrices[firstPriceIndex], closePrices[closePrices.length-1]).toFixed(2)
        this.timespanPriceChange = this.calculatePriceChange(closePrices[firstPriceIndex], closePrices[closePrices.length-1]).toFixed(2)
        let filtPrices = closePrices.slice(firstPriceIndex)
        let filtDates = JSdate.slice(firstPriceIndex)
        this.priceMax = Math.max(...filtPrices) * 1.001
        this.priceMin = Math.min(...filtPrices) * 0.999
        let delta = this.priceMax - this.priceMin
        delta > 1
            ? this.chart.majorStep = Math.ceil(delta / 5)
            : this.chart.majorStep = 0.25
        let output = []

        for (let i=0; i<filtPrices.length; i++) {
            output.push({
                date: filtDates[i],
                price: filtPrices[i],
                dema: dema[i]
            })
        }
        this._mainChart = new ObservableArray(output)
        this.loaded.chart = true
    }

    get mainChart(): ObservableArray<any> {
        return this._mainChart
    }

    itemDetailGraphStatus() {
        return this.stock.details.Change > 0 ? '&#xf062;' : '&#xf063;'
    }

    get getGraphTimeChangeText() {
        return this.timespanPerChange > 0
            ? (this.timespanPriceChange + ' (+' + this.timespanPerChange + '%)')
            : (this.timespanPriceChange + ' (' + this.timespanPerChange + '%)')
    }

    nav(route) {
        this.router.navigate([route + '/' + this.stock.symbol])
    }

    goBack() {
        this.router.back()
    }

    toggleExploreScreener() {
        this.show.exploreScreener = !this.show.exploreScreener
    }

    toggleDetailHeadlines() {
        this.show.detailHeadlines = !this.show.detailHeadlines
    }

    get getMajorStep(): string {
        switch (this.chart.current) {
            case '1M':
                return 'Day'
                break;
            case '3M':
                return 'Day'
                break;
            case '6M':
                return 'Month'
                break;
            case '1Y':
                return 'Month'
                break;
        }
    }

    get getMajorStepCount() {
        switch (this.chart.current) {
            case '1M':
                return 7
                break;
            case '3M':
                return 14
                break;
            case '6M':
                return 1
                break;
            case '1Y':
                return 1
                break;
        }
    }

    get getMajorStepLinear() {
        return this.chart.majorStep
    }

    get getDateFormat(): string {
        if (this.chart.current) {
            switch (this.chart.current) {
                case '1M':
                    return 'MM/dd'
                    break;
                case '3M':
                    return 'MM/dd'
                    break;
                case '6M':
                    return 'MMM'
                    break;
                case '1Y':
                    return 'MMM'
                    break;
            }
        } else {
            return 'MM/dd'
        }
    }

    get getPriceMin() {
        return this.priceMin
    }

    get getPriceMax() {
        return this.priceMax
    }

    get minDate() {
        return this.chart.MINDATE
    }

    get maxDate() {
        return this.chart.MAXDATE
    }
}
