// import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
// import { NativeScriptCommonModule } from 'nativescript-angular/common';
// import { NativeScriptRouterModule } from "nativescript-angular";
// // import { ItemsComponent } from "~/app/item/items.component";
// // import { ItemDetailComponent } from "~/app/item/item-detail.component";
//
// @NgModule({
//   declarations: [ItemsComponent, ItemDetailComponent],
//   imports: [
//       NativeScriptCommonModule,
//       NativeScriptRouterModule,
//       NativeScriptRouterModule.forChild([
//           { path: '', redirectTo: 'lists' },
//           { path: 'lists', component: ItemsComponent },
//           { path: 'details/:id', component: ItemDetailComponent }
//       ])
//   ],
//   schemas: [NO_ERRORS_SCHEMA]
// })
// export class ItemModule { }
