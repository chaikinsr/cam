import {Injectable} from "@angular/core"
import {HttpClient} from "@angular/common/http"
import {Observable} from "rxjs"
import {UserService} from "~/app/user.service"
import * as appSettings from 'tns-core-modules/application-settings'
import {ApiService} from "~/app/api.service";

@Injectable({
    providedIn: "root"
})
export class ItemService {
    constructor(private http: HttpClient,
                private user: UserService,
                private api: ApiService) { }

    getAuthorizedLists(uid: string, cookie: string) {
        const url = `https://app.chaikinanalytics.com/CPTRestSecure/app/portfolio/getAuthorizedLists?uid=` + uid + `&rank=false&responseType=custom&environment=desktop&version=1.3.4`;
        const options = {
            params: {
                'uid': uid
            },
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'en-US,en;q=0.9',
                'Connection': 'keep-alive',
                'Cookie': cookie
            }
        }
        return this.api.getJSON(url, options)
    }

    getStockSummaryData(symbol: string) {
        const url = 'https://app.chaikinanalytics.com/CPTRestSecure/app/stockSummary/getStockSummaryData?'
        const options = {
            params: {
                symbol: symbol,
                components: 'stockSummaryData,oneYearChartData,oneYearPgrData'
            }
        }
        return this.api.getJSON(url, options)
    }

    getSymbolData(symbol: string, uid: string, cookie: string) {
        const url = 'https://app.chaikinanalytics.com/CPTRestSecure/app/portfolio/getSymbolData?&uid=' + uid + '&symbol=' + symbol + '&components=pgr%2CmetaInfo%2CfundamentalData%2CEPSData'
        const options = {
            params: {
                symbol: symbol
            },
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Accept-Encoding' : 'gzip, deflate, br',
                'Accept-Language' : 'en-US,en;q=0.9',
                'Connection' : 'keep-alive',
                'Cookie': cookie
            }
        }
        return this.api.getJSON(url, options)
    }

    getListAlerts(uid, listId, cookie): Observable<any> {
        const url = 'https://app.chaikinanalytics.com/CPTRestSecure/app/alerts/getAllAlerts?id=' + uid + '&list_id=' + listId + '&period=1';
        const options = {
            params: {
                id : uid,
                list_id: listId
            },
            headers: {
                'Accept'          : 'application/json, text/plain, */*',
                'Accept-Encoding' : 'gzip, deflate, br',
                'Accept-Language' : 'en-US,en;q=0.9',
                'Connection'      : 'keep-alive',
                'Cookie'          : cookie
            }
        }
        return this.api.getJSON(url, options)
    }

    getListInfo(uid: string, listId: number, cookie: string): Observable<any> {
        const url = 'https://app.chaikinanalytics.com/CPTRestSecure/app/portfolio/getListSymbols'
        const options = {
            params: {
                listId: listId,
                uid: uid
            },
            headers: {
                Accept: 'application/json, text/plain, */*',
                'Accept-Encoding' : 'gzip, deflate, br',
                'Accept-Language' : 'en-US,en;q=0.9',
                Connection : 'keep-alive',
                Cookie : cookie
            }
        }
        return this.api.getJSON(url, options)
    }

    getListData(uid: string, listId: string, cookie: string): Observable<any> {
        const url = 'https://app.chaikinanalytics.com/CPTRestSecure/app/portfolio/getListData'
        const options = {
            withCredentials: true,
            params: {
                uid: uid,
                listId: listId
            },
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Accept-Encoding' : 'gzip, deflate, br',
                'Accept-Language' : 'en-US,en;q=0.9',
                'Connection' : 'keep-alive',
                'Cookie' : cookie
            }
        }
        return this.api.getJSON(url, options)
    }

    deleteStockFromList(item: any, uid: string, listId: string, cookie: string) {
        const url = "https://app.chaikinanalytics.com/CPTRestSecure/app/portfolio/deleteSymbolFromList?symbol=" + item + "&uid=" + uid + "&listId=" + listId
        const options = {
            params: {
                'listId': listId,
                'uid': uid
            },
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Accept-Encoding' : 'gzip, deflate, br',
                'Accept-Language' : 'en-US,en;q=0.9',
                'Connection' : 'keep-alive',
                'Cookie' : cookie
            }
        }
        return this.api.getJSON(url, options)
    }

    deleteENTIREstockList(uid: string, listId: string, cookie: string) {
        const url = 'https://app.chaikinanalytics.com/CPTRestSecure/app/portfolio/deleteStockList'
        const options = {
            withCredentials: true,
            params: {
                'uid': uid,
                'listId': listId
            },
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Accept-Encoding' : 'gzip, deflate, br',
                'Accept-Language' : 'en-US,en;q=0.9',
                'Connection' : 'keep-alive',
                'Cookie' : cookie
            }
        }
        return this.api.getJSON(url, options)
    }

    initialMarketSectorData(cookie: string): Observable<any> {
        const url = `https://app.chaikinanalytics.com/CPTRestSecure/app/midTier/getInitialData?`;
        const options = {
            params: {
                'components': 'majorMarketIndices,sectors'
            },
            headers: {
                Accept: 'application/json, text/plain, */*',
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'en-US,en;q=0.9',
                'Connection': 'keep-alive',
                'Cookie': cookie
            }
        }
        return this.api.getJSON(url, options)
    }

    symbolLookup(query: string): Observable<any> {
        const url = 'https://app.chaikinanalytics.com/CPTRestSecure/app/stocks/symbol-lookupV1?q=' + query + '&searchColumn=symbol&mode=null';
        const options = {
            headers: {
                'Accept'          : 'application/json, text/plain, */*',
                'Accept-Encoding' : 'gzip, deflate, br',
                'Accept-Language' : 'en-US,en;q=0.9',
                'Connection'      : 'keep-alive'
            }
        }
        return this.api.getJSON(url, options)
    }

    addStockIntoList(ticker: string, uid: string, listId: number, cookie: string): Observable<any> {
        const url = "https://app.chaikinanalytics.com/CPTRestSecure/app/portfolio/addStockIntoList?symbol=" + ticker + "&listId=" + listId;
        const options = {
            params: {
                listId: listId,
                symbol: ticker
            },
            headers: {
                'Accept'          : 'application/json, text/plain, */*',
                'Accept-Encoding' : 'gzip, deflate, br',
                'Accept-Language' : 'en-US,en;q=0.9',
                'Connection'      : 'keep-alive',
                'Cookie'          : cookie
            }
        }
        return this.api.getJSON(url, options)
    }

    getPgrDataAndContextSummary(ticker: string, cookie: string): Observable<any> {
        const url = "https://app.chaikinanalytics.com/CPTRestSecure/app/researchReportServices/getPgrDataAndContextSummary?symbol=" + ticker;
        const options = {
            withCredentials: true,
            headers: {
                'Accept'          : 'application/json, text/plain, */*',
                'Accept-Encoding' : 'gzip, deflate, br',
                'Accept-Language' : 'en-US,en;q=0.9',
                'Connection'      : 'keep-alive',
                'Cookie'          : cookie
            }
        }
        return this.api.getJSON(url, options)
    }

    getTickerCompetitors(ticker: string, cookie: string): Observable<any> {
        const url = "https://app.chaikinanalytics.com/CPTRestSecure/app/researchReportServices/getTickerCompetitors?symbol=" + ticker;
        const options = {
            headers: {
                'Accept'          : 'application/json, text/plain, */*',
                'Accept-Encoding' : 'gzip, deflate, br',
                'Accept-Language' : 'en-US,en;q=0.9',
                'Connection'      : 'keep-alive',
                'Cookie'          : cookie
            }
        };
        return this.api.getJSON(url, options)
    }

    getResearchReportData(ticker: string, cookie: string): Observable<any> {
        const url = "https://app.chaikinanalytics.com/CPTRestSecure/app/researchReportServices/getResearchReportData?symbol=" + ticker;
        const options = {
            headers: {
                'Accept'          : 'application/json, text/plain, */*',
                'Accept-Encoding' : 'gzip, deflate, br',
                'Accept-Language' : 'en-US,en;q=0.9',
                'Connection'      : 'keep-alive',
                'Cookie'          : cookie,
            }
        }
        return this.api.getJSON(url, options)
    }

    getHeadlines(ticker: string, cookie: string): Observable<any> {
        const url = "https://app.chaikinanalytics.com/CPTRestSecure/app/xigniteNews/getHeadlines?symbol=" + ticker;
        const options = {
            headers: {
                'Accept'          : 'application/json, text/plain, */*',
                'Accept-Encoding' : 'gzip, deflate, br',
                'Accept-Language' : 'en-US,en;q=0.9',
                'Connection'      : 'keep-alive',
                'Cookie'          : cookie,
            }
        }
        return this.api.getJSON(url, options)
    }

    getGraphData(ticker: string, cookie: string): Observable<any> {
        const url = "https://app.chaikinanalytics.com/CPTRestSecure/app/chart/getChart?symbol=" + ticker + "&interval=1D&type=1&numBars=250&version=1.1&_=1546464273438";
        const options = {
            headers: {
                'Accept'          : 'application/json, text/plain, */*',
                'Accept-Encoding' : 'gzip, deflate, br',
                'Accept-Language' : 'en-US,en;q=0.9',
                'Connection'      : 'keep-alive',
                'Cookie'          : cookie,
            }
        }
        return this.api.getJSON(url, options)
    }

    getPGRratingETF(pgr, raw) {
        switch(pgr) {
            case -1:
                return 'res://ico__PGR-S--LG__None--light'
                break
            case 0:
                return 'res://ico__PGR-E--LG__None--light'
                break
            case 1:
                return 'res://ico__PGR-E--LG__VeryBearish--light'
                break
            case 2:
                return 'res://ico__PGR-E--LG__Bearish--light'
                break
            case 3:
                if (pgr == raw) return 'res://ico__PGR-E--LG__Neutral--light'
                if (pgr < raw) return 'res://ico__PGR-E--LG__NeutralPlus--light'
                if (pgr > raw) return 'res://ico__PGR-E--LG__NeutralNeg--light'
                break
            case 4:
                return 'res://ico__PGR-E--LG__Bullish--light'
                break
            case 5:
                return 'res://ico__PGR-E--LG__VeryBullish--light'
                break
        }
    }

    getPGRratingStockNeutStatusConfig(pgr, neut) { // FOR API THAT RETURNS NEUTRAL_STATUS INSTEAD OF RAW_PGR
        switch(pgr) {
            case -1:
                return 'res://ico__PGR-S--LG__None--light'
                break;
            case 0:
                return 'res://ico__PGR-S--LG__None--light'
                break;
            case 1:
                return 'res://ico__PGR-S--LG__VeryBearish--light'
                break;
            case 2:
                return 'res://ico__PGR-S--LG__Bearish--light'
                break;
            case 3:
                if (neut === 0) return 'res://ico__PGR-S--LG__Neutral--light'
                if (neut > 0) return 'res://ico__PGR-S--LG__NeutralPlus--light'
                if (neut < 1) return 'res://ico__PGR-S--LG__NeutralNeg--light'
                break;
            case 4:
                return 'res://ico__PGR-S--LG__Bullish--light'
                break;
            case 5:
                return 'res://ico__PGR-S--LG__VeryBullish--light'
                break;
        }
    }

    getPGRiconNoTxtNEUTconfig(pgr, neut) {
        switch(pgr) {
            case 0:
                return 'res://ico--PGR-S__NoTxt-none'
                break;
            case -1:
                return 'res://ico--PGR-S__NoTxt-none'
                break;
            case 1:
                return 'res://ico--PGR-S__NoTxt-very-bearish'
                break;
            case 2:
                return 'res://ico--PGR-S__NoTxt-bearish'
                break;
            case 3:
                if (neut === 0) return 'res://ico--PGR-S__NoTxt-neutral'
                if (neut == 1) return 'res://ico--PGR-S__NoTxt-neutral-plus'
                if (neut == -1) return 'res://ico--PGR-S__NoTxt-neutral-minus'
                break;
            case 4:
                return 'res://ico--PGR-S__NoTxt-bullish'
                break;
            case 5:
                return 'res://ico--PGR-S__NoTxt-very-bullish'
                break;
        }
    }

    getPGRiconNoTxtRAWconfig(pgr, raw) {
        switch(pgr) {
            case 0:
                return 'res://ico--PGR-S__NoTxt-none'
                break;
            case -1:
                return 'res://ico--PGR-S__NoTxt-none'
                break;
            case 1:
                return 'res://ico--PGR-S__NoTxt-very-bearish'
                break;
            case 2:
                return 'res://ico--PGR-S__NoTxt-bearish'
                break;
            case 3:
                if (pgr == raw) return 'res://ico--PGR-S__NoTxt-neutral'
                if (pgr < raw) return 'res://ico--PGR-S__NoTxt-neutral-plus'
                if (pgr > raw) return 'res://ico--PGR-S__NoTxt-neutral-minus'
                break;
            case 4:
                return 'res://ico--PGR-S__NoTxt-bullish'
                break;
            case 5:
                return 'res://ico--PGR-S__NoTxt-very-bullish'
                break;
        }
    }

    getPGRiconNoTxtRAWETFconfig(pgr, raw) {
        switch(pgr) {
            case -1:
                return 'res://ico--PGR-E__NoTxt-none'
                break
            case 1:
                return 'res://ico--PGR-E__NoTxt-very-bearish'
                break
            case 2:
                return 'res://ico--PGR-E__NoTxt-bearish'
                break
            case 3:
                if (pgr == raw) return 'res://ico--PGR-E__NoTxt-neutral'
                if (pgr < raw) return 'res://ico--PGR-E__NoTxt-neutral-plus'
                if (pgr > raw) return 'res://ico--PGR-E__NoTxt-neutral-minus'
                break
            case 4:
                return 'res://ico--PGR-E__NoTxt-bullish'
                break
            case 5:
                return 'res://ico--PGR-E__NoTxt-very-bullish'
                break
        }
    }

    getPGRiconAllRAWconfig(pgr, raw, etf) {
        if (!etf) {
            switch (pgr) {
                case -1:
                    return 'res://ico__PGR-S--LG__None--light'
                    break;
                case 0:
                    return 'res://ico__PGR-S--LG__None--light'
                    break;
                case 1:
                    return 'res://ico__PGR-S--LG__VeryBearish--light'
                    break;
                case 2:
                    return 'res://ico__PGR-S--LG__Bearish--light'
                    break;
                case 3:
                    return pgr == raw
                        ? 'res://ico__PGR-S--LG__Neutral--light'
                        : pgr < raw
                            ? 'res://ico__PGR-S--LG__NeutralPlus--light'
                            : 'res://ico__PGR-S--LG__NeutralNeg--light'
                    break;
                case 4:
                    return 'res://ico__PGR-S--LG__Bullish--light'
                    break;
                case 5:
                    return 'res://ico__PGR-S--LG__VeryBullish--light'
                    break;
            }
        } else {
            switch (pgr) {
                case -1:
                    return 'res://ico__PGR-E--LG__None--light'
                    break;
                case 0:
                    return 'res://ico__PGR-E--LG__None--light'
                    break;
                case 1:
                    return 'res://ico__PGR-E--LG__VeryBearish--light'
                    break;
                case 2:
                    return 'res://ico__PGR-E--LG__Bearish--light'
                    break;
                case 3:
                    return pgr == raw
                        ? 'res://ico__PGR-E--LG__Neutral--light'
                        : pgr < raw
                            ? 'res://ico__PGR-E--LG__NeutralPlus--light'
                            : 'res://ico__PGR-E--LG__NeutralNeg--light'
                    break;
                case 4:
                    return 'res://ico__PGR-E--LG__Bullish--light'
                    break;
                case 5:
                    return 'res://ico__PGR-E--LG__VeryBullish--light'
                    break;
            }
        }
    }

    getPGRiconETFMaxRAWconfig(pgr, raw) {
        switch(pgr) {
            case -1:
                return 'res://ico__PGR-E--Max__None'
                break
            case 1:
                return 'res://ico__PGR-E--Max__VeryBearish'
                break
            case 2:
                return 'res://ico__PGR-E--Max__Bearish'
                break
            case 3:
                if (pgr == raw) return 'res://ico__PGR-E--Max__Neutral'
                if (pgr < raw) return 'res://ico__PGR-E--Max__Neut-plus'
                if (pgr > raw) return 'res://ico__PGR-E--Max__Neut-minus'
                break
            case 4:
                return 'res://ico__PGR-E--Max__Bullish'
                break
            case 5:
                return 'res://ico__PGR-E--Max__VeryBullish'
                break
        }
    }

    getPGRiconStockMaxRAWconfig(pgr, raw) {
        switch(pgr) {
            case 0:
                return 'res://ico__PGR-S--Max__None'
                break
            case 1:
                return 'res://ico__PGR-S--Max__VeryBearish'
                break
            case 2:
                return 'res://ico__PGR-S--Max__Bearish'
                break
            case 3:
                if (pgr == raw) return 'res://ico__PGR-S--Max__Neutral'
                if (pgr < raw) return 'res://ico__PGR-S--Max__Neut-plus'
                if (pgr > raw) return 'res://ico__PGR-S--Max__Neut-minus'
                break
            case 4:
                return 'res://ico__PGR-S--Max__Bullish'
                break
            case 5:
                return 'res://ico__PGR-S--Max__VeryBullish'
                break
        }
    }

    getPGRword(pgr, raw) {
        switch(pgr) {
            case -1:
                return 'None'
                break;
            case 0:
                return 'None'
                break;
            case 1:
                return 'Very Bearish'
                break;
            case 2:
                return 'Bearish'
                break;
            case 3:
                if (pgr == raw) return 'Neutral'
                if (pgr < raw) return 'Neutral +'
                if (pgr > raw) return 'Neutral -'
                break;
            case 4:
                return 'Bullish'
                break;
            case 5:
                return 'Very Bullish'
                break;
        }
    }

    handleSessionTimeoutError(status){
        console.log(' HANDLE TIMEOUT ')
        let user = appSettings.getString('email')
        let pW = appSettings.getString('pW')
        if (status == 403) {
            this.user.authUser(user, pW)
                .subscribe(res => {
                    let resHeader = res.headers.get('Set-Cookie')
                    let cookie = resHeader.toString()
                    appSettings.setString('cookie', cookie)
                    console.log('cookie', cookie)
                }
            )
        }
    }
}
