import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import {SearchComponent} from "~/app/search/search.component";
import {ItemDetailComponent} from "~/app/item/item-detail.component";
import {EtfDetailComponent} from "~/app/etf-detail/etf-detail.component";

const routes: Routes = [
    { path: '', component: SearchComponent },
    { path: 'stock-detail/:id', component: ItemDetailComponent },
    { path: 'etf-detail/:id', component: EtfDetailComponent },
];

@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class SearchRoutingModule { }
