import {Component, ElementRef, OnInit, ViewChild, ViewContainerRef} from '@angular/core'
import {TextField} from "tns-core-modules/ui/text-field"
import {ModalDialogOptions, ModalDialogService, RouterExtensions} from "nativescript-angular"
import {ActivatedRoute} from "@angular/router"
import {ItemService} from "~/app/item/item.service"
import * as appSettings from 'tns-core-modules/application-settings'
import * as dialogs from 'tns-core-modules/ui/dialogs'
import {ItemDetailComponent} from "~/app/item/item-detail.component"
import {EtfDetailComponent} from "~/app/etf-detail/etf-detail.component"
import {ApiService} from "~/app/api.service"
import * as utils from "tns-core-modules/utils/utils"
import {UserService} from "~/app/user.service"

@Component({
  selector: 'ns-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  moduleId: module.id,
})
export class SearchComponent implements OnInit {

    query: any = {}
    search: string
    resultList = []
    loaded: any = {}
    previousQueries = []
    ads: any = {}
    user: any = {}

    constructor(private router: RouterExtensions,
                private activeRoute: ActivatedRoute,
                private itemService: ItemService,
                private modalService: ModalDialogService,
                private viewContainerRef: ViewContainerRef,
                private api: ApiService,
                private userService: UserService) {
                this.loaded = {
                    query: false,
                    previous: false,
                    perm: false
                }
    }

    @ViewChild('searchField', { static: true, read: ElementRef }) searchField: ElementRef

    ngOnInit() {
        console.log(' -- SEARCH INIT -- ')
        this.user = {
            email: appSettings.getString('email'),
            cookie: appSettings.getString('cookie'),
            uid: appSettings.getString('uid'),
            starred: appSettings.getNumber('starred')
        }
        this.loadMyPerms()
    }

    loadMyPerms() {
        this.userService.getUserPerms(this.user.uid).subscribe(perms => {
            let permsArr = perms.split(',')
            let keys = [],
                vals = []
            permsArr.forEach(item => {
                let key = item.split(':')
                keys.push(key[0].trim())
                vals.push(key[1].trim())
            })
            let mkt = vals[0] == 'true',
                mor = vals[1] == 'true',
                mar = vals[2] == 'true',
                pre = vals[3] == 'true',
                userPerms = {
                    marketSurvivalGuide: mkt,
                    morningInsights: mor,
                    marketInsights: mar,
                    premium: pre
                }
            this.user.perms = userPerms
            this.loaded.perm = true
            if (!userPerms.premium) {
                this.api.getWPAdsBanner().subscribe(ads => {
                    let regex = /(https?:\/\/.*\.(?:png|jpg))/;
                    let img = ads[0]['content']['rendered']
                    let imgUrl = regex.exec(img)
                    let URL = imgUrl[0].replace('&#215;', 'x') // imgUrl replace &#215; with 'x' as in 1024x720
                    let linkItem = ads[0]['excerpt']['rendered']
                    let link = linkItem.slice(3, linkItem.length - 5)
                    this.ads = {
                        jpg: URL,
                        url: link
                    }
                })
            }
        })
    }

    shouldIShowAds() {
        if (this.loaded.perm) return !this.user.perms.premium
    }

    getAdSrc() {
        return this.ads.jpg
    }

    goToAds() {
        this.openBrowser(this.ads.url)
    }

    openBrowser(url) {
        utils.openUrl(url)
    }

    public onTextChange(args) {
        let textField = <TextField>args.object
        this.search = textField.text
        this.search.length == 0
            ? this.resultList = []
            : this.getSymbolLookup()
        this.loaded.query = true
    }

    public onReturn(args) {
        let textField = <TextField>args.object
        this.query = textField.text
    }

    public searchClick() {
        this.search && this.resultList[0]
            ? this.click(this.resultList[0])
            : dialogs.alert({
                title: 'Nothing Found',
                message: 'There are no stocks or ETFs with this name.',
                okButtonText: 'OK'
            })
    }

    // tells company name, symbol and is_etf of search input
    public getSymbolLookup() {
        this.itemService.symbolLookup(this.search)
            .subscribe(res => {
                this.resultList = res;
            })
    }

    public click(item) {
        this.previousQueries.includes(item)
            ? console.log('ALREADY SEARCHED')
            : this.previousQueries.length < 3
                ? this.previousQueries.push(item)
                : (this.previousQueries.shift(),
                  this.previousQueries.push(item))

        this.loaded.previous = true

        if(item.is_etf) {
            appSettings.setString('etf-detail', item.Symbol)
            const options: ModalDialogOptions = {
                viewContainerRef: this.viewContainerRef,
                fullscreen: true,
                context: {}
            }
            this.modalService.showModal(EtfDetailComponent, options)
        } else {
            appSettings.setString('symbol', item.Symbol);
            const options: ModalDialogOptions = {
                viewContainerRef: this.viewContainerRef,
                fullscreen: true,
                context: {}
            }
            this.modalService.showModal(ItemDetailComponent, options)
        }
    }

    public clearSearch() {
        this.previousQueries = []
        this.searchField.nativeElement.text = ''
    }
}
