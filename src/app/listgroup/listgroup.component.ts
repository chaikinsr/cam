import {Component, OnInit} from '@angular/core'
import {ItemService} from "~/app/item/item.service"
import * as appSettings from "tns-core-modules/application-settings"
import {ObservableArray} from "tns-core-modules/data/observable-array"
import {RouterExtensions} from "nativescript-angular"
import {ActivatedRoute} from "@angular/router"
import {ListViewEventData} from "nativescript-ui-listview/ui-listview.common"
import {ApiService} from "~/app/api.service"

@Component({
  selector: 'ns-listgroup',
  templateUrl: './listgroup.component.html',
  styleUrls: ['./listgroup.component.scss'],
  moduleId: module.id,
})
export class ListgroupComponent implements OnInit {
    id: any = {}
    user: any = {}
    items: any = {}

    constructor(private itemService: ItemService,
                private router: RouterExtensions,
                private route: ActivatedRoute,
                private api: ApiService,
                private item: ItemService) { }

    ngOnInit() {
        console.log(' --- INIT : LIST GROUP COMP --- ')
        this.user.cookie = appSettings.getString('cookie')
        this.user.uid = appSettings.getString('uid')
        this.setupLists()
    }

    setupLists() {
        this.api.getMyLists(this.user.uid, this.user.cookie).subscribe(list => {
            let lists = list[0]['User Lists'];

            // FIND STARRED LIST
            lists.map(list => {
                if (list.name == 'Starred') this.user.starred = list.list_id
            })

            this.itemService.getListInfo(this.user.uid, this.user.starred, this.user.cookie)
                .subscribe(res => {
                    this.items = res.symbols;
                })
        })
    }

    get getList(): ObservableArray<any> {
        return this.items
    }

    onPullToRefreshInitiated(args: ListViewEventData) {
        this.setupLists();
        let listView = args.object;
        listView.notifyPullToRefreshFinished();
    }

    pgrRating(pgr, rawPGR) {
        return this.itemService.getPGRiconAllRAWconfig(pgr, rawPGR, false)
    }

    onTap(item){
        appSettings.setString('symbol', item['symbol'])
        appSettings.setString('name', item['name'])
        let route = '../detail/' + item['symbol']
        this.router.navigate([ route ], {
            relativeTo: this.route,
            transition: {
                name: 'slideTop',
                duration: 300
            }
        }).then(res => console.log('wat ??', res),
                err => console.log('err', err)
        )
    }

    sortByRating() {
        this.items.sortRating = !this.items.sortRating
        this.items.sortRatingDirection = this.items.sortRating ? 1 : -1
        this.items.stocks.sort((a,b) => {
            if (a.PGR < b.PGR) return (-1 * this.items.sortRatingDirection)
            else if (a.PGR > b.PGR) return this.items.sortRatingDirection
            else return 0
        })
    }

    sortByCompany() {
        this.items.sortTicker = !this.items.sortTicker
        this.items.sortTickerDirection = this.items.sortTicker ? 1 : -1
        this.items.stocks.sort((a,b) => {
            if (a.symbol < b.symbol) return (-1 * this.items.sortTickerDirection)
            else if (a.symbol > b.symbol) return this.items.sortTickerDirection
            else return 0
        })
    }

    sortByPrice() {
        this.items.sortPrice = !this.items.sortPrice
        this.items.sortPriceDirection = this.items.sortPrice ? 1 : -1
        this.items.stocks.sort((a,b) => {
            if (a.Change < b.Change) return (-1 * this.items.sortPriceDirection)
            else if (a.Change > b.Change) return this.items.sortPriceDirection
            else return 0
        })
    }

    deleteStockList() {
        this.item.deleteENTIREstockList(this.user.uid, '1364540', this.user.cookie).subscribe(del => { })
    }
}
