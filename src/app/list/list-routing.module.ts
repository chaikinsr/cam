import {NgModule} from '@angular/core'
import {Routes} from '@angular/router'
import {NativeScriptRouterModule} from 'nativescript-angular/router'
import {ListComponent} from "~/app/list/list.component"
import {ListListComponent} from "~/app/list-list/list-list.component"
import {ItemDetailComponent} from "~/app/item/item-detail.component"
import {EtfDetailComponent} from "~/app/etf-detail/etf-detail.component"
import {ListDetailComponent} from "~/app/list-detail/list-detail.component"
import {ListSubmenuComponent} from "~/app/list-submenu/list-submenu.component"
import {ListSubmenuTwoComponent} from "~/app/list-submenu-two/list-submenu-two.component"
import {SettingsComponent} from "~/app/settings/settings.component"
import {ListPowerbarComponent} from "~/app/list-powerbar/list-powerbar.component"
const CAR: Routes = [
    {   path: '',
        component: ListComponent,
        children: [
            { path: '', redirectTo: 'all'},
            { path: 'all', component: ListListComponent},
            { path: 'powbar', component: ListPowerbarComponent},
            { path: 'submenu', component: ListSubmenuComponent},
            { path: 'subtwo', component: ListSubmenuTwoComponent},
            { path: 'list-detail', component: ListDetailComponent},
            { path: 'stock-detail', component: ItemDetailComponent},
            { path: 'etf-detail', component: EtfDetailComponent},
            { path: 'settings', component: SettingsComponent}
        ]
    }
]
@NgModule({
    imports: [NativeScriptRouterModule.forChild(CAR)],
    exports: [NativeScriptRouterModule]
})
export class ListRoutingModule { }
