import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core'
import { ListRoutingModule } from './list-routing.module'
import { NativeScriptCommonModule } from 'nativescript-angular/common'
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular"
import { ListComponent } from './list.component'
import { ListListComponent } from "~/app/list-list/list-list.component"
// import { ListgroupComponent } from "~/app/listgroup/listgroup.component"
import { ListPowerbarComponent } from "~/app/list-powerbar/list-powerbar.component"
import { SharedModule } from "~/app/shared/shared.module"
import {ListDetailComponent} from "~/app/list-detail/list-detail.component"
import {ListSubmenuComponent} from "~/app/list-submenu/list-submenu.component"
import {ListSubmenuTwoComponent} from "~/app/list-submenu-two/list-submenu-two.component"

@NgModule({
  declarations: [
        ListComponent,
        ListListComponent,
        // ListgroupComponent,
        ListPowerbarComponent,
        ListDetailComponent,
        ListSubmenuComponent,
        ListSubmenuTwoComponent
  ],
  imports: [
        ListRoutingModule,
        NativeScriptCommonModule,
        NativeScriptUIListViewModule,
        SharedModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class ListModule { }
