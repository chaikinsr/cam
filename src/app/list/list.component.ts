import { Component, OnInit } from '@angular/core';
import {RouterExtensions} from "nativescript-angular";
import {ActivatedRoute} from "@angular/router";
import * as appSettings from "tns-core-modules/application-settings";
import {ApiService} from "~/app/api.service";

@Component({
  selector: 'ns-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  moduleId: module.id,
})
export class ListComponent implements OnInit {

    public user: any = {};

    constructor(private route: ActivatedRoute,
                private router: RouterExtensions,
                private api: ApiService) { }

    ngOnInit() {
        console.log('LIST COMP INIT');
        // let uid = appSettings.getString('uid');
        // let cookie = appSettings.getString('cookie');
        // this.user.starred = null;
        // this.api.getMyLists(uid, cookie).subscribe(list => {
        //     let lists = list[0]['User Lists']; // MY USER LISTS
        //     let hasStarred = false
        //     lists.forEach(list => {
        //         if (list.name == 'Starred') {
        //             console.log(' -------- found starred --------- ')
        //             this.user.starred = list.list_id // IF FOUND, THAT IS THE LIST ID WE WANT
        //             appSettings.setNumber('starred', list.list_id) // SET THE LISTID
        //             hasStarred = true
        //         }
        //         if (list.name == 'My Stocks') appSettings.setNumber('UserListId', list.list_id) // IF FOUND, THAT IS THE LIST ID WE WANT
        //         if (!hasStarred) {
        //             console.log(' ==== HAS STARRED STILL IS FALSE ====')
        //         }
        //     })
        // })
    }
}
