import {NgModule} from '@angular/core'
import {Routes} from '@angular/router'
import {NativeScriptRouterModule} from 'nativescript-angular/router'
import {MarketComponent} from "~/app/market/market.component"
import {ItemDetailComponent} from "~/app/item/item-detail.component"
import {EtfDetailComponent} from "~/app/etf-detail/etf-detail.component"
const routes: Routes = [
    {   path: '', component: MarketComponent },
    {   path: 'stock-detail', component: ItemDetailComponent },
    {   path: 'etf-detail', component: EtfDetailComponent },
]
@NgModule({
  imports: [NativeScriptRouterModule.forChild(routes)],
  exports: [NativeScriptRouterModule]
})
export class MarketRoutingModule { }
