import {Component, ElementRef, OnInit, ViewChild, ViewContainerRef} from '@angular/core'
import {RouterExtensions} from "nativescript-angular"
import {ApiService} from "~/app/api.service"
import * as appSettings from "tns-core-modules/application-settings"
import {ItemService} from "~/app/item/item.service"
import * as utils from 'tns-core-modules/utils/utils'
import {ActivatedRoute} from "@angular/router"
import {ScrollEventData} from "tns-core-modules/ui/scroll-view"
import {ModalDialogService, ModalDialogOptions} from "nativescript-angular"
import {EtfDetailComponent} from "~/app/etf-detail/etf-detail.component"
import {ItemDetailComponent} from "~/app/item/item-detail.component"
import {UserService} from "~/app/user.service"
import {tap} from "rxjs/operators"
import {forkJoin} from "rxjs"
import {GridLayout} from "tns-core-modules/ui/layouts/grid-layout";

@Component({
    selector: 'ns-market',
    templateUrl: './market.component.html',
    styleUrls: ['./market.component.scss'],
    moduleId: module.id,
    providers: [ModalDialogService]
})

export class MarketComponent implements OnInit {

    date: any = {}
    lists: any = {}
    user: any = {}
    show: any = {}
    ads: any = {}
    index: any = []
    sectorTracker: any = []
    sectorOptions: any = {}
    industryFocus: any = {}
    gainStocks: any = []
    loseStocks: any = []
    trendingOptions: any = {}
    headlines: any = {}
    earningsUp: any = []
    earningsUpAfter: any = []
    earningsRecent: any = {}
    marketStatus: string
    morningInsights: boolean = true
    morningInsight: any = {}
    config: any = {}

    @ViewChild('MarketScroll', { static: true }) marketScroll : ElementRef<any>
    @ViewChild('IndustrySpotlightPow', { static: true }) industrySpotlightPow: ElementRef<any>

    constructor(private router: RouterExtensions,
                public api: ApiService,
                public item: ItemService,
                public userService: UserService,
                private activeRoute: ActivatedRoute,
                private modalService: ModalDialogService,
                private viewContainerRef: ViewContainerRef) {
                this.morningInsights = false
                this.sectorOptions = {
                    dayMin: 0,
                    dayMax: 1,
                    toggle: false,
                    carat: 'percent',
                    date_range: '1D'
                }
    }

    ngOnInit() {
        console.log('--- MARKET COMPONENT INIT ---')
        appSettings.setBoolean('LoggedInBefore', true)
        this.setup()
    }

    private setup() {
        this.user = {
            email: appSettings.getString('email'),
            cookie: appSettings.getString('cookie'),
            uid: appSettings.getString('uid'),
            starred: appSettings.getNumber('starred')
        }
        this.loadMyPerms()
        this.date = Date.now()
        this.api.isMarketOpen(appSettings.getString('cookie')).subscribe(res => {
            this.marketStatus = JSON.parse(res['is_market_open'])
        })
        this.loadMarketView()
    }

    public refresh() {
        this.date = Date.now()
        this.show = {
            index: false,
            sector: false,
            trending: true,
            industry: false,
            headlines: true,
            earnings: true
        }
        this.prepareSectorTracker()
        this.prepareMajorIndices()
        this.api.getIndustryFocus(this.user.cookie)
            .subscribe(res => {
                let firstSentence = res['subSector_summary']["sentence1A"] + res['subSector_summary']["sentence1B"]
                let cleanFirst = firstSentence.replace(/\<[^<>]*\>/g, '')
                let secondSentence = res['subSector_summary']["sentence2A"] + res['subSector_summary']["sentence2B"] + res['subSector_summary']["sentence2C"] + res['subSector_summary']['sentence3']
                let cleanSecond = secondSentence.replace(/\<[^<>]*\>/g, '')
                this.industryFocus = {
                    first: cleanFirst,
                    second: cleanSecond,
                    name: res['subSector_summary']['subsector_name'],
                    green: res['subSector_summary']['green'],
                    red: res['subSector_summary']['red'],
                    yellow: res['subSector_summary']['yellow'],
                    ticker: res['subSector_summary']['composite_ticker'],
                    items: res['subSector_symbols_data']
                }
                let total = this.industryFocus.green + this.industryFocus.red + 1
                let bullsOverTotal = (this.industryFocus.green / total)
                let bearsOverTotal = (this.industryFocus.red / total)
                let bullsPercent = (bullsOverTotal * 80)
                let bearsPercent = (bearsOverTotal * 80)
                let bullsMin = Math.max(bullsPercent, 14)
                let bearsMin = Math.max(bearsPercent, 14)
                this.industryFocus.bullsPer = bullsMin
                this.industryFocus.bearsPer = bearsMin
                this.show.industry = true
                this.item.getListInfo(this.user.uid, this.user.starred, this.user.cookie).subscribe(item => {
                    item.symbols.forEach(ticker => {
                        ticker.symbol == this.industryFocus.ticker
                            ? this.industryFocus.starred = true
                            : null
                    })
                })
            })
    }

    getSize(args) {
        setTimeout(() => {
            let stack = <GridLayout>args.object
            let stackSize = args.object.getActualSize()
            let stackWidth = stackSize.width
            let stackHeight = stackSize.height
            this.config.height = stackHeight
            this.config.width = stackWidth
        }, 400)
    }

    loadMyPerms() {
        this.userService.getUserPerms(this.user.uid).subscribe(perms => {
            let permsArr = perms.split(',')
            let keys = [],
                vals = []
            permsArr.forEach(item => {
                let key = item.split(':')
                keys.push(key[0].trim())
                vals.push(key[1].trim())
            })
            let mkt = vals[0] == 'true',
                mor = vals[1] == 'true',
                mar = vals[2] == 'true',
                pre = vals[3] == 'true',
                userPerms = {
                    marketSurvivalGuide: mkt,
                    morningInsights: mor,
                    marketInsights: mar,
                    premium: pre
                }
            this.user.perms = userPerms
            this.show.ads = true
            if (!userPerms.premium) {
                this.api.getWPAds().subscribe(ads => {
                    let regex = /(https?:\/\/.*\.(?:png|jpg))/;
                    let img = ads[0]['content']['rendered']
                    let imgUrl = regex.exec(img)
                    let URL = imgUrl[0].replace('&#215;', 'x') // imgUrl replace &#215; with 'x' as in 1024x720
                    let linkItem = ads[0]['excerpt']['rendered']
                    let link = linkItem.slice(3, linkItem.length - 5)
                    this.ads = {
                        jpg: URL,
                        url: link
                    }
                })
            }
        })

    }

    shouldIShowAds() {
        return this.show.ads && !this.user.perms.premium
    }

    getAdSrc() {
        return this.ads.jpg
    }

    goToAds() {
        utils.openUrl(this.ads.url)
    }

    onScroll(args: ScrollEventData) {
        if (args.scrollY < -140) {
            this.refresh()
        }
    }

    industryCols() {
        let bull = this.config.bullsRatio
        let bear = this.config.bearsRatio
        if (bull > bear) {
            let r = bull/bear
            let floor = Math.floor(r)
            let limit = Math.min(floor, 8)
            let cols = limit + '*, 40, *'
            return cols
        } else {
            let r = bear/bull
            let floor = Math.floor(r)
            let cols = '*, 40, ' + floor + '*'
            return cols
        }
    }

    loadMarketView() {
        this.prepareSectorTracker()
        this.prepareHeadlines()
        this.api.getMyLists(this.user.uid, this.user.cookie).subscribe(list => {
            let lists = list[0]['User Lists']
            let star = lists.find(list => {
                return list.name == 'Starred'
            })
            this.user.starred = star.list_id
        })
        this.prepareMajorIndices()
        this.api.getIndustryFocus(this.user.cookie)
            .subscribe(res => {
                let firstSentence = res['subSector_summary']["sentence1A"] + res['subSector_summary']["sentence1B"]
                let cleanFirst = firstSentence.replace(/\<[^<>]*\>/g, '')
                let secondSentence = res['subSector_summary']["sentence2A"] + res['subSector_summary']["sentence2B"] + res['subSector_summary']["sentence2C"] + res['subSector_summary']['sentence3']
                let cleanSecond = secondSentence.replace(/\<[^<>]*\>/g, '')
                this.industryFocus = {
                    first: cleanFirst,
                    second: cleanSecond,
                    name: res['subSector_summary']['subsector_name'],
                    green: res['subSector_summary']['green'],
                    red: res['subSector_summary']['red'],
                    yellow: res['subSector_summary']['yellow'],
                    ticker: res['subSector_summary']['composite_ticker'],
                    items: res['subSector_symbols_data']
                }
                let bulls: number = res['subSector_summary']['green']
                let bears: number = res['subSector_summary']['red']
                let tot: number = bulls + bears + 1
                let bullsRatio: number = bulls / tot
                let bearsRatio: number = bears / tot
                this.config.bullsRatio = parseFloat(bullsRatio.toFixed(4))
                this.config.bearsRatio = parseFloat(bearsRatio.toFixed(4))
                this.show.industry = true
                this.item.getListInfo(this.user.uid, this.user.starred, this.user.cookie)
                    .subscribe(item => {
                        item.symbols.forEach(ticker => {
                            if (ticker.symbol == this.industryFocus.ticker) this.industryFocus.starred = true
                        })
                    })
            })

        this.api.getTrendingStocks(this.user.cookie)
            .subscribe(res => {
                let gainStocks = [], loseStocks = []
                for (let x in res['gainer_stocks']) {
                    gainStocks.push({
                        symbol: res['gainer_stocks'][x]['symbol'],
                        pgr: res['gainer_stocks'][x]['pgr_rating'],
                        neutral_status: res['gainer_stocks'][x]['neutral_status'],
                        change: res['gainer_stocks'][x]['change'],
                        company: res['gainer_stocks'][x]['company'],
                        percent: res['gainer_stocks'][x]['change_percent'],
                        last: res['gainer_stocks'][x]['last_price']
                    })
                }
                this.gainStocks = gainStocks
                for (let x in res['loser_stocks']) {
                    loseStocks.push({
                        symbol: res['loser_stocks'][x]['symbol'],
                        pgr: res['loser_stocks'][x]['pgr_rating'],
                        neutral_status: res['loser_stocks'][x]['neutral_status'],
                        change: res['loser_stocks'][x]['change'],
                        company: res['loser_stocks'][x]['company'],
                        percent: res['loser_stocks'][x]['change_percent'],
                        last: res['loser_stocks'][x]['last_price']
                    })
                }
                this.loseStocks = loseStocks
                let count = []
                this.gainStocks.filter((val) => {
                    val = parseFloat(val.percent)
                    count.push(val)
                })
                let count2 = []
                this.loseStocks.filter((val) => {
                    val = parseFloat(val.percent)
                    count2.push(val)
                })
                this.trendingOptions = {
                    gain_max: Math.max(...count),
                    gain_min: Math.min(...count),
                    lose_max: Math.max(...count2),
                    lose_min: Math.min(...count2)
                }
                this.show.trending = true
            });

        this.api.getEarningsUpdateRecent(this.user.cookie)
            .subscribe(res => {
                let day = res['report_day']
                this.earningsRecent.day = day
                res['has_earning_data'] > 0
                    ? this.earningsRecent.data = res['earning_data']
                    : this.earningsRecent.data = null
            })

        this.api.getEarningsUpdateUpcoming(this.user.cookie)
            .subscribe(res => {
                let day = res[0]['day'] ? res[0]['day'] : null
                this.earningsUp.day = day
                let before = ''
                let before_open_bears = res[0].before_open_bears
                let before_open_neutrals = res[0].before_open_neutrals
                let before_open_bulls = res[0].before_open_bulls
                before_open_bears.length > 0 ? before = before_open_bears + ',' : before = ''
                before_open_neutrals.length > 0 ? before += before_open_neutrals + ',' : ''
                before_open_bulls.length > 0 ? before += before_open_bulls : ''
                before = before.replace(/\s+/g, '')
                let split = before.split(',')
                if (split.length > 1) {
                    for (let count = 0; count < split.length; count++) {
                        this.prepareEarningsUp(split[count])
                    }
                }
                let after = ''
                let after_close_bears = res[0].after_close_bears
                let after_close_neutrals = res[0].after_close_neutrals
                let after_close_bulls = res[0].after_close_bulls
                after_close_bears.length > 0 ? after = after_close_bears + ',' : after = ''
                after_close_neutrals.length > 0 ? after += after_close_neutrals + ',' : ''
                after_close_bulls.length > 0 ? after += after_close_bulls : ''
                after = after.replace(/\s+/g, '')
                let splitAfter = after.split(',')
                for (let count = 0; count < splitAfter.length; count++) {
                    this.getSymbolDataAfter(splitAfter[count])
                }
                this.show.earnings = true
            })
    }

    prepareEarningsUp(tickers) {
        this.item.getSymbolData(tickers, this.user.uid, this.user.cookie).subscribe(res => {
            let earningsUp = []
            if (res['metaInfo'] && res['metaInfo'][0]) {
                earningsUp.push({
                    symbol: res['metaInfo'][0]['symbol'],
                    name: res['metaInfo'][0].name,
                    pgr: res['metaInfo'][0].PGR,
                    raw_pgr: res['metaInfo'][0].raw_PGR
                })
            }
            this.earningsUp = earningsUp
            earningsUp.length > 1 ? this.quickSort(this.earningsUp, 'pgr') : null
        })
    }

    getEarningsBeatAmount(item) {
        return item.eps_diff == 0
            ? ('Met Expectations')
            : item.eps_diff > 0
                ? ('Beat by ' + item.eps_diff)
                : ('Missed by ' + item.eps_diff)
    }

    getEarningsClass(item) {
        return item.eps_diff == 0 ? 'black' : (item.eps_diff > 0 ? 'green' : 'red')
    }

    quickSort(item, by) {
        item.sort((a,b) => a[by] < b[by] ? 1 : a[by] > b[by] ? -1 : 0 )
    }

    scrollToTop() {
        this.marketScroll.nativeElement.scrollToVerticalOffset(0, true)
    }

    getMarketStatus() {
        return this.marketStatus ? ' US Markets are OPEN ' : ' US Markets are CLOSED '
    }

    getEarningsRecentDay() {
        if (this.earningsRecent.day) return this.earningsRecent.day.toUpperCase() + '\'S SURPRISES'
    }

    getEarningsUpDay() {
        return this.earningsUp.day ? 'REPORTING ' + this.earningsUp.day.toUpperCase() : null
    }

    getSymbolDataAfter(after) {
        this.item.getSymbolData(after, this.user.uid, this.user.cookie)
            .subscribe(res => {
                let earningsUpAfter = [];
                if (res['metaInfo'] && res['metaInfo'][0]) {
                    earningsUpAfter.push({
                        symbol: res['metaInfo'][0].symbol,
                        name: res['metaInfo'][0].name,
                        pgr: res['metaInfo'][0].PGR,
                        raw_pgr: res['metaInfo'][0].raw_PGR
                    })
                }
                this.earningsUpAfter = earningsUpAfter;
            })
    }

    takeMeToPowerBar(ticker) {
        appSettings.setString('etf-detail', ticker);
        appSettings.setBoolean('EtfPower', true);

        this.userService.checkSession()


        this.router.navigate([ 'etf-detail'], {
            relativeTo: this.activeRoute,
            transition: {
                name: 'slideTop',
                duration: 350
            }
        })
    }

    toggle(section) {
        this.show[section] = !this.show[section]
    }

    prepareMajorIndices() {
        let dia = this.item.getSymbolData('DIA', this.user.uid, this.user.cookie),
        spy = this.item.getSymbolData('SPY', this.user.uid, this.user.cookie),
        qqq = this.item.getSymbolData('QQQ', this.user.uid, this.user.cookie),
        iwv = this.item.getSymbolData('IWV', this.user.uid, this.user.cookie),
        iwm = this.item.getSymbolData('IWM', this.user.uid, this.user.cookie)
        forkJoin([dia, spy, qqq, iwv, iwm]).subscribe(res => {
            let index = []
            for (let x = 0; x < res.length; x++) {
                index.push({
                    symbol:     res[x]['metaInfo'][0]['symbol'],
                    name:       res[x]['metaInfo'][0].name,
                    pgr:        res[x]['metaInfo'][0].PGR,
                    raw:        res[x]['metaInfo'][0].raw_PGR,
                    last:       res[x]['metaInfo'][0].Last,
                    percent:    res[x]['metaInfo'][0]['Percentage '],
                    change:     res[x]['metaInfo'][0].Change,
                })
            }
            this.index = index
            this.index.toggle = false // index options ?
            this.index.carat = 'pgr'
            this.majorIndicesQuickSort('pgr')
            this.show.index = true
        })
    }

    prepareHeadlines() {
        this.api.stockInNews(this.user.cookie).subscribe(news => {
            let headlines = []
            news.map(item => {
                headlines.push({
                    ticker: item.symbol,
                    name: item.company,
                    pgr: item.pgr_rating,
                    neutral: item.neutral_status,
                    headlines: [],
                    starred: false
                })
            })
            this.headlines = headlines
            this.item.getListInfo(this.user.uid, this.user.starred, this.user.cookie)
                .subscribe(item => {
                    item.symbols.forEach(ticker => {
                        for (let i=0; i<4; i++) {
                            if (ticker.symbol == this.headlines[i].ticker) this.headlines[i].starred = true
                        }
                    })
                })
            this.item.getHeadlines(this.headlines[0].ticker, this.user.cookie).subscribe(first => { this.headlines[0].headlines = first.headlines.slice(0,6)})
            this.item.getHeadlines(this.headlines[1].ticker, this.user.cookie).subscribe(second => { this.headlines[1].headlines = second.headlines.slice(0,6)})
            this.item.getHeadlines(this.headlines[2].ticker, this.user.cookie).subscribe(third => { this.headlines[2].headlines = third.headlines.slice(0,6)})
            this.item.getHeadlines(this.headlines[3].ticker, this.user.cookie).subscribe(fourth => { this.headlines[3].headlines = fourth.headlines.slice(0,6)})
            this.show.headlines = true
        })
    }

    getHeadlineOne() {
        return this.headlines[0].headlines
    }

    getHeadlineTwo() {
        return this.headlines[1].headlines
    }

    getHeadlineThree() {
        return this.headlines[2].headlines
    }

    getHeadlineFour() {
        return this.headlines[3].headlines
    }

    starred() {
        return this.industryFocus.starred ? 'starredActive' : null
    }

    headlineStarred(n) {
        return this.headlines[n].starred ? 'starredActive' : null
    }

    goToEtfDetail(symbol) {
        appSettings.setString('etf-detail', symbol)
        appSettings.setBoolean('EtfPower', false)
        const options: ModalDialogOptions = {
            viewContainerRef: this.viewContainerRef,
            fullscreen: true,
            context: {}
        }

        this.userService.checkSession().subscribe(res => {
            console.log(' ============== go to etf checking perm ========== >', res)


            this.modalService.showModal(EtfDetailComponent, options)
        })
    }

    goToStockDetail(symbol) {
        appSettings.setString('symbol', symbol)
        const options: ModalDialogOptions = {
            viewContainerRef: this.viewContainerRef,
            fullscreen: true,
            context: {}
        }

        // dont show modal until you check that user can

        this.userService.checkSession().subscribe(res => {
            console.log(' ============== go to stock checking perm ========== >', res)

            this.modalService.showModal(ItemDetailComponent, options)
        })
    }

    prepareSectorTracker() {
        let sec1 = this.item.getSymbolData('XLC', this.user.uid, this.user.cookie),
        sec2 = this.item.getSymbolData('XLK', this.user.uid, this.user.cookie),
        sec3 = this.item.getSymbolData('SPY', this.user.uid, this.user.cookie),
        sec4 = this.item.getSymbolData('XLB', this.user.uid, this.user.cookie),
        sec5 = this.item.getSymbolData('XLF', this.user.uid, this.user.cookie),
        sec6 = this.item.getSymbolData('XLI', this.user.uid, this.user.cookie),
        sec7 = this.item.getSymbolData('XLP', this.user.uid, this.user.cookie),
        sec8 = this.item.getSymbolData('XLRE', this.user.uid, this.user.cookie),
        sec9 = this.item.getSymbolData('XLU', this.user.uid, this.user.cookie),
        sec10 = this.item.getSymbolData('XLY', this.user.uid, this.user.cookie),
        sec11 = this.item.getSymbolData('XLV', this.user.uid, this.user.cookie),
        sec12 = this.item.getSymbolData('XLE', this.user.uid, this.user.cookie)
        forkJoin([sec1, sec2, sec3, sec4, sec5, sec6, sec7, sec8, sec9, sec10, sec11, sec12])
            .pipe(
                tap(results => { // SORT BY PGR DESC THEN BY TICKER ASC
                    results.sort((x,y) => (y['metaInfo'][0]['PGR'] > x['metaInfo'][0]['PGR']) ? 1 : (x['metaInfo'][0]['PGR'] === y['metaInfo'][0]['PGR']) ? ((x['metaInfo'][0]['symbol'] > y['metaInfo'][0]['symbol']) ? 1 : -1) : -1)
                })
            ).subscribe(item => {
                this.api.getSectorsInfo(this.user.cookie)
                    .pipe(
                        tap(sectors => {
                            sectors['sectors_performance_table_data'].sort((x,y) => (y['PGR'] > x['PGR']) ? 1 : (x['PGR'] === y['PGR']) ? ((x['composite_ticker'] > y['composite_ticker']) ? 1 : -1) : -1)
                        })
                    ).subscribe( sectors => {
                        let box = []
                        for (let i = 0; i < 12; i++){
                            box.push({
                                symbol: sectors['sectors_performance_table_data'][i]['composite_ticker'],
                                pgr: sectors['sectors_performance_table_data'][i]['PGR'],
                                raw_pgr: sectors['sectors_performance_table_data'][i]['raw_PGR'],
                                ind_name: sectors['sectors_performance_table_data'][i]['etf_name'],
                                full_name: sectors['sectors_performance_table_data'][i]['full_name'],
                                '1D': item[i]['metaInfo'][0]['Percentage '],
                                '5D': sectors['sectors_performance_table_data'][i]['one_week_change'],
                                '1M': sectors['sectors_performance_table_data'][i]['one_month_change'],
                                '3M': sectors['sectors_performance_table_data'][i]['three_month_change'],
                                '6M': sectors['sectors_performance_table_data'][i]['six_month_change'],
                                '1Y': sectors['sectors_performance_table_data'][i]['year_change'],
                                'YTD': sectors['sectors_performance_table_data'][i]['ytd_change']
                            })
                        }
                        let count = [] // sort by % chg and make bars
                        box.filter((val) => { // filter by %
                            count.push(val['1D'])
                        })
                        box.sort((x,y) => (y['1D'] > x['1D']) ? 1 : y['1D'] < x['1D'] ? -1 : 0)
                        this.sectorOptions.dayMax = Math.max(...count)
                        this.sectorOptions.dayMin = Math.min(...count)
                        this.sectorTracker = box
                        this.show.sector = true
                    })
            })
    }

    openBrowser(url) {
        utils.openUrl(url)
    }

    get showMorningInsights(): boolean {
        return this.morningInsights
    }

    get getIndustryFocusName() {
        return this.industryFocus.name
    }

    get getIndFocSumm1() {
        return this.industryFocus.first
    }

    get getIndFocSumm2() {
        return this.industryFocus.second
    }

    get getIndGreen() {
        return this.industryFocus.green
    }

    get getIndYellow() {
        return this.industryFocus.yellow
    }

    get getIndRed() {
        return this.industryFocus.red
    }

    itemLastFormatter(item) {
        return '$' + item.toFixed(2)
    }

    itemChangeFormatter(item) {
        return item > 0
            ? '+' + item.toFixed(2)
            : item.toFixed(2)
    }

    itemPercentFormatter(item) {
        return item > 0
            ? '(+' + item + '%)'
            : '(' + item + '%)'
    }

    itemPercentFormatterNoSign(item) {
        return '(' + item + '%)'
    }

    healthColorFormatter(item) {
        return item == 0
            ? 'black'
            : (item > 0
                ? 'green'
                : 'red')
    }

    pgrRatingETF(pgr, raw) {
        return this.item.getPGRratingETF(pgr, raw)
    }

    pgrRatingStockNeut(pgr, neut) {
        return this.item.getPGRratingStockNeutStatusConfig(pgr, neut)
    }

    pgrRatingNoTxtStockNeut(pgr, neut) {
        return this.item.getPGRiconNoTxtNEUTconfig(pgr, neut)
    }

    pgrRatingNoText(pgr, raw) {
        return this.item.getPGRiconNoTxtRAWconfig(pgr, raw)
    }

    sectorTrackerDateChange(range) {
        this.sectorOptions.date_range = range
        this.sectorTrackerQuickSort(range)
    }

    sectorTrackerQuickSort(item) {
        let count = []
        this.sectorTracker.filter((val) => {
            count.push(val[item])
        })
        this.sectorOptions.dayMax = Math.max(...count)
        this.sectorOptions.dayMin = Math.min(...count)
        this.sectorTracker.sort((a,b) => {
            let sorted = b[item] < a[item] ? -1 : b[item] > a[item] ? 1 : 0;
            return sorted;
        })
    }

    sectorTrackerSort(sortBy) {
        this.sectorOptions.toggle = !this.sectorOptions.toggle
        this.sectorOptions.carat = sortBy
        !this.sectorOptions.toggle
            ? this.sectorTracker.sort((a,b) => {
                let sorted = a[sortBy] < b[sortBy] ? 1 : a[sortBy] > b[sortBy] ? -1 : 0
                return sorted
            })
            : this.sectorTracker.sort((a,b) => {
                let sorted = b[sortBy] < a[sortBy] ? 1 : b[sortBy] > a[sortBy] ? -1 : 0
                return sorted
            }
        )
    }

    majorIndicesSort(sortBy) {
        this.index.toggle = !this.index.toggle
        this.index.carat = sortBy
        !this.index.toggle
            ? this.index.sort((a,b) => {
                let sorted = a[sortBy] < b[sortBy] ? 1 : a[sortBy] > b[sortBy] ? -1 : 0
                return sorted
            })
            : this.index.sort((a,b) => {
                let sorted = b[sortBy] < a[sortBy] ? 1 : b[sortBy] > a[sortBy] ? -1 : 0
                return sorted
            })
    }

    majorIndicesQuickSort(sortBy) {
        this.index.sort((a,b) => {
            let sorted = a[sortBy] < b[sortBy] ? 1 : a[sortBy] > b[sortBy] ? -1 : 0
            return sorted
        })
    }

    sectorTrackerDisplayData(item, range) {
        return item[range] + '%'
    }

    setDataColor(item, range) { // SECTOR TRACKER SLIDERS
        return item[range] == 0 ? 'black' : (item[range] > 0 ? 'green' : 'red')
    }

    setDataColorBackground(item, range) {
        return item[range] == 0 ? 'black' : (item[range] > 0 ? 'greenbg' : 'redbg')
    }

    setDataColorBackgroundLight(item, range) {
        return item[range] == 0 ? 'black' : (item[range] > 0 ? 'greenbglt' : 'redbglt')
    }

    findWidth(item, min, max, range){
        return item[range] > 0 ? item[range] * 150/max : item[range] * 150/min;
    }

    setTrendingColors(percent) {
        percent = parseFloat(percent)
        return percent == 0
            ? 'black'
            : (percent > 0
            ? 'green'
            : 'red')
    }

    setTrendingBackground(percent) {
        percent = parseFloat(percent)
        return percent > 0 ? 'greenbg' : 'redbg'
    }

    findWidthGain(item, max) {
        item = parseFloat(item)
        return item * 150/max
    }

    findWidthLose(item, max) {
        item = parseFloat(item)
        return item * 150/max
    }

    findWidthTrending(item, max) {
        item = parseFloat(item)
        return item * 150/max
    }

    industryStar() {
        return this.industryFocus.starred
            ? this.item.deleteStockFromList(this.industryFocus.ticker, this.user.uid, this.user.starred, this.user.cookie)
                .subscribe(deletion => {
                    this.industryFocus.starred = false
                })
            : this.item.addStockIntoList(this.industryFocus.ticker, this.user.uid, this.user.starred, this.user.cookie)
                .subscribe(addition => {
                    this.industryFocus.starred = true
                }
            )
    }

    headlineStar(n) {
        return this.headlines[n].starred
            ? this.item.deleteStockFromList(this.headlines[n].ticker, this.user.uid, this.user.starred, this.user.cookie)
                .subscribe(deletion => {
                    this.headlines[n].starred = false
                })
            : this.item.addStockIntoList(this.headlines[n].ticker, this.user.uid, this.user.starred, this.user.cookie)
                .subscribe(addition => {
                    this.headlines[n].starred = true
                }
            )
    }
}
