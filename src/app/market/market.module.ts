import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { MarketRoutingModule } from './market-routing.module';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { MarketComponent } from './market.component';
import { SharedModule } from "~/app/shared/shared.module";
// import { YoutubePlayerModule } from 'nativescript-youtubeplayer/angular';
import {ModalComponent} from "~/app/modal/modal.component";

@NgModule({
  declarations: [
      MarketComponent,
      ModalComponent
  ],
  imports: [
      MarketRoutingModule,
      NativeScriptCommonModule,
      NativeScriptUIListViewModule,
      SharedModule,
      // YoutubePlayerModule
  ],
  schemas: [NO_ERRORS_SCHEMA],
    entryComponents: [ModalComponent]
})
export class MarketModule { }
