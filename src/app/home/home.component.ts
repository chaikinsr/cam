import { Component, OnInit } from '@angular/core';
import { UserService } from "~/app/user.service";
import {RouterExtensions} from "nativescript-angular";
import {ActivatedRoute} from "@angular/router";
import { SelectedIndexChangedEventData } from "tns-core-modules/ui/tab-view";

@Component({
  selector: 'ns-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  moduleId: module.id,
})
export class HomeComponent implements OnInit {

    public tabSelectedIndex: number;
    public tabSelectedIndexResult: string;

    constructor(private userService: UserService,
                private router: RouterExtensions,
                private activeRoute: ActivatedRoute) {
    }

    ngOnInit() {
        console.log('home component init');

        // this.router.navigate([{ outlets: { lists: ['lists'], headlines: ['headlines'] } }], { relativeTo: this.activeRoute })
        //     .then(res => {
        //         console.log('res?', res);
        //     }, err => {
        //         console.log('home router error', err);
        //     });
    }

    onSelectedIndexChanged(args: SelectedIndexChangedEventData) {

        console.log('old', args.oldIndex);
        console.log('new', args.newIndex);

        if (args.newIndex == 0) {
            console.log('win!');
            this.router.navigate(['market'])
        }

        // if (args.oldIndex !== -1) {
        //     const newIndex = args.newIndex;
        //     if (newIndex === 0) {
        //         this.tabSelectedIndexResult = "Profile Tab (tabSelectedIndex = 0 )";
        //     } else if (newIndex === 1) {
        //         this.tabSelectedIndexResult = "Stats Tab (tabSelectedIndex = 1 )";
        //     } else if (newIndex === 2) {
        //         this.tabSelectedIndexResult = "Settings Tab (tabSelectedIndex = 2 )";
        //     }
        //     alert(`Selected index has changed ( Old index: ${args.oldIndex} New index: ${args.newIndex} )`);
        // }
    }


}
