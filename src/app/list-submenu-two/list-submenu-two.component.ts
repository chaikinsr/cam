import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {ItemService} from "~/app/item/item.service";
import {ModalDialogOptions, ModalDialogService, RouterExtensions} from "nativescript-angular";
import {ActivatedRoute} from "@angular/router";
import * as appSettings from "tns-core-modules/application-settings";
import {ListEtfHoldingsComponent} from "~/app/list-etf-holdings/list-etf-holdings.component";
import {EtfDetailComponent} from "~/app/etf-detail/etf-detail.component";

@Component({
    selector: 'ns-list-submenu-two',
    templateUrl: './list-submenu-two.component.html',
    styleUrls: ['./list-submenu-two.component.css']
})
export class ListSubmenuTwoComponent implements OnInit {

    list: any = {}
    user: any = {}
    ready: boolean = false

    constructor(private item: ItemService,
                private router: RouterExtensions,
                private route: ActivatedRoute,
                private modalService: ModalDialogService,
                private viewContainerRef: ViewContainerRef) { }

    ngOnInit() {
        this.list.name = appSettings.getString('ListTitle');
        this.user.cookie = appSettings.getString('cookie');
        this.user.uid = appSettings.getString('uid');
        this.list.id = appSettings.getString('subTwo');
        this.list.name = appSettings.getString('subTwoName')
        this.item.getListData(this.user.uid, this.list.id, this.user.cookie)
            .subscribe(res => {
                let data = res['extended_lists']
                let ans = []
                //       ^.*(?=(\ -))
                // ---- this regex finds EVERYTHING BEFORE -
                // let anotherTest = RegExp('^.*(?=(\\ -))*','g')
                data.forEach(d => {
                    let id = d.list_id
                    let ticker = d['list_name'].split(' -')[0]
                    let pow = d['powerBar'].split(',')

                    this.item.getSymbolData(ticker, this.user.uid, this.user.cookie)
                        .subscribe(s => {
                            ans.push({
                                name: d['list_name'],
                                bear: pow[0],
                                neut: pow[1],
                                bull: pow[2],
                                list: d['list_id'],
                                ticker: ticker,
                                pgr: s['metaInfo'][0]['PGR'],
                                raw: s['metaInfo'][0]['raw_PGR']
                            })
                            this.list.items = ans
                            this.list.items.sort((a,b) => {
                                let sorted = b['pgr'] < a['pgr'] ? -1 : b['pgr'] > a['pgr'] ? 1 : 0
                                return sorted
                            })
                            this.ready = true
                        })
                })
            })
    }

    pgrRatingETF(pgr, raw) {
        return this.item.getPGRratingETF(pgr, raw)
    }

    goToETFDetail(i) {
        console.log('going to: ---------------> ', i)
        appSettings.setString('etf-detail', i.ticker)
        appSettings.setNumber('ListId', i.list)
        appSettings.setString('ListName', i.name)
        const options: ModalDialogOptions = {
            viewContainerRef: this.viewContainerRef,
            fullscreen: true,
            context: {}
        }
        this.modalService.showModal(EtfDetailComponent, options)
    }

    goToListDetail(item){
        appSettings.setNumber('ListId', item.list)
        appSettings.setString('ListName', item.name)
        const options: ModalDialogOptions = {
            viewContainerRef: this.viewContainerRef,
            fullscreen: true,
            context: {}
        }
        this.modalService.showModal(ListEtfHoldingsComponent, options)
    }

    setBullWidth(item) {
        let total = (item.bull+item.bear)
        let bullishness = (item['bull'] / total)
        let math = (bullishness * 90)
        return Math.max(math,40)
    }

    setBearWidth(item) {
        let total = (item.bull+item.bear)
        let bearishness = (item.bear / total)
        let math = (bearishness * 90)
        return Math.max(math,40)
    }
}

