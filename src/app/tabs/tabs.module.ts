import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptRouterModule, NSEmptyOutletComponent } from "nativescript-angular";
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { TabsComponent } from './tabs.component';

@NgModule({
  declarations: [TabsComponent],
  imports: [
      NativeScriptCommonModule,
      NativeScriptRouterModule,
      NativeScriptRouterModule.forChild([
            {   path: 'tabs',
                component: TabsComponent,
                children: [
                  {
                      path: 'market',
                      outlet: 'marketTab',
                      component: NSEmptyOutletComponent,
                      loadChildren: '~/app/market/market.module#MarketModule'
                  },
                  {
                      path: 'list',
                      outlet: 'listTab',
                      component: NSEmptyOutletComponent,
                      loadChildren: '~/app/list/list.module#ListModule'
                  },
                  {
                      path: 'search',
                      outlet: 'searchTab',
                      component: NSEmptyOutletComponent,
                      loadChildren: '~/app/search/search.module#SearchModule'
                  },
                  {
                      path: 'insight',
                      outlet: 'insightTab',
                      component: NSEmptyOutletComponent,
                      loadChildren: '~/app/insights/insights.module#InsightsModule'
                  },
                  {
                      path: 'explore',
                      outlet: 'exploreTab',
                      component: NSEmptyOutletComponent,
                      loadChildren: '~/app/explore/explore.module#ExploreModule'
                  }
            ]}
          ])
  ],
    exports: [TabsComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class TabsModule { }
