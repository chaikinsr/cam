import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from "nativescript-angular";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'ns-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.css'],
  moduleId: module.id,
})
export class TabsComponent implements OnInit {

  constructor(
      private routerExtension: RouterExtensions,
      private activeRoute: ActivatedRoute) { }

  ngOnInit() {
      this.routerExtension.navigate([
          { outlets:
                  {
                      insightTab: ['insight'],
                      marketTab: ['market'],
                      listTab: ['list'],
                      searchTab: ['search'],
                      // exploreTab: ['explore']
                  }
          }
      ], { relativeTo: this.activeRoute })
  }
}
