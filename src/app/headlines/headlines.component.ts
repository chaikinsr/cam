import { Component, OnInit } from '@angular/core';
import * as appSettings from "tns-core-modules/application-settings";
import {ItemService} from "~/app/item/item.service";
import * as utilityModule from "tns-core-modules/utils/utils";

@Component({
  selector: 'ns-headlines',
  templateUrl: './headlines.component.html',
  styleUrls: ['./headlines.component.css'],
  moduleId: module.id,
})
export class HeadlinesComponent implements OnInit {

    public stock: any = {};
    public user: any = {};
    public imageReady: any = false;

    constructor(private itemService: ItemService) { }

    ngOnInit() {
        console.log(' --- HEADLINES COMPONENT INIT --- ');

        let today = new Date();
        let dd = today.getDate();
        let mm = today.getMonth() + 1;
        let yyyy = today.getFullYear();

        this.stock.date = mm + '/' + dd + '/' + yyyy;
        this.stock.symbol = appSettings.getString('symbol');
        this.stock.name = appSettings.getString('name');
        this.user.cookie = appSettings.getString('cookie')
        this.user.uid = appSettings.getString('uid');

        this.itemService.getHeadlines(this.stock.symbol, this.user.cookie)
            .subscribe(res => {
                this.stock.news = res['headlines'];
            }, err => {
                console.log('err', err)
            })
    }

    onTap(url): void {
        console.log('going to!', url);
        utilityModule.openUrl(url);
    }

    get getNews() {
        return this.stock.news;
    }

}
