import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import {NativeScriptRouterModule} from "nativescript-angular";
import {HeadlinesComponent} from "~/app/headlines/headlines.component";

console.log(' &&&&& HEADLINES MODULE SAYS HI &&&&&&');

@NgModule({
  declarations: [HeadlinesComponent],
  imports: [
    NativeScriptCommonModule,
      NativeScriptRouterModule,
      NativeScriptRouterModule.forChild([
          { path: '', redirectTo: 'headlines' },
          { path: 'headlines', component: HeadlinesComponent },
      ])
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class HeadlinesModule { }
