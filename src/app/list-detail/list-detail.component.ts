import {Component, OnInit, ViewContainerRef} from '@angular/core'
import {ActivatedRoute} from "@angular/router"
import {ItemService} from "~/app/item/item.service"
import {ApiService} from "~/app/api.service"
import {ModalDialogOptions, ModalDialogService, RouterExtensions} from "nativescript-angular"
import * as appSettings from "tns-core-modules/application-settings"
import {ItemDetailComponent} from "~/app/item/item-detail.component"
import {EtfDetailComponent} from "~/app/etf-detail/etf-detail.component";

@Component({
    selector: 'ns-list-detail',
    templateUrl: './list-detail.component.html',
    styleUrls: ['./list-detail.component.css']
})
export class ListDetailComponent implements OnInit {

    user: any = {}
    items: any = {}
    isBusy: boolean = true
    list: any = {}

    constructor(private route: ActivatedRoute,
                private item: ItemService,
                private api: ApiService,
                private router: RouterExtensions,
                private modalService: ModalDialogService,
                private viewContainerRef: ViewContainerRef) {
        this.items.ready = false;
    }

    get getList() {
        return this.items.symbols
    }

    ngOnInit() {
        console.log(' INIT --- LIST DETAIL COMP ---')
        this.user = {
            listId: appSettings.getNumber('ListId'),
            listName: appSettings.getString('ListName'),
            cookie: appSettings.getString('cookie'),
            uid: appSettings.getString('uid'),
            starred: appSettings.getNumber('starred')
        }
        console.log('for user:', this.user)
        this.prepareList()
    }

    prepareList() {
        this.item.getListInfo(this.user.uid, this.user.listId, this.user.cookie)
            .subscribe(info => {
                let rawArr = info.symbols
                let listArr = []
                rawArr.forEach(i => {
                    let sortPGR = 0
                    if (i.PGR == 3 && i.PGR == i.raw_PGR) sortPGR = 3
                    if (3 == i.PGR && i.PGR < i.raw_PGR) sortPGR = 3.5
                    if (3 == i.PGR && i.PGR > i.raw_PGR) sortPGR = 2.5
                    if (sortPGR == 0) sortPGR = i.PGR
                    listArr.push({
                        symbol: i.symbol,
                        name: i.name,
                        last: i.Last,
                        change: i.Change,
                        percent: i['Percentage '],
                        pgr: i.PGR,
                        raw: i.raw_PGR,
                        sortPGR: sortPGR,
                        etf: i.is_etf_symbol
                    })
                })
                this.items.powerbar = info.PowerBar.split(',')
                this.items.symbols = listArr
                this.sort('sortPGR')
                this.items.ready = true
                this.isBusy = false
            })
    }

    getListName() {
        return this.user.listName
    }

    bulls() {
        return this.items.powerbar[2]
    }

    neutrals() {
        return this.items.powerbar[1]
    }

    bears() {
        return this.items.powerbar[0]
    }

    pgrRating(pgr, rawPGR, etf) {
        return this.item.getPGRiconAllRAWconfig(pgr, rawPGR, etf)

    }

    navToDetails(stock) {
        console.log('????????', stock)
        if (stock.etf) {
            appSettings.setString('etf-detail', stock.symbol);
            const options: ModalDialogOptions = {
                viewContainerRef: this.viewContainerRef,
                fullscreen: true,
                context: {}
            }
            this.modalService.showModal(EtfDetailComponent, options)
        } else {
            appSettings.setString('symbol', stock.symbol);
            const options: ModalDialogOptions = {
                viewContainerRef: this.viewContainerRef,
                fullscreen: true,
                context: {}
            }
            this.modalService.showModal(ItemDetailComponent, options)
        }
    }

    sort(by) {
        this.list.toggle = !this.list.toggle
        this.list.carat = by
        this.list.toggle
            ? this.items.symbols.sort((a, b) => {
                let sorted = a[by] < b[by] ? 1 : a[by] > b[by] ? -1 : 0;
                return sorted
            })
            : this.items.symbols.sort((a, b) => {
                let sorted = b[by] < a[by] ? 1 : b[by] > a[by] ? -1 : 0;
                return sorted
            })
    }

    getColumns() {
        let bull: number = parseInt(this.items.powerbar[2])
        let bear: number = parseInt(this.items.powerbar[0])
        if (bull > bear) {
            let r = bull/bear
            let floor = Math.floor(r)
            let cols = floor + '*, 40, *'
            return cols
        } else {
            let r = bear/bull
            let floor = Math.floor(r)
            let cols = '*, 40, ' + floor + '*'
            return cols
        }
    }

    itemLastFormatter(item) {
        return '$' + item.toFixed(2)
    }

    itemChangeFormatter(item) {
        return item > 0
            ? '+' + item.toFixed(2)
            : item.toFixed(2)
    }

    itemPercentFormatter(item) {
        return item > 0
            ? '(+' + item + '%)'
            : '(' + item + '%)'
    }

    itemPercentFormatterNoSign(item) {
        return '(' + item + '%)'
    }

    healthColorFormatter(item) {
        return item == 0
            ? 'black'
            : (item > 0
                ? 'green'
                : 'red')
    }
}
