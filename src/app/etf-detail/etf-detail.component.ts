import {Component, OnInit, ElementRef, ViewChild, ViewContainerRef} from "@angular/core"
import {ActivatedRoute} from "@angular/router"
import {ItemService} from "~/app/item/item.service"
import {UserService} from "~/app/user.service"
import * as appSettings from "tns-core-modules/application-settings"
import {ObservableArray} from "tns-core-modules/data/observable-array"
import {ModalDialogOptions, ModalDialogParams, ModalDialogService, RouterExtensions} from "nativescript-angular"
import {ApiService} from "~/app/api.service"
import {TNSFancyAlert} from "nativescript-fancyalert"
import {ItemDetailComponent} from "~/app/item/item-detail.component"
import {ListEtfHoldingsComponent} from "~/app/list-etf-holdings/list-etf-holdings.component";
import * as dialogs from "tns-core-modules/ui/dialogs";

@Component({
  selector: 'ns-etf-detail',
  templateUrl: './etf-detail.component.html',
  moduleId: module.id,
})
export class EtfDetailComponent implements OnInit {

    etf: any = {}
    user: any = {}
    data: any = {}
    readyFlag: boolean = false
    priceMin: number
    priceMax: number
    chart: any = {}
    timespanPerChange: any
    timespanPriceChange: any
    loaded: any = {}
    holdings: any = {}
    rating: any = {}
    menu = 'chart'
    dates: any = {}
    majorStep: string
    noRating: any = {}

    @ViewChild('ScrollList', { static: true }) private scrollList: ElementRef
    // @Input() private scrollableHeight

    private _mainChart: ObservableArray<any>

    constructor(private itemService: ItemService,
                private route: ActivatedRoute,
                private userService: UserService,
                private router: RouterExtensions,
                private api: ApiService,
                private params: ModalDialogParams,
                private modalService: ModalDialogService,
                private viewContainerRef: ViewContainerRef) {
                this.user.perms = {
                    prem: false,
                    marketSurvivalGuide: false,
                    morningInsights: false,
                    marketInsights: false
                }
                this.chart.current = '1M'
                this.loaded = {
                    data: false,
                    graph: false,
                    holdings: false,
                    rating: false,
                    chart: false,
                    perms: false
                }
    }

    ngOnInit(): void {
        console.log(' ------- ETF DETAIL INIT -------')
        this.etf.symbol = appSettings.getString('etf-detail')
        this.user = {
            cookie: appSettings.getString('cookie'),
            uid: appSettings.getString('uid'),
            starred: appSettings.getNumber('starred'),
            userList: appSettings.getNumber('UserListId')
        }
        this.loadMyPerms()
        let viewingPowerBar = appSettings.getBoolean('EtfPower');
        viewingPowerBar
            ? this.menu = 'rating'
            : this.menu = 'chart';

        this.itemService.getListInfo(this.user.uid, this.user.starred, this.user.cookie)
            .subscribe(item => {
                if (item['symbols']) {
                    item['symbols'].forEach(ticker => {
                        ticker.symbol == this.etf.symbol
                            ? this.etf.starred = true
                            : null
                    })
                }
            })

        this.itemService.getListInfo(this.user.uid, this.user.userList, this.user.cookie)
            .subscribe(item => {
                if (item['symbols']) {
                    item['symbols'].forEach(ticker => {
                        ticker.symbol == this.etf.symbol
                            ? this.etf.myList = true
                            : null
                    })
                }
            })
        this.getSymbolData(this.etf.symbol)
        this.getReportData(this.etf.symbol)
    }

    loadMyPerms() {
        let userPerms = this.user.perms
        let userType = appSettings.getString('usertype')
        if (userType == 'CA') {
            userPerms = {
                marketSurvivalGuide: false,
                morningInsights: true,
                marketInsights: true,
                prem: true
            }
        }
        if (userType == 'PP') {
            userPerms = {
                marketSurvivalGuide: false,
                morningInsights: true,
                marketInsights: false,
                prem: true
            }
        }
        this.user.perms = userPerms
        this.loaded.perms = true
    }

    starred() {
        return this.etf.starred ? 'starredActive' : null
    }

    onMyList() {
        return this.etf.myList ? 'starredActive' : null
    }

    changeModal(etf) {
        this.etf.symbol = etf
        this.getSymbolData(etf)
        this.getReportData(etf)
    }

    close() {
        this.params.closeCallback()
    }

    moreInformation() {
        TNSFancyAlert.showSuccess(
            this.etf.details.name + ' Company Information',
            this.etf.fundamentals['Company Text Blurb'],
            'OK')
    }

    Star() {
        return this.etf.starred
            ? this.itemService.deleteStockFromList(this.etf.symbol, this.user.uid, this.user.starred, this.user.cookie)
                .subscribe(del => {
                    this.etf.starred = false
                    let msg = 'Removed ' + this.etf.symbol + ' from my Starred List'
                    dialogs.alert({
                        title: 'Removed',
                        message: msg,
                        okButtonText: 'OK'
                    })
                })
            : this.itemService.addStockIntoList(this.etf.symbol, this.user.uid, this.user.starred, this.user.cookie)
                .subscribe(add => {
                    this.etf.starred = true
                    let msg = 'Added ' + this.etf.symbol + ' to my Starred List'
                    dialogs.alert({
                        title: 'Added',
                        message: msg,
                        okButtonText: 'OK'
                    })
                })
    }

    addToUser() {
        return this.etf.myList
            ? this.itemService.deleteStockFromList(this.etf.symbol, this.user.uid, this.user.userList, this.user.cookie)
                .subscribe(del => {
                    this.etf.myList = false
                    let msg = 'Removed ' + this.etf.symbol + ' from my Stocks List'
                    dialogs.alert({
                        title: 'Removed',
                        message: msg,
                        okButtonText: 'OK'
                    })
                })
            : this.itemService.addStockIntoList(this.etf.symbol, this.user.uid, this.user.userList, this.user.cookie)
                .subscribe(add => {
                    this.etf.myList = true
                    let msg = 'Added ' + this.etf.symbol + ' to my Stocks List'
                    dialogs.alert({
                        title: 'Added',
                        message: msg,
                        okButtonText: 'OK'
                    })
                })
    }

    get contextSummaries(): any {
        return this.etf.contextSums
    }

    getSymbolData(stock: string) {
        this.itemService.getSymbolData(stock, this.user.uid, this.user.cookie)
            .subscribe(res => {
                this.etf.list = parseInt(res['metaInfo'][0]['etf_data']['list_id'])
                if (res['metaInfo'][0]) this.etf.details = res['metaInfo'][0]
                this.etf.fundamentals = res['fundamentalData']
                this.etf.details.assets = res['pgr'][6].assets
                this.etf.details.powerbar = {
                    bulls: res['pgr'][6].power_bar[2],
                    neutrals: res['pgr'][6].power_bar[1],
                    bears: res['pgr'][6].power_bar[0]
                }
                this.etf.pgr = {
                    val: res['pgr'][0]['PGR Value'],
                    fin: res['pgr'][1]['Financials'][0]['Value'],
                    earn: res['pgr'][2]['Earnings'][0]['Value'],
                    tech: res['pgr'][3]['Technicals'][0]['Value'],
                    exp: res['pgr'][4]['Experts'][0]['Value']
                }
                this.loaded.data = true
                this.api.singleEtfInfo(this.user.cookie, this.etf.details.symbol).subscribe(res => {
                    this.holdings = res['sectors_brackdown_table_and_chart_data']['symbol_info']
                    this.loaded.holdings = true
                })
                this.etf.pgr.val > 0 // if we have a rating
                    ? this.api.generalEtfInfo(this.user.cookie)
                        .subscribe(res => {
                            this.api.singleEtfInfo(this.user.cookie, this.etf.symbol).subscribe(etf => {
                                etf['sector_summary_table'] == undefined
                                    ? (this.api.generalEtfInfoGroupType(this.user.cookie, 'Large Cap Blend').subscribe(etf => {
                                            this.api.singleEtfInfo(this.user.cookie, this.etf.symbol).subscribe(answer => {
                                                this.rating = etf['sector_summary_table'];
                                            })
                                        }))
                                    : this.rating = etf['sector_summary_table'];
                                this.loaded.rating = true;
                            })
                        })
                    : (this.rating = {
                        header: 'No Rating - Chaikin Analytics does not yet rate this ETF. This occurs when the security: ',
                        one: '(1) Has been trading for less than one year, ',
                        two: '(2) Does not have a minimum number of valid factors, or ',
                        three: '(3) Is not the primary share class.'
                    },
                    this.loaded.rating = true)
            }
        )
    }

    getReportData(stock: string): void {
        this.itemService.getStockSummaryData(stock)
            .subscribe(data => {
                this.data = data
                this.toggleChartTime('1M')
            })
    }

    pgrRatingEtf(pgr, raw) {
        return this.itemService.getPGRiconETFMaxRAWconfig(pgr, raw)
    }

    pgrNoTxtSmall(pgr, raw) {
        return this.itemService.getPGRiconNoTxtRAWETFconfig(pgr, raw)
    }

    pgrNoTxt(item) {
        return item.is_etf
            ? this.itemService.getPGRiconNoTxtRAWETFconfig(item.pgr, item.raw_pgr)
            : this.itemService.getPGRiconNoTxtRAWconfig(item.pgr, item.raw_pgr)
    }


    statusColors() {
        return this.timespanPerChange
            ? (this.timespanPerChange > 0 ? 'green' : 'red') : (this.etf.details.Change > 0 ? 'green' : 'red')
    }

    invStatusColors() {
        return this.etf.details['PGR'] == 3
            ? 'yellowInv'
            : ( this.etf.details['PGR'] > 3
                    ? 'greenInv'
                    : 'redInv'
            )
    }

    pgrRatingWord(pgr, raw) {
        return this.itemService.getPGRword(pgr, raw)
    }

    get stockSymbol(): string {
        return this.etf['symbol']
    }

    get stockName(): string {
        return this.etf['name']
    }

    get lastChange(): string {
        return this.etf.details['Change'].toFixed(2)
    }

    get lastPercentage(): string {
        return this.etf.details['Percentage '] > 0
            ? '(+' + this.etf.details['Percentage '].toFixed(2) + '%)'
            : '(' + this.etf.details['Percentage '].toFixed(2) + '%)'
    }

    get lastPrice(): string {
        return '$' + parseFloat(this.etf.details['Last']).toFixed(2)
    }

    get movementStatus1():string {
        return this.etf.details['name'] + ' was'
    }

    get getIndustry(): string {
        return this.etf.details['industry_name']
    }

    get getMktCap(): string {
        let cap = this.etf.details['marketCap']
        if (cap > 1000) {
            return (cap / 1000).toFixed(2) + 'B'
        } else if (cap < 1000) {
            return cap.toFixed(2) + 'M'
        }
        return this.etf.details['marketCap']
    }


    get graphStep(): number {
        return this.priceMax/3
    }

    get movementDescription():string {
        return this.etf.details['Change'] > 0 ? '\f062;' + this.etf.details['Change'] : '\f063;' + this.etf.details['Change']
    }

    calculatePricePerChange(firstClose: number, lastClose: number): number {
        return (((lastClose - firstClose) / firstClose) * 100)
    }

    calculatePriceChange(firstClose: number, lastClose: number): number {
        return lastClose - firstClose
    }

    toggleChartTime(span: string) {
        this.loaded.chart = false
        console.log(' === setting up chart === ')
        this.chart.current = span
        let cut = 0
        switch (span) {
            case '1M':
                cut = 20
                this.chart.labelFormatLinear = '%.2f'
                break
            case '3M':
                cut = 60
                this.chart.labelFormatLinear = '%.0f'
                break;
            case '6M':
                cut = 120
                this.chart.labelFormatLinear = '%.0f'
                break
            case '1Y':
                cut = 250
                this.chart.labelFormatLinear = '%.0f'
                break
        }

        let dates = this.data['one_year_chart_data']['calculations_dates'].slice(0, cut).reverse()
        this.chart.DATES = dates

        this.chart.MAXDATE = new Date(dates[cut-1]) // today
        this.chart.MINDATE = new Date(dates[0]) // CUT

        let JSdate = dates.map(d => { return new Date(d)})

        let closePrices = this.data['one_year_chart_data']['close_price'].map(x => +x).reverse().slice(this.data['one_year_chart_data']['close_price'].length - cut)
        let dema = this.data['one_year_chart_data']['chaikin_trend_20001'].map(x => parseFloat(x)).reverse().slice(this.data['one_year_chart_data']['chaikin_trend_20001'].length - cut)

        let firstPriceIndex = 0;
        for (let i=0; i<closePrices.length; i++) {
            if (closePrices[i] == 0) {
                firstPriceIndex = i+1
            }
        }
        this.timespanPerChange = this.calculatePricePerChange(closePrices[firstPriceIndex], closePrices[closePrices.length-1]).toFixed(2)
        this.timespanPriceChange = this.calculatePriceChange(closePrices[firstPriceIndex], closePrices[closePrices.length-1]).toFixed(2)
        let filtPrices = closePrices.slice(firstPriceIndex)
        let filtDates = JSdate.slice(firstPriceIndex)
        this.priceMax = Math.max(...filtPrices) * 1.001
        this.priceMin = Math.min(...filtPrices) * 0.999
        let delta = this.priceMax - this.priceMin
        delta > 1
            ? this.chart.majorStep = Math.ceil(delta / 5)
            : this.chart.majorStep = 0.25
        let output = []

        for (let i=0; i<filtPrices.length; i++) {
            output.push({
                date: filtDates[i],
                price: filtPrices[i],
                dema: dema[i]
            })
        }
        this._mainChart = new ObservableArray(output);
        this.loaded.chart = true;
        console.log(' === chart is set. === ')
    }

    getMainChart(): ObservableArray<any> {
        return this._mainChart;
    }

    getMajorStep(): string {
        switch (this.chart.current) {
            case '1M':
                return 'Day'
                break;
            case '3M':
                return 'Day'
                break;
            case '6M':
                return 'Month'
                break;
            case '1Y':
                return 'Month'
                break;
        }
    }

    getMajorStepCount() {
        switch (this.chart.current) {
            case '1M':
                return 7
                break;
            case '3M':
                return 14
                break;
            case '6M':
                return 1
                break;
            case '1Y':
                return 1
                break;
        }
    }

    getDateFormat(): string {
        if (this.chart.current) {
            switch (this.chart.current) {
                case '1M':
                    return 'MM/dd'
                    break;
                case '3M':
                    return 'MM/dd'
                    break;
                case '6M':
                    return 'MMM'
                    break;
                case '1Y':
                    return 'MMM'
                    break;
            }
        } else {
            return 'MM/dd'
        }
    }

    minDate() {
        return this.chart.MINDATE
    }

    maxDate() {
        return this.chart.MAXDATE
    }

    getLabelFormatLinear() {
        return this.chart.labelFormatLinear;
    }

    getPriceMin(): number {
        return this.priceMin
    }

    getPriceMax(): number {
        return this.priceMax
    }

    itemDetailGraphStatus() {
        return this.etf.details['Change'] > 0 ? '&#xf062;' : '&#xf063;'
    }

    get getGraphTimeChangeText() {
        return this.timespanPerChange > 0
            ? (this.timespanPriceChange + ' (+' + this.timespanPerChange + '%)')
            : (this.timespanPriceChange + ' (' + this.timespanPerChange + '%)')
    }

    getCols() {
        let bull = this.etf.details.powerbar.bulls
        let bear = this.etf.details.powerbar.bears
        if (bull > bear) {
            let r = bull/bear
            let floor = Math.floor(r)
            let cols = floor + '*, 40, *'
            return cols
        } else {
            let r = bear/bull
            let floor = Math.floor(r)
            let cols = '*, 40, ' + floor + '*'
            return cols
        }
    }

    navigate(item): void {
        const options: ModalDialogOptions = {
            viewContainerRef: this.viewContainerRef,
            fullscreen: true,
            context: {}
        }
        appSettings.setString('symbol', item.symbol)
        item.is_etf ? this.changeModal(item.symbol) : this.modalService.showModal(ItemDetailComponent, options)
    }

    showMoreInformation() {
        TNSFancyAlert.showInfo(this.etf.details.name + ' Company Information',
            this.etf.fundamentals['Company Text Blurb'],
            'OK')
    }

    goBack(): void {
        this.router.back()
    }

    getGraph(): void {
        this.itemService.getGraphData(this.etf.symbol, this.user.cookie).subscribe(res => console.log('graph?', res), err => console.error('err', err))
    }

    getMajorStepLinear() {
        return this.chart.majorStep
    }

    viewAllHoldings() {
        appSettings.setNumber('ListId', this.etf.list)
        appSettings.setString('ListName', this.etf.symbol + ' HOLDINGS')
        const options: ModalDialogOptions = {
            viewContainerRef: this.viewContainerRef,
            fullscreen: true,
            context: {}
        }
        this.modalService.showModal(ListEtfHoldingsComponent, options)
    }
}
