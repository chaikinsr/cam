import {Component, ElementRef, OnInit, ViewChild} from '@angular/core'
import {UserService} from "~/app/user.service"
import {RouterExtensions} from "nativescript-angular"
import * as appSettings from 'tns-core-modules/application-settings'
import {ApiService} from "~/app/api.service"
import {ItemService} from "~/app/item/item.service"
import * as dialogs from 'tns-core-modules/ui/dialogs'
import {LoginOptions, LoginResult} from "tns-core-modules/ui/dialogs"

@Component({
    selector: 'ns-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css'],
    moduleId: module.id,
})
export class LoginComponent implements OnInit {

    input: any = {}
    user: any = {}
    remember = false
    checkTest: boolean
    loggedInBefore: boolean = false
    isBusy: boolean = false

    constructor(private userService: UserService,
                private router: RouterExtensions,
                public api: ApiService,
                public item: ItemService) {
    }

    @ViewChild('emailField', { static: true, read: ElementRef }) emailField: ElementRef
    @ViewChild('passwordField', { static: true, read: ElementRef }) passwordField: ElementRef
    @ViewChild('CB1', { static: true, read: ElementRef }) checkBox: ElementRef
    @ViewChild('activityIndicator', { static: true }) activityIndicator: ElementRef

    ngOnInit() {
        console.log(' --- LOGIN COMP INIT --- ')
        // this.input.enabled = true
        let remember = appSettings.getBoolean('rememberUser')
        console.log('found user : ', remember)
        let first = appSettings.getBoolean('LoggedInBefore')
        remember ? (
            this.user.remember = remember,
            this.checkForSession() )
            : this.user.remember = false;

        first ?
            this.loggedInBefore = first :
            this.loggedInBefore = false
    }

    checkForSession() {
        let session = {
            email: appSettings.getString('email'),
            cookie: appSettings.getString('cookie')
        }
        if (session.cookie && session.cookie.length > 1) {
            this.user.email = session.email,
                this.user.cookie = session.cookie,
                this.rememberLogin()
        }
    }

    registerPrompt() {
        let options: LoginOptions = {
            title: "New User Registration",
            message: "Enter your email and password below",
            okButtonText: "Register",
            cancelButtonText: "Cancel",
            userNameHint: "Enter your email",
            passwordHint: "Enter your password",
            userName: '',
            password: ''
        }
        dialogs.login(options).then(
            (loginResult: LoginResult) => {

                if (loginResult.result == false) {
                    console.log('cancelled register')
                }
                if (loginResult.result == true) {
                    console.log('register')
                    console.log('REGISTER NEW USER RESULT: -----> ', loginResult)
                    this.registerNew(loginResult.userName, loginResult.password)
                }
            }, (err: any) => {
                console.log('canceled / error')
            }
        )
    }

    registerNew(email, pW) {
        this.isBusy = true
        this.userService.ISFTcreateContact(email).subscribe(r => { // #1 ISFT CREATE CONTACT
            this.userService.ISFTaddMobileTag(email).subscribe(newtag => console.log(' --- new tag ---> ', newtag))
            this.userService.registerNewUser(email, pW).subscribe(n => { // #2 SEND EMAIL AND PW TO BACKEND
                this.userService.authUser(email, pW).subscribe(auth => { // #4a AUTH
                    // let userPermStatus = auth.headers.get('userPermissionStatus')
                    let cookie = auth.headers.get('Set-Cookie').toString()
                    appSettings.setString('email', email)
                    appSettings.setString('cookie', cookie)
                    this.userService.loginWithEmail(email, cookie).subscribe(userInfo => {  // # 4b LOGIN
                        let userPerms = 'marketSurvivalGuide: false, morningInsights: false, marketInsights: false, premium: false'
                        this.userService.setUserPerms(userInfo.UID, userPerms).subscribe(setPerms => {
                            appSettings.setString('uid', userInfo.UID);
                            appSettings.setString('usertype', 'free');
                            this.api.createStarredList(cookie, userInfo.UID).subscribe(newList => {  // OR CREATE A NEW LIST
                                if (newList.Status) {
                                    this.api.getMyLists(userInfo.UID, cookie).subscribe(list => { // GET NEW LISTID
                                        let lists = list[0]['User Lists'];
                                        let starredId;
                                        let starredList = lists.find(list => list.name == 'Starred')
                                        starredId = parseInt(starredList.list_id)
                                        appSettings.setNumber('starred', starredId)
                                        this.item.addStockIntoList('AAPL', userInfo.UID, starredId, cookie)//.subscribe(res => console.log('1'))
                                        this.item.addStockIntoList('NFLX', userInfo.UID, starredId, cookie)//.subscribe(res => console.log('2'))
                                        this.item.addStockIntoList('GOOGL', userInfo.UID, starredId, cookie)//.subscribe(res => console.log('3'))
                                        this.item.addStockIntoList('AMZN', userInfo.UID, starredId, cookie)//.subscribe(res => console.log('4'))
                                        this.item.addStockIntoList('MSFT', userInfo.UID, starredId, cookie)//.subscribe(res => console.log('5'))
                                    })
                                }
                            })
                        })
                    })
                })
                this.userService.setInitialNewUserRole(email).subscribe(role => {})
            })
            dialogs.alert({
                title: 'Success!',
                message: 'Welcome to Stock & ETF Explorer, powered by Chaikin Analytics.',
                okButtonText: 'OK'
            }).then(res => {
                dialogs.alert({
                    title: 'Privacy',
                    message: 'Stock & ETF Explorer is provided by Chaikin Analytics for educational use only. By signing up, you agree to receive email communications from Chaikin Analytics. This consent can be withdrawn at any time. You may access our complete privacy policy in your profile after logging in.',
                    okButtonText: 'OK'
                }).then( res => {
                    this.router.navigate(['onboard'], {
                        transition: {
                            name: 'fade',
                            duration: 400,
                            curve: 'ease'
                        },
                        clearHistory: true
                    })
                })
            })
        })
    }

    rememberMe() {
        this.checkBox.nativeElement.toggle()
        console.log('checkTest status -->', this.checkBox.nativeElement.checked)
    }

    checkedChange(modelRef) {
        console.log("checkedChange:", modelRef.checked)
        console.log('checkekChange checkTest', this.checkBox.nativeElement.checked)
    }

    rememberProcess() {
        this.checkBox.nativeElement.checked
            ? (
                appSettings.setBoolean('rememberUser', true),
                    console.log('SUBMIT LOGIN -- YES REMEMBER USER')
            )
            : (
                appSettings.setBoolean('rememberUser', false),
                    console.log('SUBMIT LOGIN -- NO DONT REMEMBER')
            )
    }

    submitLogin(input): void {

        // this.userService.refreshLogin().subscribe(session => {
        //     console.log('from submitLogin checking session', session)

            this.isBusy = true
            this.rememberProcess()
            this.userService.authUser(input.email, input.password)
                .subscribe(response => {
                        let userPermStatus = response.headers.get('userPermissionStatus')

                        console.log('this should say allow --> ', userPermStatus)
                        console.log('this should say true --> ', response.body.loggedInStatus)

                        if (response.body.loggedInStatus == 'true' && userPermStatus == 'allow') {
                            console.log('logged in status is true and userpermstatus is allow')
                            this.userService.ISFTaddMobileTag(input.email).subscribe(newtag => console.log(' --- new tag in login --->', newtag))
                            let resHeader = response.headers.get('Set-Cookie')
                            let cookie = resHeader.toString()
                            appSettings.setString('cookie', cookie)
                            this.userService.loginWithEmail(input.email, cookie)
                                .subscribe(userInfo => {

                                    console.log('WHAT IS MY UID', userInfo.UID)

                                    console.log('login with email worked')

                                    let userPerms = 'marketSurvivalGuide: false, morningInsights: false, marketInsights: false, premium: false'
                                    appSettings.setString('uid', userInfo.UID)
                                    if (userInfo.userType == 'CAUser' || userInfo.userType == 'CA-BearUser') {
                                        appSettings.setString('usertype', 'CA')
                                        userPerms = 'marketSurvivalGuide: false, morningInsights: true, marketInsights: true, premium: true'
                                    } else if (userInfo.userType == 'CPSPremiumUser') {
                                        appSettings.setString('usertype', 'PP')
                                        userPerms = 'marketSurvivalGuide: false, morningInsights: false, marketInsights: true, premium: true'
                                    } else {
                                        appSettings.setString('usertype', 'free')
                                        userPerms = 'marketSurvivalGuide: false, morningInsights: false, marketInsights: false, premium: false'
                                    }
                                    this.userService.getUserPerms(userInfo.UID).subscribe(perms => {
                                        perms == 'unable to fetch data'
                                            ? ( this.userService.setUserPerms(userInfo.UID, userPerms).subscribe(set => this.prepareLists(userInfo.UID, cookie)))
                                            : this.prepareLists(userInfo.UID, cookie)
                                        }
                                    )
                                })
                        } else {
                            dialogs.alert({
                                title: 'Sorry',
                                message: 'Those credentials are not correct',
                                okButtonText: 'OK'
                            })
                            this.isBusy = false
                        }
                    }
                )
            // }
        // )
    }

    rememberLogin() {
        this.isBusy = true;
        this.userService.loginWithEmail(this.user.email, this.user.cookie)
            .subscribe(u => {
                this.router.navigate(['nav/tabs'], {
                    transition: {
                        name: 'fade',
                        duration: 500,
                        curve: 'ease'
                    },
                    clearHistory: true
                })
            })
    }

    prepareLists(uid, cookie) {
        this.api.getMyLists(uid,cookie).subscribe(list => {
            let lists = list[0]['User Lists']
            let myStocks = lists.find(list => list.name == 'My Stocks')
            appSettings.setNumber('UserListId', parseInt(myStocks.list_id))
            let myStarred = lists.find(list => list.name == 'Starred')
            appSettings.setNumber('starred', parseInt(myStarred.list_id))
            console.log('PREPARE LISTS DO YOUR JOB ------> ', parseInt(myStarred.list_id))
            this.prepareRouting()

            if (myStarred == undefined) {
                console.log(' ==== starred is undefined === ')
                this.api.createStarredList(cookie, uid) // OR CREATE A NEW LIST
                    .subscribe(newList => {
                        newList.Status == true // IF IT WORKED
                            ? this.api.getMyLists(uid, cookie)
                                .subscribe(list => { // GET NEW LISTID
                                    let lists = list[0]['User Lists'];
                                    let starredList = lists.find(list => list.name == 'Starred')
                                    let starredId: number = parseInt(starredList.list_id)
                                    appSettings.setNumber('starred', starredId)
                                    this.item.addStockIntoList('AAPL', uid, starredId, cookie)//.subscribe(r => console.log('1'))
                                    this.item.addStockIntoList('NFLX', uid, starredId, cookie)//.subscribe(r => console.log('2'))
                                    this.item.addStockIntoList('GOOGL', uid, starredId, cookie)//.subscribe(r => console.log('3'))
                                    this.item.addStockIntoList('AMZN', uid, starredId, cookie)//.subscribe(r => console.log('4'))
                                    this.item.addStockIntoList('MSFT', uid, starredId, cookie)//.subscribe(r => console.log('5'))
                                    this.prepareRouting()
                                })
                            : console.log('ERROR CREATING LIST')
                    })
            }
        })
    }

    prepareRouting() {
        let before = appSettings.getBoolean('LoggedInBefore')
        before == undefined ? (
            this.router.navigate(['onboard'], {
                transition: {
                    name: 'fade',
                    duration: 500,
                    curve: 'ease'
                },
                clearHistory: true
            })
        )
        : (
            this.router.navigate(['nav/tabs'], {
                transition: {
                    name: 'fade',
                    duration: 500,
                    curve: 'ease'
                },
                clearHistory: true
            })
        )
    }

    resetPassword() {
        dialogs.prompt({
            title: "Request Password Reset",
            message: "Enter the email associated with the account",
            okButtonText: "Reset",
            cancelButtonText: "Cancel",
            inputType: dialogs.inputType.text
        }).then(function (r) {
            console.log("Dialog result: " + r.result + ", text: " + r.text)
        })
    }
}
