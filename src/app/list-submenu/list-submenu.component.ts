import {Component, OnInit, ViewContainerRef} from '@angular/core'
import {ActivatedRoute} from "@angular/router"
import * as appSettings from 'tns-core-modules/application-settings'
import {ItemService} from "~/app/item/item.service"
import {ModalDialogOptions, ModalDialogService, RouterExtensions} from "nativescript-angular"
import {EtfDetailComponent} from "~/app/etf-detail/etf-detail.component";
import {ListEtfHoldingsComponent} from "~/app/list-etf-holdings/list-etf-holdings.component";
@Component({
  selector: 'ns-list-submenu',
  templateUrl: './list-submenu.component.html',
  styleUrls: ['./list-submenu.component.css']
})
export class ListSubmenuComponent implements OnInit {
    list: any = {}
    user: any = {}
    isBusy: boolean = true
    lists: any = {}
    constructor(private item: ItemService,
                private router: RouterExtensions,
                private route: ActivatedRoute,
                private modalService: ModalDialogService,
                private viewContainerRef: ViewContainerRef,) {
        this.list.ready = false
    }
    ngOnInit() {

        console.log(' ---- init: list submenu ----- ')

        this.list.name = appSettings.getString('ListTitle')
        this.user.cookie = appSettings.getString('cookie')
        this.user.uid = appSettings.getString('uid')
        this.item.getAuthorizedLists(this.user.uid, this.user.cookie)
            .subscribe(lists => {
                let indexOfUserList, indexOfIndustry, indexOfETF
                let numberOfLists = Object.keys(lists).length
                for (let i=0;i<numberOfLists;i++) {
                    if (Object.keys(lists[i]).toString() == 'User Lists') indexOfUserList = i
                    if (Object.keys(lists[i]).toString() == 'Industries') indexOfIndustry = i
                    if (Object.keys(lists[i]).toString() == 'ETFs') indexOfETF = i
                }
                let userList = lists[indexOfUserList]['User Lists']
                let industry = lists[indexOfIndustry]['Industries']
                let etf = lists[indexOfETF]['ETFs']
                this.lists = userList
                let answer = [];

                industry.forEach(item => {
                    let powerBarSplit = item['powerBar'].split(',')
                    answer.push({
                        name: item['name'],
                        bear: parseInt(powerBarSplit[0]),
                        neut: parseInt(powerBarSplit[1]),
                        bull: parseInt(powerBarSplit[2]),
                        list: item['list_id'],
                    })
                    this.list.industry = answer
                })

                this.list.etf = etf
                this.list.ready = true
            })
    }

    nav(item) {
        appSettings.setNumber('ListId', item.list_id)
        appSettings.setString('ListName', item.name)
        this.router.navigate(['../list-detail'],{
            relativeTo: this.route,
            transition: {
                name:  'fade',
                duration: 300
            }
        })
    }

    navETF(x) {
        console.log('go to submenu 2', x)
        this.item.getListData(this.user.uid, x.list_id, this.user.cookie)
            .subscribe(res => {
                appSettings.setString('subTwoName', x.name);
                appSettings.setNumber('subTwo', x.list_id);
                this.router.navigate(['../subtwo'],{
                    relativeTo: this.route,
                    transition: {
                        name:  'fade',
                        duration: 300
                    }
                })
        })
    }

    pgrRatingETF(pgr, raw) {
        return this.item.getPGRratingETF(pgr, raw)
    }

    setBullWidth(item) {
        let total = (item.bull+item.bear)
        let bullishness = (item['bull'] / total)
        let math = (bullishness * 90)
        return Math.max(math,40)
    }

    setBearWidth(item) {
        let total = (item.bull+item.bear)
        let bearishness = (item.bear / total)
        let math = (bearishness * 90)
        return Math.max(math,40)
    }

    goToETFDetail(item) {
        console.log(' === GO TO ETF DETAIL : === ', item)
        appSettings.setString('etf-detail', item.ticker)
        // appSettings.setString('holdings-list-id', item.list.toString())
        const options: ModalDialogOptions = {
            viewContainerRef: this.viewContainerRef,
            fullscreen: true,
            context: {}
        }
        this.modalService.showModal(EtfDetailComponent, options)
    }

    goToListDetail(item) {
        console.log(' --- GO TO LIST DETAIL : --- ', item)
        appSettings.setNumber('ListId', item.list)
        appSettings.setString('ListName', item.name)
        const options: ModalDialogOptions = {
            viewContainerRef: this.viewContainerRef,
            fullscreen: true,
            context: {}
        }
        this.modalService.showModal(ListEtfHoldingsComponent, options)
    }
}
